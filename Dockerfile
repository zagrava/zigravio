# ---- Base Node ----
FROM node:8.9

# Create app directory
WORKDIR /zigravio

# Copy and install packages
COPY package*.json ./
RUN npm install

# Copy other files
COPY . .

EXPOSE 8080
CMD ["node", "server.js"]