const Controller = require('./Controller.js');
const Pl = require('./Player.js');
const wall = require('./Wall.js');

function v(a, b) {
    this.x = a;
    this.y = b;
}

let canvas;
let context;

var socket = io.connect();

const keys = {left:false,
            right:false,
            up:false,
            down:false,
            shoot:false,
            blink:false,
            is_blink_pressed:false,
            respawn: false,
            mouse: new v(window.innerWidth * 0.5, window.innerHeight * 0.5) };

let objects = [];
let walls = [];

var main;
var eye = new v(0, 0);
let view = Controller.CreateBoundingBox(0,0,0,0);
var Texture = new v(111, 114);
var FonePicture;
var CrosshairPicture;
var AimconfPicture;

///////////////////////////
var last = 0;

var frameCountDate;//FRAME COUNT
var frames = 0;//FRAME COUNT
///////////////////////////

window.requestAnim = window.requestAnimationFrame ||
                     window.webkitRequestAnimationFrame ||
                     window.mozRequestAnimationFrame ||
                     window.oRequestAnimationFrame ||
                     window.msRequestAnimationFrame ||
                     function(h) { return window.setTimeout(h, 1E3 / 60) };

// window.cancelAnimationFrame = window.cancelAnimationFrame || window.webkitCancelAnimationFrame || ... !!!!!LOOK DOWN!!!!!
// https://www.paulirish.com/2011/requestanimationframe-for-smart-animating/
// https://developer.mozilla.org/en-US/docs/Glossary/Vendor_Prefix

var GLOBALKA = [];

var FRAMETIME = (1000/60);
var RENDERTIME = 100;
var room = {};
let max_react_num = undefined;

var PingIn = 0;
var timelist = [];

//////////////////////////
var sessionid = "";
socket.on('connect', function() {
    sessionid = socket.id;
});

var requestId = undefined;

function startloop() {
    if (!requestId) {
       requestId = window.requestAnim(loop);
    } else {
        console.log("requestId is already defined");
    }
}

function stoploop() {
    if(requestId) {
       window.cancelAnimationFrame(requestId);
       requestId = undefined;
    } else {
        console.log("requestId is undefined");
    }
}
//////////////////////////

socket.on('sync', function(data) {
    PingIn++;

    objects = Pl.AcceptSyncData(objects, timelist, sessionid, data);
});

socket.on('entered', function(data) {
    canvas.style.display = "block";
    document.body.style.cursor = 'none';

    const mainpage = document.getElementById('background');
    const mainpage_elems = mainpage.getElementsByTagName("*");
    for (let i = 0; i < mainpage_elems.length; i++) {
        const element = mainpage_elems[i];
        element.style.display = "none";
    }
    mainpage.style.display = "none";

    max_react_num = data[0].max_react_num;

    room.type = data[0].type;
    room.width = data[0].width;
    room.height = data[0].height;

    for(var i = 1; i < data.length; i++) {
        var Data = data[i];

        if(Data.type == "pl") {
            if(Data.id == sessionid) {
                main = {};
                Pl.player(main, Data);
                Pl.plSetClientData(main, Data);
            	objects.push(main);
            } else {
                var pl = {};
                Pl.player(pl, Data);
                Pl.plSetClientData(pl, Data);
            	objects.push(pl);
            }
        } else {
            var w1 = new wall();
            for(var j = 0; j < Data.vertices.length; j++) {
                w1.AddV(Data.vertices[j].x, Data.vertices[j].y);
            }
            w1.midVert = Data.midVert;
            w1.bb = Data.bb;
            walls.push(w1);
        }
    }

    // Start the loop/game
    startloop();
});

socket.on('closeroom', function(data) {
    document.getElementById("matchinfo").style.display = "block";
    document.body.style.cursor = "";

    if(data.final == "vic") {
        document.getElementById("results").style.color = 'rgb(255,215,0)';
        document.getElementById("results").innerText = 'VICTORY';
    } else if(data.final == "def") {
        document.getElementById("results").style.color = 'rgb(198,7,7)';
        document.getElementById("results").innerText = 'DEFEAT';
    } else {
        document.getElementById("results").style.color = 'rgb(198,7,7)';
        document.getElementById("results").innerText = 'DRAW';
    }

    timelist = [];
    objects = [];
    walls = [];
    stoploop();
});

document.getElementById('DUEL').addEventListener("click", function(){
    socket.emit('enter', {
        type: 'DUEL',
        name: document.getElementById('name').value,
    });
});

document.getElementById('FFA').addEventListener("click", function(){
    socket.emit('enter', {
        type: 'FFA',
        name: document.getElementById('name').value,
    });
});

document.getElementById('exitroom').addEventListener("click", function(){
    document.getElementById("matchinfo").style.display = "none";
    canvas.style.display = "none";

    const mainpage = document.getElementById('background');
    const mainpage_elems = mainpage.getElementsByTagName("*");
    for (let i = 0; i < mainpage_elems.length; i++) {
        const element = mainpage_elems[i];
        element.style.display = "";
    }
    mainpage.style.display = "";
});

document.getElementById('respawn').addEventListener("click", function(){
    keys.respawn = true;
});

function PaintGameData(data, elem, ToHide = false) {
    document.getElementById(elem).style = "color:red; border: 1px solid blue; font-size: 200%;";
    document.getElementById(elem).innerHTML = data;

    if(ToHide) {
        document.getElementById(elem).style = "";
        document.getElementById(elem).innerHTML = "";
    }
}


function start() {
    canvas = document.getElementById( 'canvas' );

    if (canvas && canvas.getContext) {
        context = canvas.getContext('2d');

		// Register event listeners
		window.addEventListener('mousemove', MouseMoveHandler, false);
		window.addEventListener('mousedown', MouseDownHandler, false);
		window.addEventListener('mouseup', MouseUpHandler, false);
		window.addEventListener('keydown', KeyDownHandler, false);
		window.addEventListener('keyup', KeyUpHandler, false);
		window.addEventListener('resize', ResizeHandler, false);

		FonePicture = new Image();
        FonePicture.src = "images/" + "Background.jpg";

        CrosshairPicture = new Image();
        CrosshairPicture.src = "images/" + "crosshair.png";

        AimconfPicture = new Image();
        AimconfPicture.src = "images/" + "aimconfirmation.png";


		frameCountDate = performance.now();//FRAME COUNT

        ResizeHandler();

        /////////////////////////

        /////////////////////////
	}
}

/////////////////////////
// const ServerController = require('./ServerController');
// const RoomRuler = require("./RoomRuler");
// const walls1 = require("./Warehouse");
// const Pl = require("./Player");

/////////////////////////

function ResizeHandler() {
    canvas.width = window.innerWidth;
	canvas.height = window.innerHeight;
}

function MouseMoveHandler(event) {
	keys.mouse.x = event.clientX;
	keys.mouse.y = event.clientY;
}

function MouseDownHandler(event) {
	keys.shoot = true;
}

function MouseUpHandler(event) {
	keys.shoot = false;
}

function KeyDownHandler(event) {
    var is = true;
    switch(event.keyCode) {
        case 65:keys.left = true; break;
        case 68:keys.right = true; break;
        case 87:keys.up = true; break;
        case 83:keys.down = true; break;
        case 32:keys.blink = true;break;
    }
  //if(is) event.preventDefault();
}

function KeyUpHandler(event) {
    var is = true;
    switch(event.keyCode) {
        case 65:keys.left = false; break;
        case 68:keys.right = false; break;
        case 87:keys.up = false; break;
        case 83:keys.down = false; break;
        case 32:keys.blink = false; keys.is_blink_pressed = false; break;
        default: is = false;
    }
    //if(is) event.preventDefault();
}

function loop(curr) {
    var seconds = (curr - last) / 1000;
    last = curr;

    // FRAME COUNT
    var frseconds = (curr - frameCountDate ) / 1000;
    if(frseconds >= 1.0) {
        frameCountDate = curr;
        frames = 0;
    }
    frames++;

    // HANDLE SENT TIMELIST + REACTARRAYS + LASTSEENFRAME
    const now = performance.now();

    Pl.AlignTimelist(timelist, now - RENDERTIME, now, FRAMETIME, PingIn);
    PingIn = 0;

    const result_arr = Pl.CreateReactArrAndLSeenFrame(timelist, now - RENDERTIME, max_react_num);
    const reaction_arr = result_arr[0];
    const last_frame_seen = result_arr[1];
    // HANDLE SENT TIMELIST + REACTARRAYS + LASTSEENFRAME

    // Remember main data before applying any inputs
    const objs_walls = objects.concat(walls);
    const oldmainx = main.x;
    const oldmainy = main.y;
    const oldmainenergy = main.energy;

    // GAME TICK //
    // GAME TICK //

    // HANDLE PLAYER COMMANDS //
    Pl.HandleCommands(main, keys, eye);

    main.velocity = Controller.mult(main.velocity, reaction_arr.length);
    // HANDLE PLAYER COMMANDS //

    // PROCESS PLAYER'S INPUTS //
    if(last_frame_seen !== -1) {
        //Fire Stuff
        let hitlist = Controller.ObjFiring(objs_walls, objects.indexOf(main), objs_walls);

        GraphicsController.setup.paint.hit = Pl.DoesShootPlayer(main, hitlist);

        // Skills stuff
        if(Pl.IsHeAlive(main)) {
            GraphicsController.setup.paint.healtbar = true;
            GraphicsController.setup.paint.energybar = true;

            for (let i = 0; i < (last_frame_seen+1); i++) {
                Pl.EnergyRegen(main);
            }
        } else {
            GraphicsController.setup.paint.healtbar = false;
            GraphicsController.setup.paint.energybar = false;
        }

        if(Pl.CanHeBlink(main)) {
            Pl.Blink(main);
        }
        Controller.ZoneClip(main, room.width, room.height);

        // Collision Stuff
        Pl.SetBoundingBox(main, main.r+Controller.len(main.velocity)+1);
        if(Pl.CanHeMove(main)) {
            Controller.PlRunCollCycle(main, walls);
            Pl.Move(main);
        }

        // Synchronization stuff
        main.synclist.push({
            poschange: new v(main.x-oldmainx, main.y-oldmainy),
            energychange: main.energy-oldmainenergy,
            last_react_sent: reaction_arr[reaction_arr.length-1]
        });
    }
    // PROCESS PLAYER'S INPUTS //

    // SCENE PAINTING //
    GraphicsController.SetEyeViewTo(main);

    GraphicsController.PaintRepeatingBackground();

    for(let i = 0; i < objects.length; i++) {
        if(objects[i].id != sessionid) {
            Pl.AdjustSynclist(objects[i], timelist, 100);
            Pl.GetDataFromSynclist(objects[i], last_frame_seen);
            objects[i].synclist.splice(0, last_frame_seen+1);
        }

        Pl.SetBoundingBox(objects[i], objects[i].r+1);

        if(Pl.IsHeAlive(objects[i]) && Controller.BoxBoxCollDet(objects[i].bb, view)) {
            GraphicsController.PaintPlayer(objects[i]);
        }
    }

    for(let i = 0; i < walls.length; i++) {
        if(Controller.BoxBoxCollDet(walls[i].bb, view)) {
            GraphicsController.PaintWall(walls[i]);
        }
    }

    GraphicsController.DrawInterface();
    // SCENE PAINTING //

    timelist.splice(0, last_frame_seen+1);

    if(last_frame_seen !== -1) {
        socket.emit('sync', {
            dir: {x: main.dir.x, y: main.dir.y},
            left: keys.left,
            right: keys.right,
            up: keys.up,
            down: keys.down,
            blink: main.blink,
            fire: main.fire,
            respawn: main.respawn,
            reaction_arr: reaction_arr
        });
    }

    requestId = window.requestAnim(loop);
}

var GraphicsController = {
    setup: {
        paint: {
            // What should I paint
            hit: false,
            fire: false,
            healtbar: false,
            energybar: false
        }
    },
    SetEyeViewTo: function(view_center) {
        eye.x = view_center.c.x - canvas.width/2;
        eye.y = view_center.c.y - canvas.height/2;

        view = Controller.CreateBoundingBox(view_center.c.y - (canvas.height/2),
                                            view_center.c.y + (canvas.height/2),
                                            view_center.c.x - (canvas.width/2),
                                            view_center.c.x + (canvas.width/2));
    },
    PaintBox: function(box) {
        context.strokeStyle = "#FF0000";
        context.strokeRect(box.top_left.x-eye.x, box.top_left.y-eye.y,
                           box.bottom_right.x - box.top_left.x,
                           box.bottom_right.y - box.top_left.y);
    },
    PaintRepeatingBackground: function() {
        const relative_point = new v(eye.x, eye.y);
        Controller.DistBetP_Sq(relative_point, {x: 222, y: 228});
        context.drawImage(FonePicture, relative_point.x, relative_point.y, canvas.width, canvas.height, 0, 0, canvas.width, canvas.height);
    },
    PaintWall: function(wall) {
        context.fillStyle = 'rgb(86, 105, 135)';
        context.beginPath();
        context.moveTo(wall.vertices[0].x - eye.x, wall.vertices[0].y - eye.y);
        for(var i = 1, l = wall.vertices.length; i < l; i++) {
            context.lineTo(wall.vertices[i].x - eye.x, wall.vertices[i].y - eye.y);
        }
        context.lineTo(wall.vertices[0].x - eye.x, wall.vertices[0].y - eye.y);
        context.fill();
    },
    PaintPlayer: function(pl) {
        context.save();
        context.translate(pl.c.x - eye.x, pl.c.y - eye.y);
        context.rotate(Math.atan2(pl.dir.y, pl.dir.x));

        // context.drawImage(pl.skin, -(pl.r), -(pl.r), pl.r*2, pl.r*2);
        context.beginPath();
        context.arc(0, 0,  pl.r, 0, 2*Math.PI, false);
        context.fillStyle = pl.color;
        context.fill();

        context.beginPath();
        if(pl.fire) context.filter = "brightness(170%)";
        context.arc(0, 0, pl.r-1, 347.5*(Math.PI/180), 12.5*(Math.PI/180), false);
        context.lineTo(pl.r*1.4, 0);
        context.fill();

        context.restore();
    },
    PaintLeaderboard: function(sx, sy, width, line_height, headline_height, lines) {
        // var a = objects;
        // var objects = [{kills: 7,},{kills: 8,},{kills: 14,},
        //                {kills: 1,},{kills: 5,},{kills: 5,},
        //                {kills: 3,},{kills: 15,},{kills: 16,}];

        var leaderlist = [];
        for(var j = 0; j < objects.length; j++) {
            for(var i = 0; i < leaderlist.length; i++) {
                if(objects[j].kills > leaderlist[i].kills) {
                    break;
                }
            }
            leaderlist.splice(i, 0, objects[j]);
        }
        var lines = Math.min(lines, leaderlist.length);
        var height = lines * line_height;
        context.fillStyle = 'rgba(33, 52, 121, 0.7)';
        context.fillRect(sx, sy, width, height+headline_height+10);
        context.fillStyle = 'rgb(255, 255, 255)';

        var y = sy+10;

        y += headline_height-10;
        context.font = headline_height +"px Arial";
        context.textAlign = "left";
        context.fillText("Top players:", sx, y);
        context.textAlign = "right";
        context.fillText("Kills:", sx+width, y);
        context.font = line_height +"px Arial";

        for(j = 0; j < lines; j++) {
            (leaderlist[j] === main) ? context.fillStyle = 'rgb(255,215,0)' : context.fillStyle = 'rgb(255, 255, 255)';

            y += line_height;
            context.textAlign = "left";
            context.fillText(leaderlist[j].name, sx, y);
            context.textAlign = "right";
            context.fillText(leaderlist[j].kills, sx+width, y);
        }

        context.fillStyle = 'rgb(255, 255, 255)';

        // objects = a;
    },
    PaintStripedBar: function(sx, sy, width, height, value, value_max, value_step, step_style, gap_width, gap_style) {
        var x = sx; var y = sy;
        var topaint = value;
        var gaps_width = (Math.ceil(value_max / value_step) - 1) * gap_width;
        var paint_place = width-gaps_width;
        var step_width = (value_step/value_max) * paint_place;

        while(topaint > value_step) {
            context.fillStyle = step_style;
            context.fillRect(x, y, step_width, height);
            x += step_width;
            context.fillStyle = gap_style;
            context.fillRect(x, y, gap_width, height);
            x += gap_width;
            topaint -= value_step;
        }
        context.fillStyle = step_style;
        context.fillRect(x, y, (topaint/value_max) * paint_place, height);
    },
    DrawInterface: function() {
        context.drawImage(CrosshairPicture, keys.mouse.x - (20/2), keys.mouse.y - (20/2), 20, 20);

        if(this.setup.paint.hit) {
            context.drawImage(AimconfPicture, keys.mouse.x - (17/2), keys.mouse.y - (17/2), 17, 17);
        }

        if(this.setup.paint.healtbar && this.setup.paint.energybar) {
            context.fillStyle = 'rgba(33, 52, 121, 0.7)';
            var combat_bar_width = 480;
            var combat_bar_height = 65;
            var combat_bar_x = (canvas.width/2) - (combat_bar_width/2);
            var combat_bar_y = canvas.height-combat_bar_height;
            context.fillRect(combat_bar_x, combat_bar_y, combat_bar_width, combat_bar_height);
            
            GraphicsController.PaintStripedBar(combat_bar_x + 10, combat_bar_y + 10, combat_bar_width - 20, 10, main.energy, main.max_energy, main.blink_energycost, "#FFFFFF", 5, 'rgba(0, 0, 0, 0)');
            GraphicsController.PaintStripedBar(combat_bar_x + 10, combat_bar_y + 25, combat_bar_width - 20, 30, main.hp, main.hp_max, main.hp_max, "#FFFFFF", 3, 'rgba(0, 0, 0, 0)');
        }

        if(room.type == "DUEL")
        {
            
        }
        else if(room.type == "FFA")
        {
            var respawn_width = 280;
            var respawn_height = 70;
            var respawn_left = ((canvas.width/2) - (respawn_width/2));
            var respawn_top = (canvas.height-respawn_height);
            var respawnbtn = document.getElementById('respawn');
            if(!Pl.IsHeAlive(main)) {
                respawnbtn.style.top = respawn_top + "px";
                respawnbtn.style.left = respawn_left + "px";
                respawnbtn.style.display = "block";
            } else {
                respawnbtn.style.display = ""; // default display = none
            }

            GraphicsController.PaintLeaderboard(canvas.width-300-10, 10, 300, 30, 30, 10);
        }
    }

};


window.onload = start;


