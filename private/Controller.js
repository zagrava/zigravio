function v(a, b) {
    this.x = a;
    this.y = b;
}

function Contact(norm, dest, contactP, distToContactP, a, b) {
    this.contN = norm;
    this.destP = dest;
    this.contactP = contactP;
    this.distToContactP = distToContactP;
    this.ind1 = a;
    this.ind2 = b;
}

var Controller = {
    ContactList: new Array(),
    IsStuck: false,
    dot: function(v1, v2) {
        return v1.x * v2.x + v1.y * v2.y;
    },
    subt1_2: function(v1, v2) {
        return { x: v1.x - v2.x, y: v1.y - v2.y };
    },
    add: function(v1, v2) {
        return { x: v1.x + v2.x, y: v1.y + v2.y };
    },
    perp: function(v) {
        return { x: v.y, y: -v.x };
    },
    norm: function(v) {
        var l = this.len(v);
        if(l == 0) {
            return { x: 0, y: 0 };
        }
        return { x: v.x / l, y: v.y / l };
    },
    len: function(v) {
        return Math.sqrt(v.x * v.x + v.y * v.y);
    },
    neg: function(v) {
        return { x: -v.x, y: -v.y };
    },
    mult: function(v, s) {
        return { x: v.x * s, y: v.y * s};
    },
    RandomColor: function(brightness) {
        return 'rgb('+(Math.round(Math.random()*brightness))+','+(Math.round(Math.random()*brightness))+','+(Math.round(Math.random()*brightness))+')';
    },
    BoxBoxCollDet: function(b0, b1) {
        return !(b0.top_left.x > b1.bottom_right.x ||
                 b0.bottom_right.x < b1.top_left.x ||
                 b0.top_left.y > b1.bottom_right.y ||
                 b0.bottom_right.y < b1.top_left.y)
    },
    CreateBoundingBox: function(top, bottom, left, right) {
        return {
            top_left: new v (left, top),
            bottom_right: new v(right, bottom)
        };
    },
    ZoneClip: function(player, zwidth, zheight) {
        player.x = Math.max(10, player.x);
        player.y = Math.max(10, player.y);
        player.x = Math.min(zwidth - 10 - (player.r*2), player.x);
        player.y = Math.min(zheight - 10 - (player.r*2), player.y);

        player.c.x = player.x + player.r;
        player.c.y = player.y + player.r;
    },
    ObjFiring: function (objects_views, shooter_index, actual_objects) {
        let dist = -1;
        const hitlist = [];
        const shooting_obj = actual_objects[shooter_index];

        // Test if this object is able to shoot
        if(shooting_obj.type != "pl") {
            return hitlist;
        }

        for(let j = 0; j < objects_views.length; j++) {
            if(shooter_index == j) continue;

            //console.log(objects_views[j].midVert, "J:", j);
            if(objects_views[j].type == "pl" && this.len(this.subt1_2(shooting_obj.c, objects_views[j].c)) < (shooting_obj.range + 200 ) ) {
                // Find out the dist by dir vector from obj[shooter_index] to obj[j]
                dist = this.DistBetP_C(shooting_obj.c, shooting_obj.dir, objects_views[j].c, objects_views[j].r);
            }
            else if (objects_views[j].type == "wl" && this.len(this.subt1_2(shooting_obj.c, objects_views[j].midVert)) < (shooting_obj.range + 600 ) ) {
                // Find out the dist by dir vector from obj[shooter_index] to obj[j]
                dist = this.IsWlHit(objects_views[j].vertices, shooting_obj);
            }

            if(dist != -1 && dist <= shooting_obj.range) {
                let k = 0;
                for(k = 0; k < hitlist.length; k++)
                {
                    if(dist < hitlist[k].dist) {
                        break;
                    }
                }

                hitlist.splice(k, 0, {
                    dist: dist,
                    obj: actual_objects[j],
                    index: j,
                });
            }

            dist = -1;
        }

        return hitlist;
    },
    IsWlHit: function(vertices, player) {
        var mind = 1085;
        var is_hit = false;
        for(var i = 0, l = vertices.length - 1; i < l; i++) {
            var norm = this.norm(this.perp(this.subt1_2(vertices[i+1], vertices[i])));
            if(this.dot(norm, this.subt1_2(player.c, vertices[i])) < 0) { norm = this.neg(norm); }
            var d = this.DistBetP_L(player.c, player.dir, vertices[i], norm);
            if(d >= 0) {
                var p = this.add(player.c, this.mult(player.dir, d));

                if(this.IsPinW(p, vertices[i+1], vertices[i])) {
                    if(d <= mind) {
                        mind = d;
                        is_hit = true;
                    }
                }
            }
        }
        //i++
        var norm = this.norm(this.perp(this.subt1_2(vertices[i], vertices[0])));
        if(this.dot(norm, this.subt1_2(player.c, vertices[0])) < 0) { norm = this.neg(norm); }
        var d = this.DistBetP_L(player.c, player.dir, vertices[0], norm);
        if(d >= 0) {
            var p = this.add(player.c, this.mult(player.dir, d));

            if(this.IsPinW(p, vertices[i], vertices[0])) {
                if(d <= mind) {
                    mind = d;
                    is_hit = true;
                }
            }
        }
        if(is_hit == true) {
            return mind;
        }
        return -1;
    },
    IsPlHit: function(c, r) {
        return this.DistBetP_C(main.c, main.dir, c, r);
    },
    IsWlsColl: function(vertices, pl, inda, indb) {
        var mind = Controller.len(pl.velocity);
        for(var i = 0, l = vertices.length - 1; i < l; i++)
        {
            mind = this.IsWlColl(vertices[i], vertices[i+1], mind, pl, inda, indb);
        }
        mind = this.IsWlColl(vertices[i], vertices[0], mind, pl, inda, indb);
        if ( typeof this.ContactList === "undefined" || !this.ContactList || this.ContactList.length == 0 || mind == Controller.len(pl.velocity) ) {
            return -1;
        }
        return this.ContactList[this.ContactList.length - 1].distToContactP;
    },
    IsWlColl: function(V0, V1, argmind, pl, inda, indb) {
        var TcollP;
        var CcollP;

        var d;
        var mind = argmind;

        var v0 = V0;
        var v1 = V1;

        var norm = this.norm(this.perp(this.subt1_2(v1, v0)));
        if(this.dot(norm, this.subt1_2(pl.c, v0)) < 0) { norm = this.neg(norm); }

        CcollP = this.subt1_2(pl.c, this.mult(norm, pl.r));

        if(this.IsinTPl(pl.c, this.mult(norm, pl.r), v0)) { //Sphere in plane
            d = this.DistBetP_L(CcollP, norm, v0, this.neg(norm) );
            TcollP = this.add(CcollP, this.mult(norm, d));
            if(Math.max(0.0005, d) == 0.0005) {
                d = 0.0005;
            }

            if(this.IsPinW(TcollP, v1, v0)) { //STUCK
                Pl.Move(pl, d, norm);//<------------------------------------------------------------------------------------------------------------Controller.mult(norm, 1);
                IsStuck = true; // PROBABLE BUG PROBABLE BUG PROBABLE BUG PROBABLE BUG PROBABLE BUG PROBABLE BUG PROBABLE BUG
                d = 0;

                var cos = this.dot(this.neg(norm), pl.norm_velocity);
                if(cos >= 0.0005) {
                    //Sphere coll with wall
                    ///
                    if(this.ContactList.length > 0) {
                        if( (this.ContactList[this.ContactList.length-1].ind1 == inda && this.ContactList[this.ContactList.length-1].ind2 == indb) ||
                            (this.ContactList[this.ContactList.length-1].ind1 == indb && this.ContactList[this.ContactList.length-1].ind2 == inda) )
                        {
                            this.ContactList.pop();
                        }
                    }
                    ///

                    this.ContactList.push(new Contact(norm, this.add(TcollP, pl.velocity), TcollP, d, inda, indb));
                    mind = d;
                }
            }
            else //COLL CORNERS
            {
                var s0 = this.DistBetC_P(pl.c, v0, pl.r);
                var s1 = this.DistBetC_P(pl.c, v1, pl.r);
                var normV;
                if(s0 != -1) {
                    normV = this.norm(this.subt1_2(pl.c, v0));
                    Pl.Move(pl, s0, normV);
                    IsStuck = true;// PROBABLE BUG PROBABLE BUG PROBABLE BUG PROBABLE BUG PROBABLE BUG PROBABLE BUG PROBABLE BUG
                }
                else if(s1 != -1) {
                    normV = this.norm(this.subt1_2(pl.c, v1));
                    Pl.Move(pl, s1, normV);
                    IsStuck = true;// PROBABLE BUG PROBABLE BUG PROBABLE BUG PROBABLE BUG PROBABLE BUG PROBABLE BUG PROBABLE BUG
                }

                var dt = this.DistBetP_C(v1, this.neg(pl.norm_velocity), pl.c, pl.r);
                d = this.DistBetP_C(v0, this.neg(pl.norm_velocity), pl.c, pl.r);
                if( d == -1 || (dt < d && dt != -1) ) { d = dt; v0 = v1; }
                if( d != -1 && d < mind) {
                    norm = this.norm(this.subt1_2(pl.c, this.add(v0, this.mult(this.neg(pl.norm_velocity), d))));
                    CcollP = this.subt1_2(pl.c, this.mult(norm, pl.r) );
                    ///
                    if(this.ContactList.length > 0) {
                        if( (this.ContactList[this.ContactList.length-1].ind1 == inda && this.ContactList[this.ContactList.length-1].ind2 == indb) ||
                            (this.ContactList[this.ContactList.length-1].ind1 == indb && this.ContactList[this.ContactList.length-1].ind2 == inda) )
                        {
                            this.ContactList.pop();
                        }
                    }
                    ///
                    this.ContactList.push(new Contact(norm, this.add(CcollP, pl.velocity), v0, d, inda, indb));
                    mind = d;
                }
            }
        }
        else { //Sphere not in plane
            d = this.DistBetP_L(CcollP, pl.norm_velocity, v0, norm);
            TcollP = this.add(CcollP, this.mult(pl.norm_velocity, d));

            if(d >= 0)
            {
                if(this.IsPinW(TcollP, v1, v0) && d < mind)//COLL PLANE
                {
                    //Sphere coll with wall
                    ///
                    if(this.ContactList.length > 0) {
                        if( (this.ContactList[this.ContactList.length-1].ind1 == inda && this.ContactList[this.ContactList.length-1].ind2 == indb) ||
                            (this.ContactList[this.ContactList.length-1].ind1 == indb && this.ContactList[this.ContactList.length-1].ind2 == inda) )
                        {
                            this.ContactList.pop();
                        }
                    }
                    ///

                    this.ContactList.push(new Contact(norm, this.add(CcollP, pl.velocity), TcollP, d, inda, indb));
                    mind = d;
                }
                else //COLL CORNERS
                {
                    var dt = this.DistBetP_C(v1, this.neg(pl.norm_velocity), pl.c, pl.r);
                    d = this.DistBetP_C(v0, this.neg(pl.norm_velocity), pl.c, pl.r);
                    if( d == -1 || (dt < d && dt != -1) ) { d = dt; v0 = v1; }
                    if( d != -1 && d < mind) {
                        norm = this.norm(this.subt1_2(pl.c, this.add(v0, this.mult(this.neg(pl.norm_velocity), d))));
                        CcollP = this.subt1_2(pl.c, this.mult(norm, pl.r) );
                        ///
                        if(this.ContactList.length > 0) {
                            if( (this.ContactList[this.ContactList.length-1].ind1 == inda && this.ContactList[this.ContactList.length-1].ind2 == indb) ||
                                (this.ContactList[this.ContactList.length-1].ind1 == indb && this.ContactList[this.ContactList.length-1].ind2 == inda) )
                            {
                                this.ContactList.pop();
                            }
                        }
                        ///
                        this.ContactList.push(new Contact(norm, this.add(CcollP, pl.velocity), v0, d, inda, indb));
                        mind = d;
                    }
                }
            }
        }

        return mind;
    },
    PlsCollDo: function(obj1, obj2) {

        var Sd = this.DistBetC_P(obj1.c, obj2.c, (obj1.r+obj2.r) )/2;
        if(Sd != -0.5) {
            if(Sd < 0.0005) {
                Sd = 0.0005;
            }


            var Snorm = this.norm(this.subt1_2(obj1.c, obj2.c));
            if(Snorm.x == 0 && Snorm.y == 0) {
                Snorm.x = 1; Snorm.y = 1;
                Snorm = this.norm(Snorm);
            }

            Pl.Move(obj1, 1, this.mult(Snorm, Sd));
            Pl.Move(obj2, 1, this.mult(this.neg(Snorm), Sd));
            IsStuck = true;
        }

        var relV = this.norm(this.subt1_2(obj1.velocity, obj2.velocity));
        var d = this.DistBetP_C(obj1.c, relV, obj2.c, (obj1.r+obj2.r) );
        if(d != -1 && d <= obj1.speed) {/////POSIBLE ERROR POSIBLE ERROR POSIBLE ERROR POSIBLE ERROR POSIBLE ERROR POSIBLE ERROR POSIBLE ERROR
            return d;
        }
        return -1;
    },
    MainResolve: function(objects) {
        var Thecounter = 0;
        do {
            IsStuck = false;
            Thecounter++;
            var indexes = [];
            var v1; var v2;
            for (var i = 0; i < objects.length; ++i) {
                var columns = [];
                for (var j = 0; j < objects.length; ++j) {
                    if(i == j) {
                        columns[j] = -1;
                    }
                    else if(objects[i].type == "wl") {
                        if(objects[j].type == "pl") {
                            v1 = objects[i].midVert; v2 = objects[j].c;
                            if(this.len(this.subt1_2(v1, v2)) < 600) {
                                columns[j] = 4;
                            } else { columns[j] = -1; }
                        }
                    }
                    else {
                        if(objects[j].type == "wl") {
                            v1 = objects[i].c; v2 = objects[j].midVert;
                            if(this.len(this.subt1_2(v1, v2)) < 600) {
                                columns[j] = 4;
                            } else { columns[j] = -1; }
                        }
                        else {
                            v1 = objects[i].c; v2 = objects[j].c;
                            if(this.len(this.subt1_2(v1, v2)) < (2*(objects[i].r+objects[j].r))+10) {
                                columns[j] = 4;
                            } else { columns[j] = -1; }
                        }
                    }
                }
                indexes[i] = columns;
            }

            for(i = 0; i < objects.length; i++) {
                for(j = 0; j < objects.length; j++) {
                    if(i != j && indexes[j][i] == 4) {
                        if(objects[i].type == "wl") {
                            if(objects[j].type == "pl") {
                                //Wall coll check
                                indexes[i][j] = this.IsWlsColl(objects[i].vertices, objects[j], i, j);
                            }
                        }
                        else {
                            if(objects[j].type == "wl") {
                                //Wall coll check
                                indexes[i][j] = this.IsWlsColl(objects[j].vertices, objects[i], j, i);
                            }
                            else {
                                //Pl/Pl coll resolve
                                indexes[i][j] = this.PlsCollDo(objects[i], objects[j]);
                            }
                        }
                    }
                    else {
                        indexes[i][j] = indexes[j][i];
                    }
                }
            }

            var min = [];
            var minV;
            var ind;
            for(i = 0; i < indexes.length; i++) {
                minV = 100;
                ind = -1;
                for(j = 0; j < indexes.length; j++) {
                    if(indexes[i][j] != -1 && indexes[i][j] < minV) {
                        minV = indexes[i][j];
                        ind = j;
                    }
                }
                if(ind != -1 ) {
                    min[min.length] = [minV, i, ind];
                }
            }
            if(min.length != 0) {
            for(var curr = 0; curr < min.length; curr++) {
                for(var checked = 0; checked < min.length; checked++) {
                    if(curr != checked && min[curr][0] != -1 && min[checked][0] != -1 &&
                            min[curr][0] == min[checked][0] &&
                            min[curr][2] == min[checked][1] &&
                            min[curr][1] == min[checked][2] ) {

                        if(objects[min[curr][1]].type == "wl") {
                            if(objects[min[curr][2]].type == "pl") {
                                //Wall coll check
                                this.WlPlCollResolve(objects[min[curr][2]].k, objects[min[curr][2]], this.FindContact(min[curr][1], min[curr][2]));

                                for(i = 0; i < min.length; i++) {
                                    if(min[i][1] == min[curr][2] || min[i][2] == min[curr][2]) {
                                        min[i][0] = -1; }
                                }
                            }
                        }
                        else {
                            if(objects[min[curr][2]].type == "wl") {
                                //Wall coll check
                                this.WlPlCollResolve(objects[min[curr][1]].k, objects[min[curr][1]], this.FindContact(min[curr][2], min[curr][1]));

                                for(i = 0; i < min.length; i++) {
                                    if(min[i][1] == min[curr][1] || min[i][2] == min[curr][1]) {
                                        min[i][0] = -1; }
                                }
                            }
                            else {
                                //Pl/Pl coll resolve
                                this.PlPlResolve(objects[min[curr][1]].k, objects[min[curr][2]].k, objects[min[curr][1]], objects[min[curr][2]], min[curr][0]);

                                for(i = 0; i < min.length; i++) {
                                    if(min[i][1] == min[curr][1] || min[i][2] == min[curr][1] ||
                                       min[i][1] == min[curr][2] || min[i][2] == min[curr][2] ) {
                                        min[i][0] = -1; }
                                }
                            }
                        }
                    }
                }
            }
            }

            this.ContactList.splice(0, this.ContactList.length);

        } while(Thecounter < 15 && (min.length != 0 || IsStuck == true))

        for(i = 0; i < objects.length; i++) {
            if(objects[i].type == "pl") {
                objects[i].k = 1;
            }
        }
    },
    FilterPossibleCollObjs: function(obj, objects) {
        let possible_coll_objs = [];
        for (let i = 0; i < objects.length; i++) {
            if(obj === objects[i]) continue;
    
            if(Controller.BoxBoxCollDet(obj.bb, objects[i].bb)) possible_coll_objs.push(objects[i]);
        }
        return possible_coll_objs;
    },
    PlRunCollCycle: function(pl, objects) {
        const contenders = this.FilterPossibleCollObjs(pl, objects);

        for (let coll_cycles = 0; coll_cycles < 3; coll_cycles++) {
            IsStuck = false;

            const coll_distances = [];
            for (let i = 0; i < contenders.length; i++) {
                const contender = contenders[i];
                if(contender.type == "wl")
                {
                    coll_distances.push(this.IsWlsColl(contender.vertices, pl, i, -1));
                }
                else
                {
                    // Push arr in any situation cause we need it to have same length as contenders
                    coll_distances.push(-1);
                }
            }

            let min_dist = Infinity;
            let coll_obj_index = -1;
            for (let i = 0; i < coll_distances.length; i++) {
                if(coll_distances[i] != -1 && coll_distances[i] < min_dist)
                {
                    min_dist = coll_distances[i];
                    coll_obj_index = i;
                }
            }

            if(coll_obj_index != -1) {
                this.WlPlCollResolve(pl.k, pl, this.FindContact(coll_obj_index, -1));
            }

            this.ContactList.splice(0, this.ContactList.length);

            if (coll_obj_index === -1 && !IsStuck) break;
        }

        pl.k = 1;
        for(let i = 0; i < contenders.length; i++)
        {
            if(contenders[i].type == "pl") contenders[i].k = 1;
        }
    },
    FindContact: function(a, b) {
        for(var i = 0; i < this.ContactList.length; i++) {
            if(this.ContactList[i].ind1 == a && this.ContactList[i].ind2 == b) {
                return this.ContactList[i];
            }
        }
    },
    PlPlResolve: function(n1, n2, obj1, obj2, t) {
        var relV = this.norm(this.subt1_2(obj1.velocity, obj2.velocity));
        var block = this.len(this.subt1_2(obj1.velocity, obj2.velocity));
        var k;
        if(block != 0) {
            k = this.len(this.mult(relV, t)) / block;
        }
        var BefCollv1 = this.mult(obj1.velocity, k);
        var BefCollv2 = this.mult(obj2.velocity, k);
        Pl.Move(obj1, 1, this.mult(obj1.velocity, k));
        Pl.Move(obj2, 1, this.mult(obj2.velocity, k));
        var norm = this.norm(this.subt1_2(obj2.c, obj1.c));

        if(this.dot(norm, obj1.norm_velocity) > 0) {
            if(n1 == 1) {
                obj1.velocity = this.subt1_2(obj1.velocity, BefCollv1);
                obj1.velocity = this.subt1_2(obj1.velocity, this.mult(norm, this.dot(norm, obj1.velocity)));
                obj1.norm_velocity = this.norm(obj1.velocity);
                obj1.k = 2;
            } else {
                obj1.velocity = { x: 0, y: 0 };
            }
        }
        if(this.dot(norm, obj2.norm_velocity) < 0) {
            if(n2 == 1) {
                obj2.velocity = this.subt1_2(obj2.velocity, BefCollv2);
                obj2.velocity = this.subt1_2(obj2.velocity, this.mult(norm, this.dot(norm, obj2.velocity)));
                obj2.norm_velocity = this.norm(obj2.velocity);
                obj2.k = 2;
            } else {
                obj2.velocity = { x: 0, y: 0 };
            }
        }
    },
    WlPlCollResolve: function(n, obj, contact) {
        var collVtoPlane;
        var newDestP;
        if(contact.distToContactP != -1) {
            if(n == 1) {
                collVtoPlane = this.mult(obj.norm_velocity, contact.distToContactP);
                var dot0 = this.dot(contact.contactP, contact.contN);
    	        var dot1 = this.dot(contact.destP, contact.contN);
    	        var dist = dot0 - dot1;
    	        newDestP = this.add(contact.destP, (this.mult(contact.contN, dist)));
    	        newDestP = this.subt1_2(newDestP, contact.contactP);
    	        obj.velocity = this.add(newDestP, collVtoPlane);
    	        obj.norm_velocity = this.norm(obj.velocity);
    	        obj.k = 2;
            }
            else {
                collVtoPlane = this.mult(obj.norm_velocity, contact.distToContactP);
                obj.velocity = collVtoPlane;
                obj.norm_velocity = this.norm(obj.velocity);
            }
            contact.distToContactP = -1;
        }
    },
    DistBetP_Sq: function(P, lenV) {
        var lx = lenV.x * 0.5;
        var ly = lenV.y * 0.5;
        if(P.x < 0 || P.x >= lenV.x) {
            P.x = lx + (P.x % lx);
        }
        if(P.y < 0 || P.y >= lenV.y) {
            P.y = ly + (P.y % ly);
        }
        return P;
    },
    DistBetP_L: function(P, destV, PlaneP, PlaneN) {
        var PlaneP_proj = this.dot(PlaneN, PlaneP);
        var proj_dist = this.dot(PlaneN, P) - PlaneP_proj;
        var cos = this.dot(this.neg(PlaneN), destV);//cos is always >= 0

        if(cos < 0.0005) return -1;
        if(proj_dist < 0) {proj_dist = 0;}//Float innacuracy stuff

        //if (cos <= 0) return -1;////Float innacuracy stuff and paralell moving

        return (proj_dist / cos);
    },
    DistBetP_C: function(P, dir, Cc, r) {
        var centV = this.subt1_2(Cc, P);

        var hyp = this.len(centV);
        var cathe = this.dot(centV, dir);

        if (cathe < 0.0005) { return -1; }

        var d = r*r - (hyp*hyp - cathe*cathe);

        if (d < 0) { return -1; }

        return cathe - Math.sqrt(d);
    },
    DistBetC_P: function(c, p, r) {
        var d = this.len(this.subt1_2(c, p));
        if(d < r) {
            return (r - d);
        }
        return -1;
    },
    IsPinW: function(P, p1, p0) {
        var edgeV = this.subt1_2(p1, p0);
        var PV = this.subt1_2(P, p0);
        if(this.dot(edgeV, PV) >= 0 && this.len(PV) <= this.len(edgeV)) {
            return true;
        }
        return false;
    },
    IsinTPl: function(sc, norm, tv) {
        var interP = this.subt1_2(sc, norm);
        if( this.dot(this.subt1_2(tv, interP), norm) > 0 ) {
            return true;
        }
        return false;
    }
};

module.exports = Controller;

const Pl = require("./Player");