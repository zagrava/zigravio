const Controller = require("./Controller");

function v(a, b) {
    this.x = a;
    this.y = b;
}

const Pl = {
    player: function(obj, data)
    {
        obj.x = data.x;
        obj.y = data.y;
        obj.r = data.r;
        obj.c = new v(obj.x + obj.r, obj.y + obj.r);
        obj.type = "pl";

        obj.norm_velocity = new v(0, 0);
        obj.velocity = new v(0, 0);
        obj.dir = new v(1, 0);
        obj.blink = false;
        obj.fire = false;
        obj.respawn = false;

        obj.hp = data.hp;
        obj.hp_max = data.hp_max;
        obj.hp_regen = data.hp_regen;
        obj.hp_regen_threshold = data.hp_regen_threshold;
        obj.dmg = data.dmg;
        obj.speed = data.speed;
        obj.blinkspeedV = data.blinkspeedV;
        obj.blink_energycost = data.blink_energycost;
        obj.range = data.range;
        obj.energy = data.energy;
        obj.energy_regen = data.energy_regen;
        obj.max_energy = data.max_energy;
        obj.undamaged = 0;

        obj.id = data.id;
        obj.name = data.name;
        obj.color = data.color;
        obj.skin = data.skin;
        obj.aimskin = data.aimskin;
        obj.ammoskin = data.ammoskin;

        obj.k = 1;
    },

    plSetClientData: function(obj, data)
    {
        // Client-need data
        obj.synclist = [];
        obj.desync = 0;
        // Client-need statistics
        obj.kills = data.kills;
    },
    
    SetBoundingBox: function(pl, bb_r) {
        pl.bb = Controller.CreateBoundingBox(pl.c.y - bb_r, pl.c.y + bb_r, pl.c.x - bb_r, pl.c.x + bb_r);
    },

    TransformData: function(to, from, trans_phys, trans_intentions, trans_gamedata, trans_generaldata, trans_stats)
    {
        // Transform Physique
        if(trans_phys) {
            if (typeof(to.c) !== 'object') to.c = new v(0, 0);
            
            to.x = from.x;
            to.y = from.y;
            to.c.x = from.c.x;
            to.c.y = from.c.y;
            to.r = from.r;
            to.type = from.type;
        }

        // Transform Intentions
        if(trans_intentions) {
            if (typeof(to.norm_velocity) !== 'object') to.norm_velocity = new v(0, 0);
            if (typeof(to.dir) !== 'object') to.dir = new v(0, 0);

            to.norm_velocity.x = from.norm_velocity.x;
            to.norm_velocity.y = from.norm_velocity.y;
            to.velocity = Controller.mult(to.norm_velocity, to.speed); // speed is used before it's tranflated
            to.dir.x = from.dir.x;
            to.dir.y = from.dir.y;
            to.blink = from.blink;
            to.fire = from.fire;
            to.respawn = from.respawn;
        }

        // Transform Gamedata
        if(trans_gamedata) {
            to.hp = from.hp;
            to.hp_max = from.hp_max;
            to.hp_regen = from.hp_regen;
            to.hp_regen_threshold = from.hp_regen_threshold;
            to.dmg = from.dmg;
            to.speed = from.speed;
            to.blinkspeedV = from.blinkspeedV;
            to.blink_energycost = from.blink_energycost;
            to.range = from.range;
            to.energy = from.energy;
            to.energy_regen = from.energy_regen;
            to.max_energy = from.max_energy;
            to.undamaged = from.undamaged;

            to.type = from.type;
        }

        // Transform General Data
        if(trans_generaldata) {
            to.id = from.id;
            to.name = from.name;
            to.color = from.color;
            to.skin = from.skin;
            to.aimskin = from.aimskin;
            to.ammoskin = from.ammoskin;
        }

        // Transform Statistics
        if(trans_stats) {
            to.kills = from.kills;
        }

        return to;
    },

    AcceptSyncData: function(players, timelist, sessionid, data) {
        const GameData = data[0];
    
        timelist.push({
            t: 0,
            index: 0,
            reaction_num: GameData.reaction_num
        });
    
        let newplayers = [];
        for(let i = 1; i < data.length; i++) {
            const Data = data[i];
            
            let pl = players.find(function( obj ) {
                return obj.id == Data.id;
            });
    
            if(typeof pl === "undefined") {
                console.log("NEW PLAYER!");

                let newpl = {};
                Pl.player(newpl, Data);
                Pl.plSetClientData(newpl, Data);

                pl = newpl;
            }
    
            newplayers.push(pl);
    
            // Accept data
            pl.hp = Data.hp;
            pl.kills = Data.kills;
    
            // Accept synclist data
            if(pl.id == sessionid)
            {
                Pl.AcceptServerReactResults(pl, Data.synclist);
            }
            else
            {
                for(var j = 0; j < Data.synclist.length; j++) {
                    pl.synclist.push(Data.synclist[j]);
                }
            }
        }

        return newplayers;

    },

    CreateReactArrAndLSeenFrame: function(timelist, SEENTIME, max_react_num) {
        const reaction_arr = []; let last_frame_seen = -1;
        for (let i = timelist.length-1; i >= 0 && reaction_arr.length < max_react_num; i--) {
            if(timelist[i].t < SEENTIME) {
                reaction_arr.unshift(timelist[i].reaction_num);

                if(last_frame_seen == -1) last_frame_seen = i;
            }
        }
        return [reaction_arr, last_frame_seen];
    },

    AlignTimelist: function(timelist, SEENTIME, now, FRAMETIME, PingIn) {
        if(timelist.length == 0) return;

        timelist[0].t = SEENTIME - (FRAMETIME / 2);
        for(let i = 1; i < timelist.length; i++) {
            if(PingIn == 0) {
                timelist[i].index = 0;
            }

            if(timelist[i].index == 1) {
                timelist[i].t = timelist[i-1].t;
            } else {
                timelist[i].t = timelist[i-1].t + FRAMETIME;
            }

            if(timelist[i].t > now) {
                timelist[i].t = timelist[i-1].t;
                timelist[i].index = 1;
            }
        }

    },

    GetDataFromSynclist: function(pl, last_frame_seen) {
        if(last_frame_seen === -1
        || last_frame_seen > pl.synclist.length-1
        || pl.synclist[last_frame_seen] === undefined) {
            return;
        }

        pl.dir.x = pl.synclist[last_frame_seen].dir.x;
        pl.dir.y = pl.synclist[last_frame_seen].dir.y;

        pl.x = pl.synclist[last_frame_seen].x;
        pl.y = pl.synclist[last_frame_seen].y;

        pl.c.x = pl.x + pl.r;
        pl.c.y = pl.y + pl.r;

        pl.fire = pl.synclist[last_frame_seen].fire;
    },

    AcceptServerReactResults: function(main, server_synclist) {
        if(server_synclist.length === 0) return;
        const last_server_sync = server_synclist[server_synclist.length-1];

        // Clear solved reactions
        for (let i = main.synclist.length-1; i >= 0; i--) {
            if(main.synclist[i].last_react_sent === last_server_sync.last_react_processed)
            {
                main.synclist.splice(0, i+1);
                break;
            }
        }
        
        // Apply latest sync data
        main.x = last_server_sync.x;
        main.y = last_server_sync.y;
        main.c.x = main.x + main.r;
        main.c.y = main.y + main.r;
        main.energy = last_server_sync.energy;

        // Apply pending reactions
        for(let i = 0; i < main.synclist.length; i++) {
            Pl.Move(main, 1, Controller.mult(main.synclist[i].poschange, 1) );
            main.energy += main.synclist[i].energychange;
        }
    },

    AdjustSynclist: function(pl, timelist, align_threshold) {
        if(pl.synclist.length === timelist.length) {
            pl.desync = 0;
            return;
        }

        let delta = Math.abs(pl.synclist.length - timelist.length);
        pl.desync += delta;
        if(pl.desync < align_threshold) return;

        if(pl.synclist.length < timelist.length) {
            while(pl.synclist.length < timelist.length) pl.synclist.push(undefined);
        } else {
            pl.synclist.splice(0, delta);
        }
    },

    DoesShootPlayer: function(pl, hitlist) {
        if(!Pl.CanHeFire(pl) || !pl.fire) return false;

        for (let i = 0; i < hitlist.length; i++) {
            if(hitlist[i].obj.type == "pl" && Pl.IsHeAlive(hitlist[i].obj) )
            {
                return true;
            }
            else if(hitlist[i].obj.type == "wl")
            {
                return false;
            }
        }

        return false;
    },

    Move: function(pl, dist = 1, moveV = pl.velocity)
    {
        pl.x += moveV.x * dist;
        pl.y += moveV.y * dist;

        pl.c.x = pl.x + pl.r;
        pl.c.y = pl.y + pl.r;
        moveV.x = 0;
        moveV.y = 0;
    },

    PlayerTick: function(pl)
    {
        pl.undamaged = Math.min(200, pl.undamaged + 1);
    },

    Blink: function(pl)
    {
        if(pl.blink && pl.blink_energycost <= pl.energy) {
            Pl.Move(pl, pl.blinkspeedV, pl.norm_velocity);
            pl.norm_velocity = Controller.norm(pl.velocity);
            pl.energy -= pl.blink_energycost;
        }
    },

    Respawn: function(room, pl) {
        if(pl.respawn) {
            pl.x = Math.random() * room.width;
            pl.y = Math.random() * room.height;
            pl.hp = pl.hp_max;
            pl.energy = pl.max_energy;
            pl.undamaged = 0;
        }
    },

    EnergyRegen: function(pl) {
        pl.energy = Math.min(pl.max_energy, pl.energy + pl.energy_regen);
    },

    HpRegen: function(pl)
    {
        if(pl.undamaged >= pl.hp_regen_threshold && pl.hp > 0) {
            pl.hp = Math.min(pl.hp_max, pl.hp + pl.hp_regen);
        }
    },

    CanHePlay: function(pl) {
        return true;
    },

    CanHeMove: function(pl) {
        return this.IsHeAlive(pl);
    },

    CanHeBlink: function(pl) {
        return this.IsHeAlive(pl);
    },

    CanHeFire: function(pl) {
        return this.IsHeAlive(pl);
    },

    IsHeAlive: function(pl) {
        if(pl.hp > 0) {
            return true;
        }

        return false;
    },

    HandleCommands: function(pl, keys, eye) {
        if(!Pl.CanHePlay(pl)) return;

        // VELOCITY
        if (keys.left) pl.velocity.x += -1;
        if (keys.right) pl.velocity.x += 1;
        if (keys.up) pl.velocity.y += -1;
        if (keys.down) pl.velocity.y += 1;

        pl.norm_velocity = Controller.norm(pl.velocity);
        pl.velocity = Controller.mult(pl.norm_velocity, pl.speed);

        // DIRECTION
        pl.dir = Controller.norm( {x: keys.mouse.x - (pl.c.x - eye.x),
                                   y: keys.mouse.y - (pl.c.y - eye.y) } );
        
        // SHOOT
        if (keys.shoot) {
            pl.fire = true;
        } else {
            pl.fire = false;
        }

        // BLINK
        if (!keys.is_blink_pressed && keys.blink) {
            pl.blink = true;
            keys.is_blink_pressed = true;
        } else {
            pl.blink = false;
        }

        // RESPAWN
        if(keys.respawn) {
            pl.respawn = true;
            keys.respawn = false;
        } else {
            pl.respawn = false;
        }
    }

};

module.exports = Pl;