'use strict';

const ServerController = require('./ServerController');
const Controller = require('./Controller');
const walls = require('./Warehouse');
const Pl = require("./Player");

const RoomRuler = {

    IsSyncDataOk: function(data)
    {
        if( typeof(data) !== 'object' ||
            typeof(data.left) !== 'boolean' ||
            typeof(data.right) !== 'boolean' ||
            typeof(data.up) !== 'boolean' ||
            typeof(data.down) !== 'boolean' ||
            typeof(data.blink) !== 'boolean' ||
            typeof(data.fire) !== 'boolean' ||
            typeof(data.dir) !== 'object' ||
            !Number.isFinite(data.dir.x) ||
            !Number.isFinite(data.dir.y) ||
            !Array.isArray(data.reaction_arr) ||
            data.reaction_arr.length > ServerController.max_react_num )
        {
            return false;
        }

        for (let i = 0; i < data.reaction_arr.length; i++) {
            if(!Number.isFinite(data.reaction_arr[i]) || 
               !(data.reaction_arr[i] >= 0 && data.reaction_arr[i] <= ServerController.ARCHIVELENGTH-1) )
            {
                return false;
            }
        }
        
        return true;
    },

    AcceptAndArchiveInputs: function(socket, data)
    {
        // Check if player is in any room
        const loc = ServerController.FindSocketInSpace(socket);
        if(loc[0] == -1) {
            console.log("Socket is not in any room");
            return;
        }
        // Check if player has sent healthy data
        if(!this.IsSyncDataOk(data)) {
            console.log("Syncdata is corrupted");
            return;
        }

        // Handle player commands
        let Speed = { x: 0, y: 0 };
        if (data.left) {Speed.x += -1; }
        if (data.right) {Speed.x += 1; }
        if (data.up) {Speed.y += -1; }
        if (data.down) {Speed.y += 1; }
        Speed = Controller.norm(Speed);

        for (let i = 0; i < data.reaction_arr.length; i++) {
            const archive_index = ServerController.timelist.findIndex(function( elem ) {
                return elem[0] == data.reaction_arr[i];
            });

            ServerController.space[loc[0]].archive[archive_index][loc[1]].dir.x = data.dir.x;
            ServerController.space[loc[0]].archive[archive_index][loc[1]].dir.y = data.dir.y;
            ServerController.space[loc[0]].archive[archive_index][loc[1]].norm_velocity.x = Speed.x;
            ServerController.space[loc[0]].archive[archive_index][loc[1]].norm_velocity.y = Speed.y;
            ServerController.space[loc[0]].archive[archive_index][loc[1]].blink = data.blink;
            ServerController.space[loc[0]].archive[archive_index][loc[1]].fire = data.fire;
            ServerController.space[loc[0]].archive[archive_index][loc[1]].respawn = data.respawn;

            data.blink = false;

            socket.react_indexes.push(archive_index);
            ServerController.space[loc[0]].farest_index = Math.min(archive_index, ServerController.space[loc[0]].farest_index);
        }
    },

    ResolveHitlist: function(shooter, hitlist, archive_moment)
    {
        if(shooter.hp <= 0 || !shooter.fire) {
            return;
        }

        for(let j = 0; j < hitlist.length; j++)
        {
            let hit = hitlist[j];
            
            if(hit.obj.type == "pl" && hit.obj.hp > 0)
            {
                this.ObjDamageObj(hit, shooter, archive_moment);
                break;
            }
            else if(hit.obj.type == "wl")
            {
                break;
            }
        }
    },

    ObjDamageObj: function(victim_shot_info, offender, archive_moment)
    {
        victim_shot_info.obj.hp -= offender.dmg;
        victim_shot_info.obj.undamaged = 0;

        if(archive_moment !== undefined) {
            archive_moment[victim_shot_info.index].influence.dmg.push(offender);
        }
    },

    ProcessInputs: function(room)
    {
        const objects = room.objects;
	    const archive = room.archive;
        const gamehistory = room.gamehistory;
        const changeFrom = room.farest_index; room.farest_index = ServerController.ARCHIVELENGTH-1;

        let objs_history = [];
        let objs_walls = [];

        for (let j = 0; j < objects.length; j++) {
            Pl.TransformData(objects[j], archive[changeFrom][j], true, false, true, false, false);
            objs_history.push({});
        }

        objs_history = objs_history.concat(walls[room.type]);
        objs_walls = objects.concat(walls[room.type]);

        for(let i = changeFrom; i < archive.length; i++)
        {
            for(let j = 0; j < objects.length; j++)
            {
                //Set modified data to the next step of the archive
                Pl.TransformData(archive[i][j], objects[j], true, false, true, false, false);

                //Clear the data that needs to be recalculated
                archive[i][j].influence.dmg = [];

                //Get player intentions from this step of the archive
                Pl.TransformData(objects[j], archive[i][j], false, true, false, false, false);

                //Get the data that was sent to the clients from the server
                Pl.TransformData(objs_history[j], gamehistory[i][j], true, false, false, false, false);
            }

            //Змінюємо об'єкти за новими інпутами
            for(let j = 0; j < objects.length; j++)
            {
                //Calculate min dist to wounded object if the shot was succesful and possible
                const hitlist = Controller.ObjFiring(objs_history, j, objs_walls);

                this.ResolveHitlist(objects[j], hitlist, archive[i]);
            }

            for(let j = 0; j < objects.length; j++)
            {
                // Resolve player buffs/debuffs/skills/cooldowns
                if(!Pl.IsHeAlive(objects[j]) && room.type === "FFA") {
                    Pl.Respawn(room, objects[j]);
                }

                if(Pl.IsHeAlive(objects[j])) {
                    Pl.EnergyRegen(objects[j]);
                    Pl.PlayerTick(objects[j]);
                    Pl.HpRegen(objects[j]);
                }
                
                if(Pl.CanHeBlink(objects[j])) {
                    Pl.Blink(objects[j]);
                }
                Controller.ZoneClip(objects[j], room.width, room.height);

                // Collision stuff
                Pl.SetBoundingBox(objects[j], objects[j].r+Controller.len(objects[j].velocity)+1);
                if(Pl.CanHeMove(objects[j])) {
                    Controller.PlRunCollCycle(objects[j], walls[room.type]);
                    Pl.Move(objects[j]);
                }
            }

        }

    },

    SendData: function(room, FrameTime)
    {
        const objects = room.objects;
        const archive = room.archive;

        for(let k = 0; k < objects.length; k++)
        {
            var alldata = [];
            var gamedata = {
                time: "TEST",
                gamestuff: "TEST1",
                reaction_num: ServerController.timelist[ServerController.timelist.length-1][0]
            };

            alldata.push(gamedata);

            for(let i = 0; i < objects.length; i++)
            {
                const objdata = {};

                ///INITDATA///
                if(objects[i].logintime == FrameTime) {
                    Pl.TransformData(objdata, objects[i], true, false, true, true, true);
                }
                ///INITDATA///

                // ID
                objdata.id = objects[i].id;
                // ID

                // STATS
                objdata.kills = objects[i].kills;
                // STATS

                // HP
                if(archive[1][i].hp <= 0) {
                    objdata.hp = 0;
                } else {
                    objdata.hp = 1;
                }
                if(i == k) {
                    objdata.hp = archive[1][i].hp;
                }
                // HP
                
                // SYNCLIST
                objdata.synclist = [];
                for(let j = 0; j < objects[i].react_indexes.length; j++)
                {
                    objdata.synclist.push({
                        x:             archive[ objects[i].react_indexes[j]+1 ][i].x,
                        y:             archive[ objects[i].react_indexes[j]+1 ][i].y,
                        energy:        archive[ objects[i].react_indexes[j]+1 ][i].energy,
                        dir:           archive[ objects[i].react_indexes[j]   ][i].dir,
                        norm_velocity: archive[ objects[i].react_indexes[j]   ][i].norm_velocity,
                        blink:         archive[ objects[i].react_indexes[j]   ][i].blink,
                        fire:          archive[ objects[i].react_indexes[j]   ][i].fire,
                    });

                    if(j === objects[i].react_indexes.length - 1) {
                        objdata.synclist[j].last_react_processed = ServerController.timelist[objects[i].react_indexes[j]][0];
                    }
                }
                // SYNCLIST

                alldata.push(objdata);
            }

            objects[k].emit('sync', alldata);
        }
    },



};

module.exports = RoomRuler;