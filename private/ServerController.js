const walls = require('./Warehouse');
const Controller = require('./Controller');
const Pl = require('./Player');

const ARCHIVELENGTH = 40;

const ServerController = {
    space: [],
    duelqueue: [],
    ffaqueue: [],
    ARCHIVELENGTH: ARCHIVELENGTH,
    max_react_num: 5,
    timelist: (function FillUpTimelist(ARCHIVELENGTH) {
        const timelist = [];
        for (let i = 0; i < ARCHIVELENGTH; i++) timelist.push([i, 100]);
        return timelist;
    })(ARCHIVELENGTH),
    rooms: {
        DUEL: {
            type: "DUEL",
            width: 1420,
            height: 800
        },
        FFA: {
            type: "FFA",
            width: 3000,
            height: 2000
        }
    },
    plServerData: function(obj, FrameTime) {
        //Server-need data
        obj.react_indexes = [];
        //Statistics
        obj.kills = 0;
        obj.deaths = 0;
        obj.accuracy = 0;
        obj.logintime = FrameTime;
    },
    IsEnterDataOk: function(data) {
        if( typeof(data) !== 'object' ||
            typeof(data.type) !== 'string' ||
            typeof(data.name) !== 'string' )
        {
            return false;
        }
        
        return true;
    },
    IsAppropriate: function(room, type) {
        return (room.type == type && room.enterable && room.objects.length < room.max);
    },
    CreateRoom: function(r_data) {
        const arch = [];
        for(let number = 0; number < this.ARCHIVELENGTH; number++) arch.push([]);

        const gmhist = [];
        for(let number = 0; number < this.ARCHIVELENGTH; number++) gmhist.push([]);

        const room = {
            objects: [],
            archive: arch,
            gamehistory: gmhist,
            width: r_data.width,
            height: r_data.height,
            time: -1,
            type: r_data.type,
            max: 15,
            enterable: true,
            gamestate: { final: -1 },
            farest_index: this.ARCHIVELENGTH-1,
        };

        this.space.push(room);

        return room;
    },
    DeletePlFromQueues: function(player) {
        let index = this.duelqueue.indexOf(player);
        if (index != -1) this.duelqueue.splice(index, 1);

        index = this.ffaqueue.indexOf(player);
        if(index != -1) this.ffaqueue.splice(index, 1);
    },
    DeletePlFromRoom: function(player) {
        const location = this.FindSocketInSpace(player);
        const i = location[0]; const j = location[1];

        if(i != -1) {

            this.space[i].objects.splice(j, 1);
            for(let number = 0; number < this.ARCHIVELENGTH; number++) this.space[i].archive[number].splice(j, 1);
            for(let number = 0; number < this.ARCHIVELENGTH; number++) this.space[i].gamehistory[number].splice(j, 1);

            return true;
        } else {
            console.log("SOCKET IS NOT FOUND");
            return false;
        }

    },
    AddPlToRoom: function(room, socket) {
        room.objects.push(socket);

        for(let number = 0; number < this.ARCHIVELENGTH; number++) {
            const arch_obj_moment = {
                norm_velocity: {x: 0, y: 0},
                dir: {x: 0, y: 0},
                blink: false,
                fire: false,
                
                influence: {
                    dmg: [],
                }
            };

            Pl.TransformData(arch_obj_moment, socket, true, false, true, false, false);
            room.archive[number].push(arch_obj_moment);
        }

        for(let number = 0; number < this.ARCHIVELENGTH; number++) {
            const gamehistory_obj_moment = {};

            Pl.TransformData(gamehistory_obj_moment, socket, true, false, false, false, false);
            room.gamehistory[number].push(gamehistory_obj_moment);
        }

    },
    MainCheckRules: function(room) {
        if(room.type == "DUEL") {
            this.CheckDuelRules(room);
        } else if(room.type == "FFA") {
            this.CheckFFARules(room);
        }
    },
    CheckDuelRules: function(room) {
        if(room.objects.length < 2) {
            room.gamestate.final = 3;
            return;
        }

        if(room.archive[0][0].hp <= 0 && room.archive[0][1].hp <= 0) {
            room.gamestate.final = 2;
        } else if(room.archive[0][0].hp <= 0) {
            room.gamestate.final = 1;
        } else if(room.archive[0][1].hp <= 0) {
            room.gamestate.final = 0;
        }
    },
    CheckFFARules: function(room) {
        for(let i = 0; i < room.objects.length; i++)
        {
            const pl = room.archive[0][i];
            const next_frame_pl = room.archive[1][i];

            for(let j = 0; j < pl.influence.dmg.length; j++)
            {
                const offender = pl.influence.dmg[j];
                const offender_index = room.objects.indexOf(offender);
                if(offender_index === -1) continue;

                if(!Pl.IsHeAlive(next_frame_pl)) offender.kills++;
            }
        }

        if(room.objects.length == 0) {
            room.gamestate.final = 0;
        }
    },
    ResolveQueues: function(FrameTime) {
        ///DUEL QUEUE RESOLVE///
        let envdata = this.GetRoomEnvData(this.rooms["DUEL"]);

        while (this.duelqueue.length > 1) {
            const last_room = this.CreateRoom(this.rooms["DUEL"]);
            
            //FIRST PLAYER//
            this.GeneratePlayer(last_room, this.duelqueue[0], {team: 1}, FrameTime);
            this.AddPlToRoom(last_room, this.duelqueue[0]);
            
            //SECOND PLAYER//
            this.GeneratePlayer(last_room, this.duelqueue[1], {team: 2}, FrameTime);
            this.AddPlToRoom(last_room, this.duelqueue[1]);
            
            // Send data to pls
            let objsdata = this.GetRoomObjsInitData(last_room);
            let alldata = envdata.concat(objsdata);
            last_room.objects[0].emit('entered', alldata);
            last_room.objects[1].emit('entered', alldata);

            this.duelqueue.splice(0, 2);
        }

        ///FFA QUEUE RESOLVE///
        envdata = this.GetRoomEnvData(this.rooms["FFA"]);

        for(let i = 0; i < this.space.length; i++) {
            if(this.ffaqueue.length == 0) break;
            if(!this.IsAppropriate(this.space[i], "FFA")) continue;

            //PLAYER//
            this.GeneratePlayer(this.space[i], this.ffaqueue[0], {}, FrameTime);
            this.AddPlToRoom(this.space[i], this.ffaqueue[0]);

            // Send data to pls
            let objsdata = this.GetRoomObjsInitData(this.space[i]);
            let alldata = envdata.concat(objsdata);
            this.ffaqueue[0].emit('entered', alldata);

            this.ffaqueue.shift();
            i--;
        }
        
        while(this.ffaqueue.length > 0) {
            const last_room = this.CreateRoom(this.rooms["FFA"]);

            while(last_room.objects.length < last_room.max && this.ffaqueue.length > 0) {
                //PLAYER//
                this.GeneratePlayer(last_room, this.ffaqueue[0], {}, FrameTime);
                this.AddPlToRoom(last_room, this.ffaqueue[0]);

                // Send data to pls
                let objsdata = this.GetRoomObjsInitData(last_room);
                let alldata = envdata.concat(objsdata);
                this.ffaqueue[0].emit('entered', alldata);

                this.ffaqueue.shift();
            }
        }

    },
    CloseRoom: function(room) {
        //Find out what type our closing room is
        if(room.type == "DUEL") {
            //Close the duel room
            if(room.gamestate.final == 0) {
                room.objects[0].emit('closeroom', {final: "vic"});
                room.objects[1].emit('closeroom', {final: "def"});
            } else if(room.gamestate.final == 1) {
                room.objects[0].emit('closeroom', {final: "def"});
                room.objects[1].emit('closeroom', {final: "vic"});
            } else if(room.gamestate.final == 2) {
                room.objects[0].emit('closeroom', {final: "draw"});
                room.objects[1].emit('closeroom', {final: "draw"});
            } else if(room.gamestate.final == 3) {
                if(room.objects.length == 1) room.objects[0].emit('closeroom', {final: "vic"});
            }
        }
        
        //Clear the closed room from the space array
        let index = this.space.indexOf(room);
        if (index != -1) this.space.splice(index, 1);
    },
    FindSocketInSpace: function(socket) {
        for(let i = 0; i < this.space.length; i++) {
            const objindex = this.space[i].objects.indexOf(socket);

            if(objindex != -1) return [i, objindex];
        }
        return [-1, -1];
    },
    GetRoomEnvData: function(room) {
        const envdata = [{
            // Server data
            max_react_num: this.max_react_num,
            // Room data
            width: room.width,
            height: room.height,
            type: room.type,
        }].concat(walls[room.type]);

        return envdata;
    },
    GetRoomObjsInitData: function(room) {
        const objsdata = [];
        for(let j = 0; j < room.objects.length; j++) {
            objsdata.push( Pl.TransformData({}, room.objects[j], true, false, true, true, true) );
        }
        return objsdata;
    },
    GeneratePlayer: function(room, pl, metadata, FrameTime) {
        const initdata = {};

        if(room.type === "FFA")
        {
            initdata.x = Math.random()*3000;
            initdata.y = Math.random()*2000;
            initdata.r = 30.5;

            initdata.hp = 450;
            initdata.hp_max = 450;
            initdata.hp_regen = 1;
            initdata.hp_regen_threshold = 200;
            initdata.dmg = 3;
            initdata.speed = 3;
            initdata.blinkspeedV = 100;
            initdata.blink_energycost = 100;
            initdata.range = 1085;
            initdata.energy = 200;
            initdata.energy_regen = 1;
            initdata.max_energy = 200;
            initdata.undamaged = 0;
    
            initdata.id = pl.id;
            initdata.name = pl.name;
            initdata.color = Controller.RandomColor(150);
            initdata.skin = "TEST";
            initdata.aimskin = "AIMTEST";
            initdata.ammoskin = "AMMOTEST";
        }
        else if(room.type === "DUEL")
        {
            if(metadata.team === 1) {
                initdata.x = 10;
                initdata.y = 370;
            } else if(metadata.team === 2) {
                initdata.x = 1370;
                initdata.y = 370;
            }

            initdata.r = 30.5;

            initdata.hp = 450;
            initdata.hp_max = 450;
            initdata.hp_regen = 1;
            initdata.hp_regen_threshold = 200;
            initdata.dmg = 3;
            initdata.speed = 3;
            initdata.blinkspeedV = 100;
            initdata.blink_energycost = 100;
            initdata.range = 1085;
            initdata.energy = 200;
            initdata.energy_regen = 1;
            initdata.max_energy = 200;
            initdata.undamaged = 0;
    
            initdata.id = pl.id;
            initdata.name = pl.name;
            initdata.color = Controller.RandomColor(150);
            initdata.skin = "TEST";
            initdata.aimskin = "AIMTEST";
            initdata.ammoskin = "AMMOTEST";
        }

        Pl.player(pl, initdata);
        this.plServerData(pl, FrameTime);
    }

};

module.exports = ServerController;