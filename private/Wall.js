var Controller = require('./Controller.js');

function v(a, b) {
    this.x = a;
    this.y = b;
}

function wall() {
    this.vertices = [];
    this.type = "wl";
}

wall.prototype.AddV = function(x, y) {
    this.vertices.push(new v(x, y));
};

wall.prototype.FindMidV = function() {
    var midV = this.vertices[0];
    for(var i = 0, l = this.vertices.length - 1; i < l; i++) {
        midV = Controller.mult(Controller.add(midV, this.vertices[i + 1]), 0.5);
    }
    this.midVert = midV;
};

wall.prototype.SetBoundingBox = function() {
    let minX = Infinity; let minY = Infinity;
    let maxX = -Infinity; let maxY = -Infinity;

    for (let i = 0; i < this.vertices.length; i++) {
        const vertex = this.vertices[i];
        minX = Math.min(minX, vertex.x);
        maxX = Math.max(maxX, vertex.x);

        minY = Math.min(minY, vertex.y);
        maxY = Math.max(maxY, vertex.y);
    }

    this.bb = Controller.CreateBoundingBox(minY, maxY, minX, maxX);
};

wall.prototype.CreateCorner = function(length, width) {
    this.vertices.push(new v(0, 0));
    this.vertices.push(new v(length, 0));
    this.vertices.push(new v(length, width));
    this.vertices.push(new v(length-10, width));
    this.vertices.push(new v(length-10, 10));
    this.vertices.push(new v(0, 10));
}

wall.prototype.Move = function(mx, my) {
    for (var i = 0; i < this.vertices.length; i++) {
        this.vertices[i].x += mx;
        this.vertices[i].y += my;
    }
}

wall.prototype.Scale = function(sx, sy) {
    for (var i = 0; i < this.vertices.length; i++) {
        this.vertices[i].x *= sx;
        this.vertices[i].y *= sy;
    }
}

module.exports = wall;