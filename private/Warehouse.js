var wall = require('./Wall');

var walls = {};
walls["DUEL"] = [];
walls["FFA"] = [];

// DUEL DUEL DUEL DUEL DUEL DUEL DUEL DUEL DUEL DUEL DUEL
// DUEL WALL 0
var dwtop = new wall();
dwtop.AddV(0, 0);
dwtop.AddV(1420, 0);
dwtop.AddV(1420, 10);
dwtop.AddV(0, 10);
dwtop.FindMidV(); dwtop.SetBoundingBox();
walls["DUEL"].push(dwtop);

var dwbot = new wall();
dwbot.AddV(0, 790);
dwbot.AddV(1420, 790);
dwbot.AddV(1420, 800);
dwbot.AddV(0, 800);
dwbot.FindMidV(); dwbot.SetBoundingBox();
walls["DUEL"].push(dwbot);

var dwleft = new wall();
dwleft.AddV(0, 10);
dwleft.AddV(10, 10);
dwleft.AddV(10, 790);
dwleft.AddV(0, 790);
dwleft.FindMidV(); dwleft.SetBoundingBox();
walls["DUEL"].push(dwleft);

var dwright = new wall();
dwright.AddV(1410, 10);
dwright.AddV(1420, 10);
dwright.AddV(1420, 790);
dwright.AddV(1410, 790);
dwright.FindMidV(); dwright.SetBoundingBox();
walls["DUEL"].push(dwright);

// DUEL WALL 1
var dw1 = new wall();
dw1.CreateCorner(280, 156);
dw1.Scale(1, -1);
dw1.Move(10, 10);
dw1.Move(280, 156*2);
dw1.FindMidV(); dw1.SetBoundingBox();
walls["DUEL"].push(dw1);

// DUEL WALL 2
var dw2 = new wall();
dw2.CreateCorner(280, 156);
dw2.Scale(-1, -1);
dw2.Move(10, 10);
dw2.Move(280*4, 156*2);
dw2.FindMidV(); dw2.SetBoundingBox();
walls["DUEL"].push(dw2);

// DUEL WALL 3
var dw3 = new wall();
dw3.CreateCorner(280, 156);
dw3.Scale(1, 1);
dw3.Move(10, 10);
dw3.Move(280, 156*3);
dw3.FindMidV(); dw3.SetBoundingBox();
walls["DUEL"].push(dw3);

// DUEL WALL 4
var dw4 = new wall();
dw4.CreateCorner(280, 156);
dw4.Scale(-1, 1);
dw4.Move(10, 10);
dw4.Move(280*4, 156*3);
dw4.FindMidV(); dw4.SetBoundingBox();
walls["DUEL"].push(dw4);



// FFA FFA FFA FFA FFA FFA FFA FFA FFA FFA FFA FFA
// FFA WALL 0
var dwtop = new wall();
dwtop.AddV(0, 0);
dwtop.AddV(3000, 0);
dwtop.AddV(3000, 10);
dwtop.AddV(0, 10);
dwtop.FindMidV(); dwtop.SetBoundingBox();
walls["FFA"].push(dwtop);

var dwbot = new wall();
dwbot.AddV(0, 2000);
dwbot.AddV(3000, 2000);
dwbot.AddV(3000, 1990);
dwbot.AddV(0, 1990);
dwbot.FindMidV(); dwbot.SetBoundingBox();
walls["FFA"].push(dwbot);

var dwleft = new wall();
dwleft.AddV(0, 10);
dwleft.AddV(10, 10);
dwleft.AddV(10, 1990);
dwleft.AddV(0, 1990);
dwleft.FindMidV(); dwleft.SetBoundingBox();
walls["FFA"].push(dwleft);

var dwright = new wall();
dwright.AddV(2990, 10);
dwright.AddV(3000, 10);
dwright.AddV(3000, 1990);
dwright.AddV(2990, 1990);
dwright.FindMidV(); dwright.SetBoundingBox();
walls["FFA"].push(dwright);

let wl0 = new wall();
wl0.AddV(178, 202);
wl0.AddV(275, 202);
wl0.AddV(275, 118);
wl0.AddV(285, 118);
wl0.AddV(285, 212);
wl0.AddV(178, 212);
wl0.FindMidV(); wl0.SetBoundingBox();
walls["FFA"].push(wl0);

wl0 = new wall();
wl0.AddV(312, 333);
wl0.AddV(339, 426);
wl0.AddV(420, 403);
wl0.AddV(423, 412);
wl0.AddV(333, 438);
wl0.AddV(302, 336);
wl0.FindMidV(); wl0.SetBoundingBox();
walls["FFA"].push(wl0);

wl0 = new wall();
wl0.AddV(645, 246);
wl0.AddV(652, 253);
wl0.AddV(583, 321);
wl0.AddV(640, 381);
wl0.AddV(633, 388);
wl0.AddV(568, 320);
wl0.FindMidV(); wl0.SetBoundingBox();
walls["FFA"].push(wl0);

wl0 = new wall();
wl0.AddV(219, 696);
wl0.AddV(314, 743);
wl0.AddV(309, 752);
wl0.AddV(223, 709);
wl0.AddV(185, 784);
wl0.AddV(176, 780);
wl0.FindMidV(); wl0.SetBoundingBox();
walls["FFA"].push(wl0);

wl0 = new wall();
wl0.AddV(144, 1155);
wl0.AddV(207, 1226);
wl0.AddV(201, 1233);
wl0.AddV(138, 1161);
wl0.FindMidV(); wl0.SetBoundingBox();
walls["FFA"].push(wl0);

wl0 = new wall();
wl0.AddV(575, 1331);
wl0.AddV(573, 1339);
wl0.AddV(475, 1308);
wl0.AddV(477, 1300);
wl0.FindMidV(); wl0.SetBoundingBox();
walls["FFA"].push(wl0);

wl0 = new wall();
wl0.AddV(272, 1469);
wl0.AddV(308, 1561);
wl0.AddV(300, 1565);
wl0.AddV(264, 1473);
wl0.FindMidV(); wl0.SetBoundingBox();
walls["FFA"].push(wl0);

wl0 = new wall();
wl0.AddV(644, 1644);
wl0.AddV(754, 1594);
wl0.AddV(820, 1742);
wl0.AddV(812, 1746);
wl0.AddV(750, 1608);
wl0.AddV(648, 1652);
wl0.FindMidV(); wl0.SetBoundingBox();
walls["FFA"].push(wl0);

wl0 = new wall();
wl0.AddV(711, 887);
wl0.AddV(701, 887);
wl0.AddV(701, 1118);
wl0.AddV(711, 1118);
wl0.FindMidV(); wl0.SetBoundingBox();
walls["FFA"].push(wl0);

wl0 = new wall();
wl0.AddV(774, 820);
wl0.AddV(774, 830);
wl0.AddV(1140, 830);
wl0.AddV(1140, 820);
wl0.FindMidV(); wl0.SetBoundingBox();
walls["FFA"].push(wl0);

wl0 = new wall();
wl0.AddV(774, 1165);
wl0.AddV(774, 1175);
wl0.AddV(1140, 1175);
wl0.AddV(1140, 1165);
wl0.FindMidV(); wl0.SetBoundingBox();
walls["FFA"].push(wl0);

wl0 = new wall();
wl0.AddV(1234, 704);
wl0.AddV(1240, 698);
wl0.AddV(1396, 852);
wl0.AddV(1390, 859);
wl0.FindMidV(); wl0.SetBoundingBox();
walls["FFA"].push(wl0);

wl0 = new wall();
wl0.AddV(1638, 852);
wl0.AddV(1644, 859);
wl0.AddV(1799, 704);
wl0.AddV(1792, 698);
wl0.FindMidV(); wl0.SetBoundingBox();
walls["FFA"].push(wl0);

wl0 = new wall();
wl0.AddV(1638, 1092);
wl0.AddV(1644, 1086);
wl0.AddV(1799, 1235);
wl0.AddV(1792, 1242);
wl0.FindMidV(); wl0.SetBoundingBox();
walls["FFA"].push(wl0);

wl0 = new wall();
wl0.AddV(1412, 1092);
wl0.AddV(1406, 1086);
wl0.AddV(1261, 1235);
wl0.AddV(1267, 1242);
wl0.FindMidV(); wl0.SetBoundingBox();
walls["FFA"].push(wl0);

wl0 = new wall();
wl0.AddV(1886, 816);
wl0.AddV(2256, 816);
wl0.AddV(2256, 806);
wl0.AddV(1886, 806);
wl0.FindMidV(); wl0.SetBoundingBox();
walls["FFA"].push(wl0);

wl0 = new wall();
wl0.AddV(1886, 1151);
wl0.AddV(2256, 1151);
wl0.AddV(2256, 1161);
wl0.AddV(1886, 1161);
wl0.FindMidV(); wl0.SetBoundingBox();
walls["FFA"].push(wl0);

wl0 = new wall();
wl0.AddV(2331, 887);
wl0.AddV(2321, 887);
wl0.AddV(2321, 1118);
wl0.AddV(2331, 1118);
wl0.FindMidV(); wl0.SetBoundingBox();
walls["FFA"].push(wl0);

wl0 = new wall();
wl0.AddV(1283, 558);
wl0.AddV(1289, 551);
wl0.AddV(1188, 449);
wl0.AddV(1182, 455);
wl0.FindMidV(); wl0.SetBoundingBox();
walls["FFA"].push(wl0);

wl0 = new wall();
wl0.AddV(1208, 389);
wl0.AddV(1414, 389);
wl0.AddV(1414, 399);
wl0.AddV(1208, 399);
wl0.FindMidV(); wl0.SetBoundingBox();
walls["FFA"].push(wl0);

wl0 = new wall();
wl0.AddV(1633, 389);
wl0.AddV(1839, 389);
wl0.AddV(1839, 399);
wl0.AddV(1633, 399);
wl0.FindMidV(); wl0.SetBoundingBox();
walls["FFA"].push(wl0);

wl0 = new wall();
wl0.AddV(1859, 450);
wl0.AddV(1865, 456);
wl0.AddV(1764, 558);
wl0.AddV(1758, 552);
wl0.FindMidV(); wl0.SetBoundingBox();
walls["FFA"].push(wl0);

wl0 = new wall();
wl0.AddV(1662, 608);
wl0.AddV(1662, 618);
wl0.AddV(1396, 618);
wl0.AddV(1396, 608);
wl0.FindMidV(); wl0.SetBoundingBox();
walls["FFA"].push(wl0);

wl0 = new wall();
wl0.AddV(1324, 77);
wl0.AddV(1334, 77);
wl0.AddV(1334, 303);
wl0.AddV(1324, 303);
wl0.FindMidV(); wl0.SetBoundingBox();
walls["FFA"].push(wl0);

wl0 = new wall();
wl0.AddV(1718, 77);
wl0.AddV(1728, 77);
wl0.AddV(1728, 303);
wl0.AddV(1718, 303);
wl0.FindMidV(); wl0.SetBoundingBox();
walls["FFA"].push(wl0);

wl0 = new wall();
wl0.AddV(1477, 173);
wl0.AddV(1574, 173);
wl0.AddV(1574, 183);
wl0.AddV(1477, 183);
wl0.FindMidV(); wl0.SetBoundingBox();
walls["FFA"].push(wl0);

wl0 = new wall();
wl0.AddV(1407, 1355);
wl0.AddV(1674, 1355);
wl0.AddV(1674, 1365);
wl0.AddV(1407, 1365);
wl0.FindMidV(); wl0.SetBoundingBox();
walls["FFA"].push(wl0);

wl0 = new wall();
wl0.AddV(1407, 1355);
wl0.AddV(1674, 1355);
wl0.AddV(1674, 1365);
wl0.AddV(1407, 1365);
wl0.FindMidV(); wl0.SetBoundingBox();
walls["FFA"].push(wl0);

wl0 = new wall();
wl0.AddV(1211, 1523);
wl0.AddV(1312, 1421);
wl0.AddV(1306, 1414);
wl0.AddV(1205, 1516);
wl0.FindMidV(); wl0.SetBoundingBox();
walls["FFA"].push(wl0);

wl0 = new wall();
wl0.AddV(1230, 1572);
wl0.AddV(1437, 1572);
wl0.AddV(1437, 1582);
wl0.AddV(1230, 1582);
wl0.FindMidV(); wl0.SetBoundingBox();
walls["FFA"].push(wl0);

wl0 = new wall();
wl0.AddV(1655, 1572);
wl0.AddV(1862, 1572);
wl0.AddV(1862, 1582);
wl0.AddV(1655, 1582);
wl0.FindMidV(); wl0.SetBoundingBox();
walls["FFA"].push(wl0);

wl0 = new wall();
wl0.AddV(1787, 1416);
wl0.AddV(1888, 1518);
wl0.AddV(1882, 1525);
wl0.AddV(1780, 1422);
wl0.FindMidV(); wl0.SetBoundingBox();
walls["FFA"].push(wl0);

wl0 = new wall();
wl0.AddV(1334, 1686);
wl0.AddV(1344, 1686);
wl0.AddV(1344, 1912);
wl0.AddV(1334, 1912);
wl0.FindMidV(); wl0.SetBoundingBox();
walls["FFA"].push(wl0);

wl0 = new wall();
wl0.AddV(1729, 1686);
wl0.AddV(1739, 1686);
wl0.AddV(1739, 1912);
wl0.AddV(1729, 1912);
wl0.FindMidV(); wl0.SetBoundingBox();
walls["FFA"].push(wl0);

wl0 = new wall();
wl0.AddV(1487, 1782);
wl0.AddV(1584, 1782);
wl0.AddV(1584, 1792);
wl0.AddV(1487, 1792);
wl0.FindMidV(); wl0.SetBoundingBox();
walls["FFA"].push(wl0);

wl0 = new wall();
wl0.AddV(1487, 1782);
wl0.AddV(1584, 1782);
wl0.AddV(1584, 1792);
wl0.AddV(1487, 1792);
wl0.FindMidV(); wl0.SetBoundingBox();
walls["FFA"].push(wl0);

wl0 = new wall();
wl0.AddV(2640, 625);
wl0.AddV(2641, 634);
wl0.AddV(2545, 650);
wl0.AddV(2560, 733);
wl0.AddV(2550, 735);
wl0.AddV(2535, 642);
wl0.FindMidV(); wl0.SetBoundingBox();
walls["FFA"].push(wl0);

wl0 = new wall();
wl0.AddV(2765, 622);
wl0.AddV(2778, 516);
wl0.AddV(2788, 517);
wl0.AddV(2777, 614);
wl0.AddV(2860, 623);
wl0.AddV(2859, 633);
wl0.FindMidV(); wl0.SetBoundingBox();
walls["FFA"].push(wl0);

wl0 = new wall();
wl0.AddV(2550, 947);
wl0.AddV(2624, 891);
wl0.AddV(2689, 976);
wl0.AddV(2681, 982);
wl0.AddV(2622, 904);
wl0.AddV(2555, 955);
wl0.FindMidV(); wl0.SetBoundingBox();
walls["FFA"].push(wl0);

wl0 = new wall();
wl0.AddV(2215, 1352);
wl0.AddV(2556, 1352);
wl0.AddV(2556, 1516);
wl0.AddV(2714, 1516);
wl0.AddV(2714, 1526);
wl0.AddV(2546, 1526);
wl0.AddV(2546, 1362);
wl0.AddV(2394, 1362);
wl0.AddV(2394, 1692);
wl0.AddV(2384, 1692);
wl0.AddV(2384, 1362);
wl0.AddV(2225, 1362);
wl0.AddV(2225, 1685);
wl0.AddV(2215, 1685);
wl0.FindMidV(); wl0.SetBoundingBox();
walls["FFA"].push(wl0);

wl0 = new wall();
wl0.AddV(2066, 340);
wl0.AddV(2066, 183);
wl0.AddV(2287, 183);
wl0.AddV(2287, 193);
wl0.AddV(2076, 193);
wl0.AddV(2076, 340);
wl0.FindMidV(); wl0.SetBoundingBox();
walls["FFA"].push(wl0);

wl0 = new wall();
wl0.AddV(2106, 387);
wl0.AddV(2254, 387);
wl0.AddV(2254, 397);
wl0.AddV(2106, 397);
wl0.FindMidV(); wl0.SetBoundingBox();
walls["FFA"].push(wl0);

wl0 = new wall();
wl0.AddV(2302, 387);
wl0.AddV(2542, 387);
wl0.AddV(2542, 333);
wl0.AddV(2552, 333);
wl0.AddV(2552, 397);
wl0.AddV(2302, 397);
wl0.FindMidV(); wl0.SetBoundingBox();
walls["FFA"].push(wl0);

wl0 = new wall();
wl0.AddV(2694, 258);
wl0.AddV(2704, 258);
wl0.AddV(2704, 183);
wl0.AddV(2653, 183);
wl0.AddV(2653, 193);
wl0.AddV(2694, 193);
wl0.FindMidV(); wl0.SetBoundingBox();
walls["FFA"].push(wl0);

wl0 = new wall();
wl0.AddV(2400, 183);
wl0.AddV(2578, 183);
wl0.AddV(2578, 193);
wl0.AddV(2400, 193);
wl0.FindMidV(); wl0.SetBoundingBox();
walls["FFA"].push(wl0);

module.exports = walls;