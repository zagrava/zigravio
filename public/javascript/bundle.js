/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

const Controller = __webpack_require__(1);
const Pl = __webpack_require__(2);
const wall = __webpack_require__(3);

function v(a, b) {
    this.x = a;
    this.y = b;
}

let canvas;
let context;

var socket = io.connect();

const keys = {left:false,
            right:false,
            up:false,
            down:false,
            shoot:false,
            blink:false,
            is_blink_pressed:false,
            respawn: false,
            mouse: new v(window.innerWidth * 0.5, window.innerHeight * 0.5) };

let objects = [];
let walls = [];

var main;
var eye = new v(0, 0);
let view = Controller.CreateBoundingBox(0,0,0,0);
var Texture = new v(111, 114);
var FonePicture;
var CrosshairPicture;
var AimconfPicture;

///////////////////////////
var last = 0;

var frameCountDate;//FRAME COUNT
var frames = 0;//FRAME COUNT
///////////////////////////

window.requestAnim = window.requestAnimationFrame ||
                     window.webkitRequestAnimationFrame ||
                     window.mozRequestAnimationFrame ||
                     window.oRequestAnimationFrame ||
                     window.msRequestAnimationFrame ||
                     function(h) { return window.setTimeout(h, 1E3 / 60) };

// window.cancelAnimationFrame = window.cancelAnimationFrame || window.webkitCancelAnimationFrame || ... !!!!!LOOK DOWN!!!!!
// https://www.paulirish.com/2011/requestanimationframe-for-smart-animating/
// https://developer.mozilla.org/en-US/docs/Glossary/Vendor_Prefix

var GLOBALKA = [];

var FRAMETIME = (1000/60);
var RENDERTIME = 100;
var room = {};
let max_react_num = undefined;

var PingIn = 0;
var timelist = [];

//////////////////////////
var sessionid = "";
socket.on('connect', function() {
    sessionid = socket.id;
});

var requestId = undefined;

function startloop() {
    if (!requestId) {
       requestId = window.requestAnim(loop);
    } else {
        console.log("requestId is already defined");
    }
}

function stoploop() {
    if(requestId) {
       window.cancelAnimationFrame(requestId);
       requestId = undefined;
    } else {
        console.log("requestId is undefined");
    }
}
//////////////////////////

socket.on('sync', function(data) {
    PingIn++;

    objects = Pl.AcceptSyncData(objects, timelist, sessionid, data);
});

socket.on('entered', function(data) {
    canvas.style.display = "block";
    document.body.style.cursor = 'none';

    const mainpage = document.getElementById('background');
    const mainpage_elems = mainpage.getElementsByTagName("*");
    for (let i = 0; i < mainpage_elems.length; i++) {
        const element = mainpage_elems[i];
        element.style.display = "none";
    }
    mainpage.style.display = "none";

    max_react_num = data[0].max_react_num;

    room.type = data[0].type;
    room.width = data[0].width;
    room.height = data[0].height;

    for(var i = 1; i < data.length; i++) {
        var Data = data[i];

        if(Data.type == "pl") {
            if(Data.id == sessionid) {
                main = {};
                Pl.player(main, Data);
                Pl.plSetClientData(main, Data);
            	objects.push(main);
            } else {
                var pl = {};
                Pl.player(pl, Data);
                Pl.plSetClientData(pl, Data);
            	objects.push(pl);
            }
        } else {
            var w1 = new wall();
            for(var j = 0; j < Data.vertices.length; j++) {
                w1.AddV(Data.vertices[j].x, Data.vertices[j].y);
            }
            w1.midVert = Data.midVert;
            w1.bb = Data.bb;
            walls.push(w1);
        }
    }

    // Start the loop/game
    startloop();
});

socket.on('closeroom', function(data) {
    document.getElementById("matchinfo").style.display = "block";
    document.body.style.cursor = "";

    if(data.final == "vic") {
        document.getElementById("results").style.color = 'rgb(255,215,0)';
        document.getElementById("results").innerText = 'VICTORY';
    } else if(data.final == "def") {
        document.getElementById("results").style.color = 'rgb(198,7,7)';
        document.getElementById("results").innerText = 'DEFEAT';
    } else {
        document.getElementById("results").style.color = 'rgb(198,7,7)';
        document.getElementById("results").innerText = 'DRAW';
    }

    timelist = [];
    objects = [];
    walls = [];
    stoploop();
});

document.getElementById('DUEL').addEventListener("click", function(){
    socket.emit('enter', {
        type: 'DUEL',
        name: document.getElementById('name').value,
    });
});

document.getElementById('FFA').addEventListener("click", function(){
    socket.emit('enter', {
        type: 'FFA',
        name: document.getElementById('name').value,
    });
});

document.getElementById('exitroom').addEventListener("click", function(){
    document.getElementById("matchinfo").style.display = "none";
    canvas.style.display = "none";

    const mainpage = document.getElementById('background');
    const mainpage_elems = mainpage.getElementsByTagName("*");
    for (let i = 0; i < mainpage_elems.length; i++) {
        const element = mainpage_elems[i];
        element.style.display = "";
    }
    mainpage.style.display = "";
});

document.getElementById('respawn').addEventListener("click", function(){
    keys.respawn = true;
});

function PaintGameData(data, elem, ToHide = false) {
    document.getElementById(elem).style = "color:red; border: 1px solid blue; font-size: 200%;";
    document.getElementById(elem).innerHTML = data;

    if(ToHide) {
        document.getElementById(elem).style = "";
        document.getElementById(elem).innerHTML = "";
    }
}


function start() {
    canvas = document.getElementById( 'canvas' );

    if (canvas && canvas.getContext) {
        context = canvas.getContext('2d');

		// Register event listeners
		window.addEventListener('mousemove', MouseMoveHandler, false);
		window.addEventListener('mousedown', MouseDownHandler, false);
		window.addEventListener('mouseup', MouseUpHandler, false);
		window.addEventListener('keydown', KeyDownHandler, false);
		window.addEventListener('keyup', KeyUpHandler, false);
		window.addEventListener('resize', ResizeHandler, false);

		FonePicture = new Image();
        FonePicture.src = "images/" + "Background.jpg";

        CrosshairPicture = new Image();
        CrosshairPicture.src = "images/" + "crosshair.png";

        AimconfPicture = new Image();
        AimconfPicture.src = "images/" + "aimconfirmation.png";


		frameCountDate = performance.now();//FRAME COUNT

        ResizeHandler();

        /////////////////////////

        /////////////////////////
	}
}

/////////////////////////
// const ServerController = require('./ServerController');
// const RoomRuler = require("./RoomRuler");
// const walls1 = require("./Warehouse");
// const Pl = require("./Player");

/////////////////////////

function ResizeHandler() {
    canvas.width = window.innerWidth;
	canvas.height = window.innerHeight;
}

function MouseMoveHandler(event) {
	keys.mouse.x = event.clientX;
	keys.mouse.y = event.clientY;
}

function MouseDownHandler(event) {
	keys.shoot = true;
}

function MouseUpHandler(event) {
	keys.shoot = false;
}

function KeyDownHandler(event) {
    var is = true;
    switch(event.keyCode) {
        case 65:keys.left = true; break;
        case 68:keys.right = true; break;
        case 87:keys.up = true; break;
        case 83:keys.down = true; break;
        case 32:keys.blink = true;break;
    }
  //if(is) event.preventDefault();
}

function KeyUpHandler(event) {
    var is = true;
    switch(event.keyCode) {
        case 65:keys.left = false; break;
        case 68:keys.right = false; break;
        case 87:keys.up = false; break;
        case 83:keys.down = false; break;
        case 32:keys.blink = false; keys.is_blink_pressed = false; break;
        default: is = false;
    }
    //if(is) event.preventDefault();
}

function loop(curr) {
    var seconds = (curr - last) / 1000;
    last = curr;

    // FRAME COUNT
    var frseconds = (curr - frameCountDate ) / 1000;
    if(frseconds >= 1.0) {
        frameCountDate = curr;
        frames = 0;
    }
    frames++;

    // HANDLE SENT TIMELIST + REACTARRAYS + LASTSEENFRAME
    const now = performance.now();

    Pl.AlignTimelist(timelist, now - RENDERTIME, now, FRAMETIME, PingIn);
    PingIn = 0;

    const result_arr = Pl.CreateReactArrAndLSeenFrame(timelist, now - RENDERTIME, max_react_num);
    const reaction_arr = result_arr[0];
    const last_frame_seen = result_arr[1];
    // HANDLE SENT TIMELIST + REACTARRAYS + LASTSEENFRAME

    // Remember main data before applying any inputs
    const objs_walls = objects.concat(walls);
    const oldmainx = main.x;
    const oldmainy = main.y;
    const oldmainenergy = main.energy;

    // GAME TICK //
    // GAME TICK //

    // HANDLE PLAYER COMMANDS //
    Pl.HandleCommands(main, keys, eye);

    main.velocity = Controller.mult(main.velocity, reaction_arr.length);
    // HANDLE PLAYER COMMANDS //

    // PROCESS PLAYER'S INPUTS //
    if(last_frame_seen !== -1) {
        //Fire Stuff
        let hitlist = Controller.ObjFiring(objs_walls, objects.indexOf(main), objs_walls);

        GraphicsController.setup.paint.hit = Pl.DoesShootPlayer(main, hitlist);

        // Skills stuff
        if(Pl.IsHeAlive(main)) {
            GraphicsController.setup.paint.healtbar = true;
            GraphicsController.setup.paint.energybar = true;

            for (let i = 0; i < (last_frame_seen+1); i++) {
                Pl.EnergyRegen(main);
            }
        } else {
            GraphicsController.setup.paint.healtbar = false;
            GraphicsController.setup.paint.energybar = false;
        }

        if(Pl.CanHeBlink(main)) {
            Pl.Blink(main);
        }
        
        // Collision Stuff
        if(Pl.CanHeMove(main)) {
            Controller.ZoneClip(main, room.width, room.height);
            
            Pl.SetBoundingBox(main, main.r+Controller.len(main.velocity)+1);

            Controller.PlRunCollCycle(main, walls);

            Pl.Move(main);
        }

        // Synchronization stuff
        main.synclist.push({
            poschange: new v(main.x-oldmainx, main.y-oldmainy),
            energychange: main.energy-oldmainenergy,
            last_react_sent: reaction_arr[reaction_arr.length-1]
        });
    }
    // PROCESS PLAYER'S INPUTS //

    // SCENE PAINTING //
    GraphicsController.SetEyeViewTo(main);

    GraphicsController.PaintRepeatingBackground();

    for(let i = 0; i < objects.length; i++) {
        if(objects[i].id != sessionid) {
            Pl.AdjustSynclist(objects[i], timelist, 100);
            Pl.GetDataFromSynclist(objects[i], last_frame_seen);
            objects[i].synclist.splice(0, last_frame_seen+1);
        }

        Pl.SetBoundingBox(objects[i], objects[i].r+1);

        if(Pl.IsHeAlive(objects[i]) && Controller.BoxBoxCollDet(objects[i].bb, view)) {
            GraphicsController.PaintPlayer(objects[i]);
        }
    }

    for(let i = 0; i < walls.length; i++) {
        if(Controller.BoxBoxCollDet(walls[i].bb, view)) {
            GraphicsController.PaintWall(walls[i]);
        }
    }

    GraphicsController.DrawInterface();
    // SCENE PAINTING //

    timelist.splice(0, last_frame_seen+1);

    if(last_frame_seen !== -1) {
        socket.emit('sync', {
            dir: {x: main.dir.x, y: main.dir.y},
            left: keys.left,
            right: keys.right,
            up: keys.up,
            down: keys.down,
            blink: main.blink,
            fire: main.fire,
            respawn: main.respawn,
            reaction_arr: reaction_arr
        });
    }

    requestId = window.requestAnim(loop);
}

var GraphicsController = {
    setup: {
        paint: {
            // What should I paint
            hit: false,
            fire: false,
            healtbar: false,
            energybar: false
        }
    },
    SetEyeViewTo: function(view_center) {
        eye.x = view_center.c.x - canvas.width/2;
        eye.y = view_center.c.y - canvas.height/2;

        view = Controller.CreateBoundingBox(view_center.c.y - (canvas.height/2),
                                            view_center.c.y + (canvas.height/2),
                                            view_center.c.x - (canvas.width/2),
                                            view_center.c.x + (canvas.width/2));
    },
    PaintBox: function(box) {
        context.strokeStyle = "#FF0000";
        context.strokeRect(box.top_left.x-eye.x, box.top_left.y-eye.y,
                           box.bottom_right.x - box.top_left.x,
                           box.bottom_right.y - box.top_left.y);
    },
    PaintRepeatingBackground: function() {
        const relative_point = new v(eye.x, eye.y);
        Controller.DistBetP_Sq(relative_point, {x: 222, y: 228});
        context.drawImage(FonePicture, relative_point.x, relative_point.y, canvas.width, canvas.height, 0, 0, canvas.width, canvas.height);
    },
    PaintWall: function(wall) {
        context.fillStyle = 'rgb(86, 105, 135)';
        context.beginPath();
        context.moveTo(wall.vertices[0].x - eye.x, wall.vertices[0].y - eye.y);
        for(var i = 1, l = wall.vertices.length; i < l; i++) {
            context.lineTo(wall.vertices[i].x - eye.x, wall.vertices[i].y - eye.y);
        }
        context.lineTo(wall.vertices[0].x - eye.x, wall.vertices[0].y - eye.y);
        context.fill();
    },
    PaintPlayer: function(pl) {
        context.save();
        context.translate(pl.c.x - eye.x, pl.c.y - eye.y);
        context.rotate(Math.atan2(pl.dir.y, pl.dir.x));

        // context.drawImage(pl.skin, -(pl.r), -(pl.r), pl.r*2, pl.r*2);
        context.beginPath();
        context.arc(0, 0,  pl.r, 0, 2*Math.PI, false);
        context.fillStyle = pl.color;
        context.fill();

        context.beginPath();
        if(pl.fire) context.filter = "brightness(170%)";
        context.arc(0, 0, pl.r-1, 347.5*(Math.PI/180), 12.5*(Math.PI/180), false);
        context.lineTo(pl.r*1.4, 0);
        context.fill();

        context.restore();
    },
    PaintLeaderboard: function(sx, sy, width, line_height, headline_height, lines) {
        // var a = objects;
        // var objects = [{kills: 7,},{kills: 8,},{kills: 14,},
        //                {kills: 1,},{kills: 5,},{kills: 5,},
        //                {kills: 3,},{kills: 15,},{kills: 16,}];

        var leaderlist = [];
        for(var j = 0; j < objects.length; j++) {
            for(var i = 0; i < leaderlist.length; i++) {
                if(objects[j].kills > leaderlist[i].kills) {
                    break;
                }
            }
            leaderlist.splice(i, 0, objects[j]);
        }
        var lines = Math.min(lines, leaderlist.length);
        var height = lines * line_height;
        context.fillStyle = 'rgba(33, 52, 121, 0.7)';
        context.fillRect(sx, sy, width, height+headline_height+10);
        context.fillStyle = 'rgb(255, 255, 255)';

        var y = sy+10;

        y += headline_height-10;
        context.font = headline_height +"px Arial";
        context.textAlign = "left";
        context.fillText("Top players:", sx, y);
        context.textAlign = "right";
        context.fillText("Kills:", sx+width, y);
        context.font = line_height +"px Arial";

        for(j = 0; j < lines; j++) {
            (leaderlist[j] === main) ? context.fillStyle = 'rgb(255,215,0)' : context.fillStyle = 'rgb(255, 255, 255)';

            y += line_height;
            context.textAlign = "left";
            context.fillText(leaderlist[j].name, sx, y);
            context.textAlign = "right";
            context.fillText(leaderlist[j].kills, sx+width, y);
        }

        context.fillStyle = 'rgb(255, 255, 255)';

        // objects = a;
    },
    PaintStripedBar: function(sx, sy, width, height, value, value_max, value_step, step_style, gap_width, gap_style) {
        var x = sx; var y = sy;
        var topaint = value;
        var gaps_width = (Math.ceil(value_max / value_step) - 1) * gap_width;
        var paint_place = width-gaps_width;
        var step_width = (value_step/value_max) * paint_place;

        while(topaint > value_step) {
            context.fillStyle = step_style;
            context.fillRect(x, y, step_width, height);
            x += step_width;
            context.fillStyle = gap_style;
            context.fillRect(x, y, gap_width, height);
            x += gap_width;
            topaint -= value_step;
        }
        context.fillStyle = step_style;
        context.fillRect(x, y, (topaint/value_max) * paint_place, height);
    },
    DrawInterface: function() {
        context.drawImage(CrosshairPicture, keys.mouse.x - (20/2), keys.mouse.y - (20/2), 20, 20);

        if(this.setup.paint.hit) {
            context.drawImage(AimconfPicture, keys.mouse.x - (17/2), keys.mouse.y - (17/2), 17, 17);
        }

        if(this.setup.paint.healtbar && this.setup.paint.energybar) {
            context.fillStyle = 'rgba(33, 52, 121, 0.7)';
            var combat_bar_width = 480;
            var combat_bar_height = 65;
            var combat_bar_x = (canvas.width/2) - (combat_bar_width/2);
            var combat_bar_y = canvas.height-combat_bar_height;
            context.fillRect(combat_bar_x, combat_bar_y, combat_bar_width, combat_bar_height);
            
            GraphicsController.PaintStripedBar(combat_bar_x + 10, combat_bar_y + 10, combat_bar_width - 20, 10, main.energy, main.max_energy, main.blink_energycost, "#FFFFFF", 5, 'rgba(0, 0, 0, 0)');
            GraphicsController.PaintStripedBar(combat_bar_x + 10, combat_bar_y + 25, combat_bar_width - 20, 30, main.hp, main.hp_max, main.hp_max, "#FFFFFF", 3, 'rgba(0, 0, 0, 0)');
        }

        if(room.type == "DUEL")
        {
            
        }
        else if(room.type == "FFA")
        {
            var respawn_width = 280;
            var respawn_height = 70;
            var respawn_left = ((canvas.width/2) - (respawn_width/2));
            var respawn_top = (canvas.height-respawn_height);
            var respawnbtn = document.getElementById('respawn');
            if(!Pl.IsHeAlive(main)) {
                respawnbtn.style.top = respawn_top + "px";
                respawnbtn.style.left = respawn_left + "px";
                respawnbtn.style.display = "block";
            } else {
                respawnbtn.style.display = ""; // default display = none
            }

            GraphicsController.PaintLeaderboard(canvas.width-300-10, 10, 300, 30, 30, 10);
        }
    }

};


window.onload = start;




/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

function v(a, b) {
    this.x = a;
    this.y = b;
}

function Contact(norm, dest, contactP, distToContactP, a, b) {
    this.contN = norm;
    this.destP = dest;
    this.contactP = contactP;
    this.distToContactP = distToContactP;
    this.ind1 = a;
    this.ind2 = b;
}

var Controller = {
    ContactList: new Array(),
    IsStuck: false,
    dot: function(v1, v2) {
        return v1.x * v2.x + v1.y * v2.y;
    },
    subt1_2: function(v1, v2) {
        return { x: v1.x - v2.x, y: v1.y - v2.y };
    },
    add: function(v1, v2) {
        return { x: v1.x + v2.x, y: v1.y + v2.y };
    },
    perp: function(v) {
        return { x: v.y, y: -v.x };
    },
    norm: function(v) {
        var l = this.len(v);
        if(l == 0) {
            return { x: 0, y: 0 };
        }
        return { x: v.x / l, y: v.y / l };
    },
    len: function(v) {
        return Math.sqrt(v.x * v.x + v.y * v.y);
    },
    neg: function(v) {
        return { x: -v.x, y: -v.y };
    },
    mult: function(v, s) {
        return { x: v.x * s, y: v.y * s};
    },
    RandomColor: function(brightness) {
        return 'rgb('+(Math.round(Math.random()*brightness))+','+(Math.round(Math.random()*brightness))+','+(Math.round(Math.random()*brightness))+')';
    },
    BoxBoxCollDet: function(b0, b1) {
        return !(b0.top_left.x > b1.bottom_right.x ||
                 b0.bottom_right.x < b1.top_left.x ||
                 b0.top_left.y > b1.bottom_right.y ||
                 b0.bottom_right.y < b1.top_left.y)
    },
    CreateBoundingBox: function(top, bottom, left, right) {
        return {
            top_left: new v (left, top),
            bottom_right: new v(right, bottom)
        };
    },
    ZoneClip: function(player, zwidth, zheight) {
        player.x = Math.max(10, player.x);
        player.y = Math.max(10, player.y);
        player.x = Math.min(zwidth - 10 - (player.r*2), player.x);
        player.y = Math.min(zheight - 10 - (player.r*2), player.y);

        player.c.x = player.x + player.r;
        player.c.y = player.y + player.r;
    },
    ObjFiring: function (objects_views, shooter_index, actual_objects) {
        let dist = -1;
        const hitlist = [];
        const shooting_obj = actual_objects[shooter_index];

        // Test if this object is able to shoot
        if(shooting_obj.type != "pl") {
            return hitlist;
        }

        for(let j = 0; j < objects_views.length; j++) {
            if(shooter_index == j) continue;

            //console.log(objects_views[j].midVert, "J:", j);
            if(objects_views[j].type == "pl" && this.len(this.subt1_2(shooting_obj.c, objects_views[j].c)) < (shooting_obj.range + 200 ) ) {
                // Find out the dist by dir vector from obj[shooter_index] to obj[j]
                dist = this.DistBetP_C(shooting_obj.c, shooting_obj.dir, objects_views[j].c, objects_views[j].r);
            }
            else if (objects_views[j].type == "wl" && this.len(this.subt1_2(shooting_obj.c, objects_views[j].midVert)) < (shooting_obj.range + 600 ) ) {
                // Find out the dist by dir vector from obj[shooter_index] to obj[j]
                dist = this.IsWlHit(objects_views[j].vertices, shooting_obj);
            }

            if(dist != -1 && dist <= shooting_obj.range) {
                let k = 0;
                for(k = 0; k < hitlist.length; k++)
                {
                    if(dist < hitlist[k].dist) {
                        break;
                    }
                }

                hitlist.splice(k, 0, {
                    dist: dist,
                    obj: actual_objects[j],
                    index: j,
                });
            }

            dist = -1;
        }

        return hitlist;
    },
    IsWlHit: function(vertices, player) {
        var mind = 1085;
        var is_hit = false;
        for(var i = 0, l = vertices.length - 1; i < l; i++) {
            var norm = this.norm(this.perp(this.subt1_2(vertices[i+1], vertices[i])));
            if(this.dot(norm, this.subt1_2(player.c, vertices[i])) < 0) { norm = this.neg(norm); }
            var d = this.DistBetP_L(player.c, player.dir, vertices[i], norm);
            if(d >= 0) {
                var p = this.add(player.c, this.mult(player.dir, d));

                if(this.IsPinW(p, vertices[i+1], vertices[i])) {
                    if(d <= mind) {
                        mind = d;
                        is_hit = true;
                    }
                }
            }
        }
        //i++
        var norm = this.norm(this.perp(this.subt1_2(vertices[i], vertices[0])));
        if(this.dot(norm, this.subt1_2(player.c, vertices[0])) < 0) { norm = this.neg(norm); }
        var d = this.DistBetP_L(player.c, player.dir, vertices[0], norm);
        if(d >= 0) {
            var p = this.add(player.c, this.mult(player.dir, d));

            if(this.IsPinW(p, vertices[i], vertices[0])) {
                if(d <= mind) {
                    mind = d;
                    is_hit = true;
                }
            }
        }
        if(is_hit == true) {
            return mind;
        }
        return -1;
    },
    IsPlHit: function(c, r) {
        return this.DistBetP_C(main.c, main.dir, c, r);
    },
    IsWlsColl: function(vertices, pl, inda, indb) {
        var mind = Controller.len(pl.velocity);
        for(var i = 0, l = vertices.length - 1; i < l; i++)
        {
            mind = this.IsWlColl(vertices[i], vertices[i+1], mind, pl, inda, indb);
        }
        mind = this.IsWlColl(vertices[i], vertices[0], mind, pl, inda, indb);
        if ( typeof this.ContactList === "undefined" || !this.ContactList || this.ContactList.length == 0 || mind == Controller.len(pl.velocity) ) {
            return -1;
        }
        return this.ContactList[this.ContactList.length - 1].distToContactP;
    },
    IsWlColl: function(V0, V1, argmind, pl, inda, indb) {
        var TcollP;
        var CcollP;

        var d;
        var mind = argmind;

        var v0 = V0;
        var v1 = V1;

        var norm = this.norm(this.perp(this.subt1_2(v1, v0)));
        if(this.dot(norm, this.subt1_2(pl.c, v0)) < 0) { norm = this.neg(norm); }

        CcollP = this.subt1_2(pl.c, this.mult(norm, pl.r));

        if(this.IsinTPl(pl.c, this.mult(norm, pl.r), v0)) { //Sphere in plane
            d = this.DistBetP_L(CcollP, norm, v0, this.neg(norm) );
            TcollP = this.add(CcollP, this.mult(norm, d));
            if(Math.max(0.0005, d) == 0.0005) {
                d = 0.0005;
            }

            if(this.IsPinW(TcollP, v1, v0)) { //STUCK
                Pl.Move(pl, d, norm);//<------------------------------------------------------------------------------------------------------------Controller.mult(norm, 1);
                IsStuck = true; // PROBABLE BUG PROBABLE BUG PROBABLE BUG PROBABLE BUG PROBABLE BUG PROBABLE BUG PROBABLE BUG
                d = 0;

                var cos = this.dot(this.neg(norm), pl.norm_velocity);
                if(cos >= 0.0005) {
                    //Sphere coll with wall
                    ///
                    if(this.ContactList.length > 0) {
                        if( (this.ContactList[this.ContactList.length-1].ind1 == inda && this.ContactList[this.ContactList.length-1].ind2 == indb) ||
                            (this.ContactList[this.ContactList.length-1].ind1 == indb && this.ContactList[this.ContactList.length-1].ind2 == inda) )
                        {
                            this.ContactList.pop();
                        }
                    }
                    ///

                    this.ContactList.push(new Contact(norm, this.add(TcollP, pl.velocity), TcollP, d, inda, indb));
                    mind = d;
                }
            }
            else //COLL CORNERS
            {
                var s0 = this.DistBetC_P(pl.c, v0, pl.r);
                var s1 = this.DistBetC_P(pl.c, v1, pl.r);
                var normV;
                if(s0 != -1) {
                    normV = this.norm(this.subt1_2(pl.c, v0));
                    Pl.Move(pl, s0, normV);
                    IsStuck = true;// PROBABLE BUG PROBABLE BUG PROBABLE BUG PROBABLE BUG PROBABLE BUG PROBABLE BUG PROBABLE BUG
                }
                else if(s1 != -1) {
                    normV = this.norm(this.subt1_2(pl.c, v1));
                    Pl.Move(pl, s1, normV);
                    IsStuck = true;// PROBABLE BUG PROBABLE BUG PROBABLE BUG PROBABLE BUG PROBABLE BUG PROBABLE BUG PROBABLE BUG
                }

                var dt = this.DistBetP_C(v1, this.neg(pl.norm_velocity), pl.c, pl.r);
                d = this.DistBetP_C(v0, this.neg(pl.norm_velocity), pl.c, pl.r);
                if( d == -1 || (dt < d && dt != -1) ) { d = dt; v0 = v1; }
                if( d != -1 && d < mind) {
                    norm = this.norm(this.subt1_2(pl.c, this.add(v0, this.mult(this.neg(pl.norm_velocity), d))));
                    CcollP = this.subt1_2(pl.c, this.mult(norm, pl.r) );
                    ///
                    if(this.ContactList.length > 0) {
                        if( (this.ContactList[this.ContactList.length-1].ind1 == inda && this.ContactList[this.ContactList.length-1].ind2 == indb) ||
                            (this.ContactList[this.ContactList.length-1].ind1 == indb && this.ContactList[this.ContactList.length-1].ind2 == inda) )
                        {
                            this.ContactList.pop();
                        }
                    }
                    ///
                    this.ContactList.push(new Contact(norm, this.add(CcollP, pl.velocity), v0, d, inda, indb));
                    mind = d;
                }
            }
        }
        else { //Sphere not in plane
            d = this.DistBetP_L(CcollP, pl.norm_velocity, v0, norm);
            TcollP = this.add(CcollP, this.mult(pl.norm_velocity, d));

            if(d >= 0)
            {
                if(this.IsPinW(TcollP, v1, v0) && d < mind)//COLL PLANE
                {
                    //Sphere coll with wall
                    ///
                    if(this.ContactList.length > 0) {
                        if( (this.ContactList[this.ContactList.length-1].ind1 == inda && this.ContactList[this.ContactList.length-1].ind2 == indb) ||
                            (this.ContactList[this.ContactList.length-1].ind1 == indb && this.ContactList[this.ContactList.length-1].ind2 == inda) )
                        {
                            this.ContactList.pop();
                        }
                    }
                    ///

                    this.ContactList.push(new Contact(norm, this.add(CcollP, pl.velocity), TcollP, d, inda, indb));
                    mind = d;
                }
                else //COLL CORNERS
                {
                    var dt = this.DistBetP_C(v1, this.neg(pl.norm_velocity), pl.c, pl.r);
                    d = this.DistBetP_C(v0, this.neg(pl.norm_velocity), pl.c, pl.r);
                    if( d == -1 || (dt < d && dt != -1) ) { d = dt; v0 = v1; }
                    if( d != -1 && d < mind) {
                        norm = this.norm(this.subt1_2(pl.c, this.add(v0, this.mult(this.neg(pl.norm_velocity), d))));
                        CcollP = this.subt1_2(pl.c, this.mult(norm, pl.r) );
                        ///
                        if(this.ContactList.length > 0) {
                            if( (this.ContactList[this.ContactList.length-1].ind1 == inda && this.ContactList[this.ContactList.length-1].ind2 == indb) ||
                                (this.ContactList[this.ContactList.length-1].ind1 == indb && this.ContactList[this.ContactList.length-1].ind2 == inda) )
                            {
                                this.ContactList.pop();
                            }
                        }
                        ///
                        this.ContactList.push(new Contact(norm, this.add(CcollP, pl.velocity), v0, d, inda, indb));
                        mind = d;
                    }
                }
            }
        }

        return mind;
    },
    PlsCollDo: function(obj1, obj2) {

        var Sd = this.DistBetC_P(obj1.c, obj2.c, (obj1.r+obj2.r) )/2;
        if(Sd != -0.5) {
            if(Sd < 0.0005) {
                Sd = 0.0005;
            }


            var Snorm = this.norm(this.subt1_2(obj1.c, obj2.c));
            if(Snorm.x == 0 && Snorm.y == 0) {
                Snorm.x = 1; Snorm.y = 1;
                Snorm = this.norm(Snorm);
            }

            Pl.Move(obj1, 1, this.mult(Snorm, Sd));
            Pl.Move(obj2, 1, this.mult(this.neg(Snorm), Sd));
            IsStuck = true;
        }

        var relV = this.norm(this.subt1_2(obj1.velocity, obj2.velocity));
        var d = this.DistBetP_C(obj1.c, relV, obj2.c, (obj1.r+obj2.r) );
        if(d != -1 && d <= obj1.speed) {/////POSIBLE ERROR POSIBLE ERROR POSIBLE ERROR POSIBLE ERROR POSIBLE ERROR POSIBLE ERROR POSIBLE ERROR
            return d;
        }
        return -1;
    },
    MainResolve: function(objects) {
        var Thecounter = 0;
        do {
            IsStuck = false;
            Thecounter++;
            var indexes = [];
            var v1; var v2;
            for (var i = 0; i < objects.length; ++i) {
                var columns = [];
                for (var j = 0; j < objects.length; ++j) {
                    if(i == j) {
                        columns[j] = -1;
                    }
                    else if(objects[i].type == "wl") {
                        if(objects[j].type == "pl") {
                            v1 = objects[i].midVert; v2 = objects[j].c;
                            if(this.len(this.subt1_2(v1, v2)) < 600) {
                                columns[j] = 4;
                            } else { columns[j] = -1; }
                        }
                    }
                    else {
                        if(objects[j].type == "wl") {
                            v1 = objects[i].c; v2 = objects[j].midVert;
                            if(this.len(this.subt1_2(v1, v2)) < 600) {
                                columns[j] = 4;
                            } else { columns[j] = -1; }
                        }
                        else {
                            v1 = objects[i].c; v2 = objects[j].c;
                            if(this.len(this.subt1_2(v1, v2)) < (2*(objects[i].r+objects[j].r))+10) {
                                columns[j] = 4;
                            } else { columns[j] = -1; }
                        }
                    }
                }
                indexes[i] = columns;
            }

            for(i = 0; i < objects.length; i++) {
                for(j = 0; j < objects.length; j++) {
                    if(i != j && indexes[j][i] == 4) {
                        if(objects[i].type == "wl") {
                            if(objects[j].type == "pl") {
                                //Wall coll check
                                indexes[i][j] = this.IsWlsColl(objects[i].vertices, objects[j], i, j);
                            }
                        }
                        else {
                            if(objects[j].type == "wl") {
                                //Wall coll check
                                indexes[i][j] = this.IsWlsColl(objects[j].vertices, objects[i], j, i);
                            }
                            else {
                                //Pl/Pl coll resolve
                                indexes[i][j] = this.PlsCollDo(objects[i], objects[j]);
                            }
                        }
                    }
                    else {
                        indexes[i][j] = indexes[j][i];
                    }
                }
            }

            var min = [];
            var minV;
            var ind;
            for(i = 0; i < indexes.length; i++) {
                minV = 100;
                ind = -1;
                for(j = 0; j < indexes.length; j++) {
                    if(indexes[i][j] != -1 && indexes[i][j] < minV) {
                        minV = indexes[i][j];
                        ind = j;
                    }
                }
                if(ind != -1 ) {
                    min[min.length] = [minV, i, ind];
                }
            }
            if(min.length != 0) {
            for(var curr = 0; curr < min.length; curr++) {
                for(var checked = 0; checked < min.length; checked++) {
                    if(curr != checked && min[curr][0] != -1 && min[checked][0] != -1 &&
                            min[curr][0] == min[checked][0] &&
                            min[curr][2] == min[checked][1] &&
                            min[curr][1] == min[checked][2] ) {

                        if(objects[min[curr][1]].type == "wl") {
                            if(objects[min[curr][2]].type == "pl") {
                                //Wall coll check
                                this.WlPlCollResolve(objects[min[curr][2]].k, objects[min[curr][2]], this.FindContact(min[curr][1], min[curr][2]));

                                for(i = 0; i < min.length; i++) {
                                    if(min[i][1] == min[curr][2] || min[i][2] == min[curr][2]) {
                                        min[i][0] = -1; }
                                }
                            }
                        }
                        else {
                            if(objects[min[curr][2]].type == "wl") {
                                //Wall coll check
                                this.WlPlCollResolve(objects[min[curr][1]].k, objects[min[curr][1]], this.FindContact(min[curr][2], min[curr][1]));

                                for(i = 0; i < min.length; i++) {
                                    if(min[i][1] == min[curr][1] || min[i][2] == min[curr][1]) {
                                        min[i][0] = -1; }
                                }
                            }
                            else {
                                //Pl/Pl coll resolve
                                this.PlPlResolve(objects[min[curr][1]].k, objects[min[curr][2]].k, objects[min[curr][1]], objects[min[curr][2]], min[curr][0]);

                                for(i = 0; i < min.length; i++) {
                                    if(min[i][1] == min[curr][1] || min[i][2] == min[curr][1] ||
                                       min[i][1] == min[curr][2] || min[i][2] == min[curr][2] ) {
                                        min[i][0] = -1; }
                                }
                            }
                        }
                    }
                }
            }
            }

            this.ContactList.splice(0, this.ContactList.length);

        } while(Thecounter < 15 && (min.length != 0 || IsStuck == true))

        for(i = 0; i < objects.length; i++) {
            if(objects[i].type == "pl") {
                objects[i].k = 1;
            }
        }
    },
    FilterPossibleCollObjs: function(obj, objects) {
        let possible_coll_objs = [];
        for (let i = 0; i < objects.length; i++) {
            if(obj === objects[i]) continue;
    
            if(Controller.BoxBoxCollDet(obj.bb, objects[i].bb)) possible_coll_objs.push(objects[i]);
        }
        return possible_coll_objs;
    },
    PlRunCollCycle: function(pl, objects) {
        const contenders = this.FilterPossibleCollObjs(pl, objects);

        for (let coll_cycles = 0; coll_cycles < 3; coll_cycles++) {
            IsStuck = false;

            const coll_distances = [];
            for (let i = 0; i < contenders.length; i++) {
                const contender = contenders[i];
                if(contender.type == "wl")
                {
                    coll_distances.push(this.IsWlsColl(contender.vertices, pl, i, -1));
                }
                else
                {
                    // Push arr in any situation cause we need it to have same length as contenders
                    coll_distances.push(-1);
                }
            }

            let min_dist = Infinity;
            let coll_obj_index = -1;
            for (let i = 0; i < coll_distances.length; i++) {
                if(coll_distances[i] != -1 && coll_distances[i] < min_dist)
                {
                    min_dist = coll_distances[i];
                    coll_obj_index = i;
                }
            }

            if(coll_obj_index != -1) {
                this.WlPlCollResolve(pl.k, pl, this.FindContact(coll_obj_index, -1));
            }

            this.ContactList.splice(0, this.ContactList.length);

            if (coll_obj_index === -1 && !IsStuck) break;
        }

        pl.k = 1;
        for(let i = 0; i < contenders.length; i++)
        {
            if(contenders[i].type == "pl") contenders[i].k = 1;
        }
    },
    FindContact: function(a, b) {
        for(var i = 0; i < this.ContactList.length; i++) {
            if(this.ContactList[i].ind1 == a && this.ContactList[i].ind2 == b) {
                return this.ContactList[i];
            }
        }
    },
    PlPlResolve: function(n1, n2, obj1, obj2, t) {
        var relV = this.norm(this.subt1_2(obj1.velocity, obj2.velocity));
        var block = this.len(this.subt1_2(obj1.velocity, obj2.velocity));
        var k;
        if(block != 0) {
            k = this.len(this.mult(relV, t)) / block;
        }
        var BefCollv1 = this.mult(obj1.velocity, k);
        var BefCollv2 = this.mult(obj2.velocity, k);
        Pl.Move(obj1, 1, this.mult(obj1.velocity, k));
        Pl.Move(obj2, 1, this.mult(obj2.velocity, k));
        var norm = this.norm(this.subt1_2(obj2.c, obj1.c));

        if(this.dot(norm, obj1.norm_velocity) > 0) {
            if(n1 == 1) {
                obj1.velocity = this.subt1_2(obj1.velocity, BefCollv1);
                obj1.velocity = this.subt1_2(obj1.velocity, this.mult(norm, this.dot(norm, obj1.velocity)));
                obj1.norm_velocity = this.norm(obj1.velocity);
                obj1.k = 2;
            } else {
                obj1.velocity = { x: 0, y: 0 };
            }
        }
        if(this.dot(norm, obj2.norm_velocity) < 0) {
            if(n2 == 1) {
                obj2.velocity = this.subt1_2(obj2.velocity, BefCollv2);
                obj2.velocity = this.subt1_2(obj2.velocity, this.mult(norm, this.dot(norm, obj2.velocity)));
                obj2.norm_velocity = this.norm(obj2.velocity);
                obj2.k = 2;
            } else {
                obj2.velocity = { x: 0, y: 0 };
            }
        }
    },
    WlPlCollResolve: function(n, obj, contact) {
        var collVtoPlane;
        var newDestP;
        if(contact.distToContactP != -1) {
            if(n == 1) {
                collVtoPlane = this.mult(obj.norm_velocity, contact.distToContactP);
                var dot0 = this.dot(contact.contactP, contact.contN);
    	        var dot1 = this.dot(contact.destP, contact.contN);
    	        var dist = dot0 - dot1;
    	        newDestP = this.add(contact.destP, (this.mult(contact.contN, dist)));
    	        newDestP = this.subt1_2(newDestP, contact.contactP);
    	        obj.velocity = this.add(newDestP, collVtoPlane);
    	        obj.norm_velocity = this.norm(obj.velocity);
    	        obj.k = 2;
            }
            else {
                collVtoPlane = this.mult(obj.norm_velocity, contact.distToContactP);
                obj.velocity = collVtoPlane;
                obj.norm_velocity = this.norm(obj.velocity);
            }
            contact.distToContactP = -1;
        }
    },
    DistBetP_Sq: function(P, lenV) {
        var lx = lenV.x * 0.5;
        var ly = lenV.y * 0.5;
        if(P.x < 0 || P.x >= lenV.x) {
            P.x = lx + (P.x % lx);
        }
        if(P.y < 0 || P.y >= lenV.y) {
            P.y = ly + (P.y % ly);
        }
        return P;
    },
    DistBetP_L: function(P, destV, PlaneP, PlaneN) {
        var PlaneP_proj = this.dot(PlaneN, PlaneP);
        var proj_dist = this.dot(PlaneN, P) - PlaneP_proj;
        var cos = this.dot(this.neg(PlaneN), destV);//cos is always >= 0

        if(cos < 0.0005) return -1;
        if(proj_dist < 0) {proj_dist = 0;}//Float innacuracy stuff

        //if (cos <= 0) return -1;////Float innacuracy stuff and paralell moving

        return (proj_dist / cos);
    },
    DistBetP_C: function(P, dir, Cc, r) {
        var centV = this.subt1_2(Cc, P);

        var hyp = this.len(centV);
        var cathe = this.dot(centV, dir);

        if (cathe < 0.0005) { return -1; }

        var d = r*r - (hyp*hyp - cathe*cathe);

        if (d < 0) { return -1; }

        return cathe - Math.sqrt(d);
    },
    DistBetC_P: function(c, p, r) {
        var d = this.len(this.subt1_2(c, p));
        if(d < r) {
            return (r - d);
        }
        return -1;
    },
    IsPinW: function(P, p1, p0) {
        var edgeV = this.subt1_2(p1, p0);
        var PV = this.subt1_2(P, p0);
        if(this.dot(edgeV, PV) >= 0 && this.len(PV) <= this.len(edgeV)) {
            return true;
        }
        return false;
    },
    IsinTPl: function(sc, norm, tv) {
        var interP = this.subt1_2(sc, norm);
        if( this.dot(this.subt1_2(tv, interP), norm) > 0 ) {
            return true;
        }
        return false;
    }
};

module.exports = Controller;

const Pl = __webpack_require__(2);

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

const Controller = __webpack_require__(1);

function v(a, b) {
    this.x = a;
    this.y = b;
}

const Pl = {
    player: function(obj, data)
    {
        obj.x = data.x;
        obj.y = data.y;
        obj.r = data.r;
        obj.c = new v(obj.x + obj.r, obj.y + obj.r);
        obj.type = "pl";

        obj.norm_velocity = new v(0, 0);
        obj.velocity = new v(0, 0);
        obj.dir = new v(1, 0);
        obj.blink = false;
        obj.fire = false;
        obj.respawn = false;

        obj.hp = data.hp;
        obj.hp_max = data.hp_max;
        obj.hp_regen = data.hp_regen;
        obj.hp_regen_threshold = data.hp_regen_threshold;
        obj.dmg = data.dmg;
        obj.speed = data.speed;
        obj.blinkspeedV = data.blinkspeedV;
        obj.blink_energycost = data.blink_energycost;
        obj.range = data.range;
        obj.energy = data.energy;
        obj.energy_regen = data.energy_regen;
        obj.max_energy = data.max_energy;
        obj.undamaged = 0;

        obj.id = data.id;
        obj.name = data.name;
        obj.color = data.color;
        obj.skin = data.skin;
        obj.aimskin = data.aimskin;
        obj.ammoskin = data.ammoskin;

        obj.k = 1;
    },

    plSetClientData: function(obj, data)
    {
        // Client-need data
        obj.synclist = [];
        obj.desync = 0;
        // Client-need statistics
        obj.kills = data.kills;
    },
    
    SetBoundingBox: function(pl, bb_r) {
        pl.bb = Controller.CreateBoundingBox(pl.c.y - bb_r, pl.c.y + bb_r, pl.c.x - bb_r, pl.c.x + bb_r);
    },

    TransformData: function(to, from, trans_phys, trans_intentions, trans_gamedata, trans_generaldata, trans_stats)
    {
        // Transform Physique
        if(trans_phys) {
            if (typeof(to.c) !== 'object') to.c = new v(0, 0);
            
            to.x = from.x;
            to.y = from.y;
            to.c.x = from.c.x;
            to.c.y = from.c.y;
            to.r = from.r;
            to.type = from.type;
        }

        // Transform Intentions
        if(trans_intentions) {
            if (typeof(to.norm_velocity) !== 'object') to.norm_velocity = new v(0, 0);
            if (typeof(to.dir) !== 'object') to.dir = new v(0, 0);

            to.norm_velocity.x = from.norm_velocity.x;
            to.norm_velocity.y = from.norm_velocity.y;
            to.velocity = Controller.mult(to.norm_velocity, to.speed); // speed is used before it's tranflated
            to.dir.x = from.dir.x;
            to.dir.y = from.dir.y;
            to.blink = from.blink;
            to.fire = from.fire;
            to.respawn = from.respawn;
        }

        // Transform Gamedata
        if(trans_gamedata) {
            to.hp = from.hp;
            to.hp_max = from.hp_max;
            to.hp_regen = from.hp_regen;
            to.hp_regen_threshold = from.hp_regen_threshold;
            to.dmg = from.dmg;
            to.speed = from.speed;
            to.blinkspeedV = from.blinkspeedV;
            to.blink_energycost = from.blink_energycost;
            to.range = from.range;
            to.energy = from.energy;
            to.energy_regen = from.energy_regen;
            to.max_energy = from.max_energy;
            to.undamaged = from.undamaged;

            to.type = from.type;
        }

        // Transform General Data
        if(trans_generaldata) {
            to.id = from.id;
            to.name = from.name;
            to.color = from.color;
            to.skin = from.skin;
            to.aimskin = from.aimskin;
            to.ammoskin = from.ammoskin;
        }

        // Transform Statistics
        if(trans_stats) {
            to.kills = from.kills;
        }

        return to;
    },

    AcceptSyncData: function(players, timelist, sessionid, data) {
        const GameData = data[0];
    
        timelist.push({
            t: 0,
            index: 0,
            reaction_num: GameData.reaction_num
        });
    
        let newplayers = [];
        for(let i = 1; i < data.length; i++) {
            const Data = data[i];
            
            let pl = players.find(function( obj ) {
                return obj.id == Data.id;
            });
    
            if(typeof pl === "undefined") {
                console.log("NEW PLAYER!");

                let newpl = {};
                Pl.player(newpl, Data);
                Pl.plSetClientData(newpl, Data);

                pl = newpl;
            }
    
            newplayers.push(pl);
    
            // Accept data
            pl.hp = Data.hp;
            pl.kills = Data.kills;
    
            // Accept synclist data
            if(pl.id == sessionid)
            {
                Pl.AcceptServerReactResults(pl, Data.synclist);
            }
            else
            {
                for(var j = 0; j < Data.synclist.length; j++) {
                    pl.synclist.push(Data.synclist[j]);
                }
            }
        }

        return newplayers;

    },

    CreateReactArrAndLSeenFrame: function(timelist, SEENTIME, max_react_num) {
        const reaction_arr = []; let last_frame_seen = -1;
        for (let i = timelist.length-1; i >= 0 && reaction_arr.length < max_react_num; i--) {
            if(timelist[i].t < SEENTIME) {
                reaction_arr.unshift(timelist[i].reaction_num);

                if(last_frame_seen == -1) last_frame_seen = i;
            }
        }
        return [reaction_arr, last_frame_seen];
    },

    AlignTimelist: function(timelist, SEENTIME, now, FRAMETIME, PingIn) {
        if(timelist.length == 0) return;

        timelist[0].t = SEENTIME - (FRAMETIME / 2);
        for(let i = 1; i < timelist.length; i++) {
            if(PingIn == 0) {
                timelist[i].index = 0;
            }

            if(timelist[i].index == 1) {
                timelist[i].t = timelist[i-1].t;
            } else {
                timelist[i].t = timelist[i-1].t + FRAMETIME;
            }

            if(timelist[i].t > now) {
                timelist[i].t = timelist[i-1].t;
                timelist[i].index = 1;
            }
        }

    },

    GetDataFromSynclist: function(pl, last_frame_seen) {
        if(last_frame_seen === -1
        || last_frame_seen > pl.synclist.length-1
        || pl.synclist[last_frame_seen] === undefined) {
            return;
        }

        pl.dir.x = pl.synclist[last_frame_seen].dir.x;
        pl.dir.y = pl.synclist[last_frame_seen].dir.y;

        pl.x = pl.synclist[last_frame_seen].x;
        pl.y = pl.synclist[last_frame_seen].y;

        pl.c.x = pl.x + pl.r;
        pl.c.y = pl.y + pl.r;

        pl.fire = pl.synclist[last_frame_seen].fire;
    },

    AcceptServerReactResults: function(main, server_synclist) {
        if(server_synclist.length === 0) return;
        const last_server_sync = server_synclist[server_synclist.length-1];

        // Clear solved reactions
        for (let i = main.synclist.length-1; i >= 0; i--) {
            if(main.synclist[i].last_react_sent === last_server_sync.last_react_processed)
            {
                main.synclist.splice(0, i+1);
                break;
            }
        }
        
        // Apply latest sync data
        main.x = last_server_sync.x;
        main.y = last_server_sync.y;
        main.c.x = main.x + main.r;
        main.c.y = main.y + main.r;
        main.energy = last_server_sync.energy;

        // Apply pending reactions
        for(let i = 0; i < main.synclist.length; i++) {
            Pl.Move(main, 1, Controller.mult(main.synclist[i].poschange, 1) );
            main.energy += main.synclist[i].energychange;
        }
    },

    AdjustSynclist: function(pl, timelist, align_threshold) {
        if(pl.synclist.length === timelist.length) {
            pl.desync = 0;
            return;
        }

        let delta = Math.abs(pl.synclist.length - timelist.length);
        pl.desync += delta;
        if(pl.desync < align_threshold) return;

        if(pl.synclist.length < timelist.length) {
            while(pl.synclist.length < timelist.length) pl.synclist.push(undefined);
        } else {
            pl.synclist.splice(0, delta);
        }
    },

    DoesShootPlayer: function(pl, hitlist) {
        if(!Pl.CanHeFire(pl) || !pl.fire) return false;

        for (let i = 0; i < hitlist.length; i++) {
            if(hitlist[i].obj.type == "pl" && Pl.IsHeAlive(hitlist[i].obj) )
            {
                return true;
            }
            else if(hitlist[i].obj.type == "wl")
            {
                return false;
            }
        }

        return false;
    },

    Move: function(pl, dist = 1, moveV = pl.velocity)
    {
        pl.x += moveV.x * dist;
        pl.y += moveV.y * dist;

        pl.c.x = pl.x + pl.r;
        pl.c.y = pl.y + pl.r;
        moveV.x = 0;
        moveV.y = 0;
    },

    PlayerTick: function(pl)
    {
        pl.undamaged = Math.min(200, pl.undamaged + 1);
    },

    Blink: function(pl)
    {
        if(pl.blink && pl.blink_energycost <= pl.energy) {
            Pl.Move(pl, pl.blinkspeedV, pl.norm_velocity);
            pl.norm_velocity = Controller.norm(pl.velocity);
            pl.energy -= pl.blink_energycost;
        }
    },

    Respawn: function(room, pl) {
        if(pl.respawn) {
            pl.x = Math.random() * room.width;
            pl.y = Math.random() * room.height;
            pl.hp = pl.hp_max;
            pl.energy = pl.max_energy;
            pl.undamaged = 0;
        }
    },

    EnergyRegen: function(pl) {
        pl.energy = Math.min(pl.max_energy, pl.energy + pl.energy_regen);
    },

    HpRegen: function(pl)
    {
        if(pl.undamaged >= pl.hp_regen_threshold && pl.hp > 0) {
            pl.hp = Math.min(pl.hp_max, pl.hp + pl.hp_regen);
        }
    },

    CanHePlay: function(pl) {
        return true;
    },

    CanHeMove: function(pl) {
        return this.IsHeAlive(pl);
    },

    CanHeBlink: function(pl) {
        return this.IsHeAlive(pl);
    },

    CanHeFire: function(pl) {
        return this.IsHeAlive(pl);
    },

    IsHeAlive: function(pl) {
        if(pl.hp > 0) {
            return true;
        }

        return false;
    },

    HandleCommands: function(pl, keys, eye) {
        if(!Pl.CanHePlay(pl)) return;

        // VELOCITY
        if (keys.left) pl.velocity.x += -1;
        if (keys.right) pl.velocity.x += 1;
        if (keys.up) pl.velocity.y += -1;
        if (keys.down) pl.velocity.y += 1;

        pl.norm_velocity = Controller.norm(pl.velocity);
        pl.velocity = Controller.mult(pl.norm_velocity, pl.speed);

        // DIRECTION
        pl.dir = Controller.norm( {x: keys.mouse.x - (pl.c.x - eye.x),
                                   y: keys.mouse.y - (pl.c.y - eye.y) } );
        
        // SHOOT
        if (keys.shoot) {
            pl.fire = true;
        } else {
            pl.fire = false;
        }

        // BLINK
        if (!keys.is_blink_pressed && keys.blink) {
            pl.blink = true;
            keys.is_blink_pressed = true;
        } else {
            pl.blink = false;
        }

        // RESPAWN
        if(keys.respawn) {
            pl.respawn = true;
            keys.respawn = false;
        } else {
            pl.respawn = false;
        }
    }

};

module.exports = Pl;

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

var Controller = __webpack_require__(1);

function v(a, b) {
    this.x = a;
    this.y = b;
}

function wall() {
    this.vertices = [];
    this.type = "wl";
}

wall.prototype.AddV = function(x, y) {
    this.vertices.push(new v(x, y));
};

wall.prototype.FindMidV = function() {
    var midV = this.vertices[0];
    for(var i = 0, l = this.vertices.length - 1; i < l; i++) {
        midV = Controller.mult(Controller.add(midV, this.vertices[i + 1]), 0.5);
    }
    this.midVert = midV;
};

wall.prototype.SetBoundingBox = function() {
    let minX = Infinity; let minY = Infinity;
    let maxX = -Infinity; let maxY = -Infinity;

    for (let i = 0; i < this.vertices.length; i++) {
        const vertex = this.vertices[i];
        minX = Math.min(minX, vertex.x);
        maxX = Math.max(maxX, vertex.x);

        minY = Math.min(minY, vertex.y);
        maxY = Math.max(maxY, vertex.y);
    }

    this.bb = Controller.CreateBoundingBox(minY, maxY, minX, maxX);
};

wall.prototype.CreateCorner = function(length, width) {
    this.vertices.push(new v(0, 0));
    this.vertices.push(new v(length, 0));
    this.vertices.push(new v(length, width));
    this.vertices.push(new v(length-10, width));
    this.vertices.push(new v(length-10, 10));
    this.vertices.push(new v(0, 10));
}

wall.prototype.Move = function(mx, my) {
    for (var i = 0; i < this.vertices.length; i++) {
        this.vertices[i].x += mx;
        this.vertices[i].y += my;
    }
}

wall.prototype.Scale = function(sx, sy) {
    for (var i = 0; i < this.vertices.length; i++) {
        this.vertices[i].x *= sx;
        this.vertices[i].y *= sy;
    }
}

module.exports = wall;

/***/ })
/******/ ]);