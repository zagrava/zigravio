var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io').listen(server);
const util = require('util');

const Controller = require("./private/Controller"); // Need to invoke Controller first
var ServerController = require('./private/ServerController');
const RoomRuler = require('./private/RoomRuler');
const Pl = require("./private/Player");

server.listen(8080);
console.log("Server running on localhost:8080");

app.use(express.static('./public'));

app.get('/', function(req, res) {
    res.sendFile(__dirname + '/public/views/index.html');
});

io.sockets.on('connection', function(socket) {
    
    socket.on('disconnect', function(data) {
        ServerController.DeletePlFromQueues(socket);
        ServerController.DeletePlFromRoom(socket);
    });

    socket.on('sync', function(data) {
        RoomRuler.AcceptAndArchiveInputs(socket, data);
    });

    socket.on('enter', function(data, func) {
        if(!ServerController.IsEnterDataOk(data)) {
            console.log("Enterdata is corrupted");
            return;
        }

        socket.name = data.name || "pro";

        ServerController.DeletePlFromQueues(socket);
        if(ServerController.FindSocketInSpace(socket)[0] == -1) {
            if(data.type == "DUEL") {
                ServerController.duelqueue.push(socket);
            } else if(data.type == "FFA") {
                ServerController.ffaqueue.push(socket);
            }
        }
    });



});

function update(FrameTime)
{
    ServerController.ResolveQueues(FrameTime);

	for(var r = 0; r < ServerController.space.length; r++)
	{
        const objects = ServerController.space[r].objects;
	    const archive = ServerController.space[r].archive;
        const gamehistory = ServerController.space[r].gamehistory;

        RoomRuler.ProcessInputs(ServerController.space[r]);

        ServerController.MainCheckRules(ServerController.space[r]);

        if(ServerController.space[r].gamestate.final != -1) {
            ServerController.CloseRoom(ServerController.space[r]);
            r--;
            continue;
        }

        //PUSH DATA
        var newarch = [];
        for(j = 0; j < objects.length; j++)
        {
            const newarch_obj_moment = {
                norm_velocity: {x: 0, y: 0},
                dir: {x: 0, y: 0},
                blink: false,
                fire: false,
                
                influence: {
                    dmg: [],
                }
            };

            Pl.TransformData(newarch_obj_moment, objects[j], true, false, true, false, false);
            newarch.push(newarch_obj_moment);
        }
        archive.push(newarch);

        newarch = [];
        for(j = 0; j < objects.length; j++)
        {
            const newgamehistory_obj_moment = {};

            Pl.TransformData(newgamehistory_obj_moment, objects[j], true, false, false, false, false);
            newarch.push(newgamehistory_obj_moment);
        }
        gamehistory.push(newarch);
        gamehistory.shift();

        RoomRuler.SendData(ServerController.space[r], FrameTime);

        for(k = 0; k < objects.length; k++)
        {
            objects[k].react_indexes = [];
        }

        archive.shift();
    }
    
    ServerController.timelist.push([ ServerController.timelist[0][0], FrameTime ]);
    ServerController.timelist.shift();

}

var tickLengthMs = 1000 / 60;

/* gameLoop related variables */
// timestamp of each loop
var Time = process.hrtime();
var Start = getMS(process.hrtime());
var Curr = getMS(process.hrtime());
var Curr1 = Curr;
var delta = 0;

var Last = getMS(process.hrtime());
// number of times gameLoop gets called
var actualTicks = 0;
var ImmediateCalls = 0;
var TimeOutCalls = 0;

///////////////////////////////////////////
var frseconds = 0;
var isFirst = true;
var Start1;
var frames1 = 0;
var frames = 0;
///////////////////////////////////////////

var Prev = getMS(process.hrtime());

function getMS(hrtime) {
	return ( hrtime[0]*1000 + hrtime[1]/1000000 );
}

var gameLoop = function () {
    if(isFirst) {
        Start1 = Date.now();
        isFirst = false;
        //console.log("STARTSTARTTTTTTTTTTTTTTSTARTTTTTTSTARTTasdasdasfasdTTTSTARTTTTTSTARTTTTTSTARTTTTTSTARTTTTTSTARTTTTTTTTTTTTTTTTTTTTTTTTTTT")
    }

    var Now = getMS(process.hrtime());

    var delta = Now - Prev;
    if (delta >= tickLengthMs)
    {
        frames1++
        //console.log( "SHOULD: ", (Date.now() - Start1) / (1000/60), "IS: ", frames1);

        //console.log(Prev, " + ", tickLengthMs, "=", (Prev + tickLengthMs) );
        Prev += tickLengthMs;

        frseconds += delta/1000;
        if(frseconds >= 1.0) {
            //console.log(frames + "!!!" + frseconds);
            frames = 0;
            frseconds = 0;
        }
        frames++;

        //console.log(delta,'DELTA');

        update(Prev);

        Curr += tickLengthMs;
        Curr1 += delta;

        var current = getMS(process.hrtime());

        //WORKING WORKING WORKING
        //console.log('Time', (current - Start)/tickLengthMs, '(target: ' + tickLengthMs +' ms)', 'node ticks', actualTicks, 'immediate calls', ImmediateCalls, 'timeout calls', TimeOutCalls);

        actualTicks = 0; ImmediateCalls = 0; TimeOutCalls = 0;
    }

    if ( (getMS(process.hrtime())-Prev) <= tickLengthMs - 2)
    {
        setTimeout(gameLoop);
        TimeOutCalls++;
    } else
    {
        setImmediate(gameLoop);
        ImmediateCalls++;
    }
    actualTicks++;
};

gameLoop();


