'use strict';

const expect = require("chai").expect;
const sinon = require("sinon");

const Pl = require("../private/Player");
const ServerController = require("../private/ServerController");

describe("Pl", function() {
    before(function () {

    });

    describe("player", function() {

        it("G: dataobj W: dataobj R: ___ D: Creates new player", function() {
            const obj_data = {
                x: 1,
                y: 2,
                r: 3,

                hp: 4,
                hp_max: 5,
                hp_regen: 6,
                hp_regen_threshold: 7,
                dmg: 8,
                speed: 9,
                blinkspeedV: 10,
                blink_energycost: 11,
                range: 12,
                energy: 13,
                energy_regen: 14,
                max_energy: 15,

                id: "16",
                name: "Petro",
                color: "rgb(120,140,150)",
                skin: "17skin",
                aimskin: "18aimskin",
                ammoskin: "19ammoskin"
            };
            const new_player = {};

            Pl.player(new_player, obj_data);

            expect(new_player.x, "x").to.equal(1);
            expect(new_player.y, "y").to.equal(2);
            expect(new_player.r, "r").to.equal(3);
            expect(new_player.c, "c").deep.equals({x: 4, y: 5});
            expect(new_player.type, "type").to.equal("pl");

            expect(new_player.norm_velocity, "norm_velocity").deep.equals({x: 0, y: 0});
            expect(new_player.velocity, "velocity").deep.equals({x: 0, y: 0});
            expect(new_player.dir, "dir").deep.equals({x: 1, y: 0});
            expect(new_player.blink, "blink").equals(false);
            expect(new_player.fire, "fire").equals(false);

            expect(new_player.hp, "hp").to.equal(4);
            expect(new_player.hp_max, "hp_max").to.equal(5);
            expect(new_player.hp_regen, "hp_regen").to.equal(6);
            expect(new_player.hp_regen_threshold, "hp_regen_threshold").to.equal(7);
            expect(new_player.dmg, "dmg").to.equal(8);
            expect(new_player.speed, "speed").to.equal(9);
            expect(new_player.blinkspeedV, "blinkspeedV").to.equal(10);
            expect(new_player.blink_energycost, "blink_energycost").to.equal(11);
            expect(new_player.range, "range").to.equal(12);
            expect(new_player.energy, "energy").to.equal(13);
            expect(new_player.energy_regen, "energy_regen").to.equal(14);
            expect(new_player.max_energy, "max_energy").to.equal(15);
            expect(new_player.undamaged, "undamaged").equals(0);

            expect(new_player.id, "id").to.equal("16");
            expect(new_player.name, "name").to.equal("Petro");
            expect(new_player.color, "color").to.equal("rgb(120,140,150)");
            expect(new_player.skin, "skin").to.equal("17skin");
            expect(new_player.aimskin, "aimskin").equals("18aimskin");
            expect(new_player.ammoskin, "ammoskin").equals("19ammoskin");
        });

    });

    describe("plSetClientData", function() {

        it("G: pl+dataobj W: pl+dataobj R: ___ D: Generates ClientData for pl", function() {
            const new_player = {};
            const data = {
                kills: 9
            };

            Pl.plSetClientData(new_player, data);

            expect(new_player.synclist).is.an('array').that.is.empty;
            expect(new_player.desync).equals(0);
            expect(new_player.kills).equals(9);
        });

    });

    describe("TransformData", function() {

        it("G: obj+obj W: obj+obj R: ___ D: Successfully translates data", function() {
            const ObjTo = {
                x: 1,
                y: 2,
                r: 3,
                c: { x: 16, y: 17 },
                type: "123",

                hp: 4,
                hp_max: 5,
                hp_regen: 6,
                hp_regen_threshold: 7,
                dmg: 8,
                speed: 9,
                energy: 10,
                blinkspeedV: 11,
                blink_energycost: 12,
                range: 13,
                energy_regen: 14,
                max_energy: 15,
                undamaged: 18,
            };

            const ObjFrom = {
                x: -1,
                y: -2,
                r: -3,
                c: { x: -16, y: -17 },
                type: "-123",

                hp: -4,
                hp_max: -5,
                hp_regen: -6,
                hp_regen_threshold: -7,
                dmg: -8,
                speed: -9,
                energy: -10,
                blinkspeedV: -11,
                blink_energycost: -12,
                range: -13,
                energy_regen: -14,
                max_energy: -15,
                undamaged: -18,
            };

            Pl.TransformData(ObjTo, ObjFrom, true, false, true);

            expect(ObjTo.x).to.equal(-1);
            expect(ObjTo.y).to.equal(-2);
            expect(ObjTo.r).to.equal(-3);
            expect(ObjTo.c).deep.equals({x: -16, y: -17});
            expect(ObjTo.type).to.equal("-123");

            expect(ObjTo.hp).to.equal(-4);
            expect(ObjTo.hp_max).to.equal(-5);
            expect(ObjTo.hp_regen).to.equal(-6);
            expect(ObjTo.hp_regen_threshold).to.equal(-7);
            expect(ObjTo.dmg).to.equal(-8);
            expect(ObjTo.speed).to.equal(-9);
            expect(ObjTo.energy).to.equal(-10);
            expect(ObjTo.blinkspeedV).to.equal(-11);
            expect(ObjTo.blink_energycost).to.equal(-12);
            expect(ObjTo.range).to.equal(-13);
            expect(ObjTo.energy_regen).to.equal(-14);
            expect(ObjTo.max_energy).to.equal(-15);
            expect(ObjTo.undamaged).equals(-18);
        });

        it("G: pl+arch_obj_moment W: pl+arch_obj_moment R: ___ D: Successfully translates data", function() {
            const player = {
                norm_velocity: {x: 1, y: 2, },
                velocity: {x: 3, y: 4, },
                dir: {x: 5, y: 6, },
                blink: true,
                speed: 5,
                fire: false
            };

            const archive_object_moment = {
                norm_velocity: {x: -1, y: -2, },
                velocity: {x: -3, y: -4, },
                dir: {x: -5, y: -6, },
                blink: false,
                fire: true
            };

            Pl.TransformData(player, archive_object_moment, false, true, false);

            expect(player.norm_velocity).deep.equals({x: -1, y: -2});
            expect(player.velocity).deep.equals({x: -5, y: -10});
            expect(player.dir).deep.equals({x: -5, y: -6});
            expect(player.blink).to.equal(false);
            expect(player.fire).to.equal(true);
        });

        it("G: pl+arch_obj_moment W: pl+arch_obj_moment R: ___ D: Successfully translates data", function() {
            const EmptyObjTo = {
                speed: 5
            };

            const ObjFrom = {
                x: -1,
                y: -2,
                r: -3,
                c: { x: -16, y: -17 },
                type: "-123",

                hp: -4,
                hp_max: -5,
                hp_regen: -6,
                hp_regen_threshold: -7,
                dmg: -8,
                speed: -9,
                energy: -10,
                blinkspeedV: -11,
                blink_energycost: -12,
                range: -13,
                energy_regen: -14,
                max_energy: -15,
                undamaged: -18,

                norm_velocity: {x: -1, y: -2, },
                velocity: {x: -3, y: -4, },
                dir: {x: -5, y: -6, },
                blink: false,
                fire: true
            };

            Pl.TransformData(EmptyObjTo, ObjFrom, true, true, true);

            
            expect(EmptyObjTo.x).to.equal(-1);
            expect(EmptyObjTo.y).to.equal(-2);
            expect(EmptyObjTo.r).to.equal(-3);
            expect(EmptyObjTo.c).deep.equals({x: -16, y: -17});
            expect(EmptyObjTo.type).to.equal("-123");

            expect(EmptyObjTo.hp).to.equal(-4);
            expect(EmptyObjTo.hp_max).to.equal(-5);
            expect(EmptyObjTo.hp_regen).to.equal(-6);
            expect(EmptyObjTo.hp_regen_threshold).to.equal(-7);
            expect(EmptyObjTo.dmg).to.equal(-8);
            expect(EmptyObjTo.speed).to.equal(-9);
            expect(EmptyObjTo.energy).to.equal(-10);
            expect(EmptyObjTo.blinkspeedV).to.equal(-11);
            expect(EmptyObjTo.blink_energycost).to.equal(-12);
            expect(EmptyObjTo.range).to.equal(-13);
            expect(EmptyObjTo.energy_regen).to.equal(-14);
            expect(EmptyObjTo.max_energy).to.equal(-15);
            expect(EmptyObjTo.undamaged).equals(-18);

            expect(EmptyObjTo.norm_velocity).deep.equals({x: -1, y: -2});
            expect(EmptyObjTo.velocity).deep.equals({x: -5, y: -10});
            expect(EmptyObjTo.dir).deep.equals({x: -5, y: -6});
            expect(EmptyObjTo.blink).to.equal(false);
            expect(EmptyObjTo.fire).to.equal(true);
        });

    });

    describe("AcceptSyncData", function() {

        it("E: objs[0]=main+obj[1].synclist.l=3+sinon.stub() R: ___ D: obj[1].synclist.l=5+hp/kills", function() {
            // Prepare
            const temp_AcceptServerReactResults = Pl.AcceptServerReactResults;
            Pl.AcceptServerReactResults = sinon.stub();

            let objects = [{
                id: "12da",
                x: 1,
                y: 1,
                r: 4,
                c: {
                    x: 5,
                    y: 5
                },
                velocity: {
                    x: 0,
                    y: 1
                }
            },
            {
                id: "234fc",
                x: 2,
                y: 2,
                r: 4,
                c: {
                    x: 6,
                    y: 6
                },
                velocity: {
                    x: 1,
                    y: 0
                },
                synclist: [1,2,3]
            }];

            const timelist = [1,2,3];
            const sessionid = "12da";

            const data = [{
                time: "TEST",
                gamestuff: "TEST1",
                reaction_num: 10
            },
            {
                id: "12da",
                hp: 100,
                kills: 20
            },
            {
                id: "234fc",
                hp: 200,
                kills: 40,
                synclist: [4,5]
            }];

            // Act
            objects = Pl.AcceptSyncData(objects, timelist, sessionid, data);
            
            // Test
            expect(objects.length).equals(2);

            expect(timelist.length).equals(4);
            expect(timelist[3].reaction_num).equals(10);

            expect(objects[0].hp).equals(100);
            expect(objects[0].kills).equals(20);
            expect(Pl.AcceptServerReactResults.calledOnce).is.true;

            expect(objects[1].hp).equals(200);
            expect(objects[1].kills).equals(40);
            expect(objects[1].synclist).deep.equals([1,2,3,4,5]);

            // Clear
            Pl.AcceptServerReactResults = temp_AcceptServerReactResults;
        });

        it("E: objs[0]=main+sinon.stub() G: new player R: ___ D: obj[1].synclist.l=5+hp/kills", function() {
            // Prepare
            const temp_AcceptServerReactResults = Pl.AcceptServerReactResults;
            Pl.AcceptServerReactResults = sinon.stub();

            let objects = [{
                id: "12da",
                x: 1,
                y: 1,
                r: 4,
                c: {
                    x: 5,
                    y: 5
                },
                velocity: {
                    x: 0,
                    y: 1
                }
            }];

            const timelist = [1,2,3];
            const sessionid = "12da";

            const data = [{
                time: "TEST",
                gamestuff: "TEST1",
                reaction_num: 10
            },
            {
                id: "12da",
                hp: 100,
                kills: 20
            },
            {
                id: "234fc",
                hp: 200,
                kills: 40,
                synclist: [4,5]
            }];

            // Act
            objects = Pl.AcceptSyncData(objects, timelist, sessionid, data);
            
            // Test
            expect(objects.length).equals(2);

            expect(timelist.length).equals(4);
            expect(timelist[3].reaction_num).equals(10);

            expect(objects[0].hp).equals(100);
            expect(objects[0].kills).equals(20);
            expect(Pl.AcceptServerReactResults.calledOnce).is.true;

            expect(objects[1].hp).equals(200);
            expect(objects[1].kills).equals(40);
            expect(objects[1].synclist).deep.equals([4,5]);

            // Clear
            Pl.AcceptServerReactResults = temp_AcceptServerReactResults;
        });

    });

    describe("CreateReactArrAndLSeenFrame", function() {

        it("G: obj+dist+moveV +++ obj W: obj+dist+moveV/obj R: ___ D: Moves pl", function() {
            // Prepare 1
            let now = 10;
            let RENDERTIME = 5;

            const timelist = [
            { t: 4.5, reaction_num: 16 },

            { t: 5.5, reaction_num: 17 },
            { t: 6.5, reaction_num: 18 },
            { t: 7.5, reaction_num: 19 },
            { t: 8.5, reaction_num: 20 },
            { t: 9.5, reaction_num: 21 }];

            // Act 1
            const result_arr = Pl.CreateReactArrAndLSeenFrame(timelist, now - RENDERTIME, ServerController.max_react_num);
            const reaction_arr = result_arr[0];
            const index = result_arr[1];
            
            // Test 1
            expect(reaction_arr.length).equals(1);
            expect(reaction_arr[0]).equals(16);
            expect(index).equals(0);
        });

        it("G: obj+dist+moveV +++ obj W: obj+dist+moveV/obj R: ___ D: Moves pl", function() {
            // Prepare 2
            let now = 10;
            let RENDERTIME = 5;

            const timelist = [
            { t: 4.5, reaction_num: 16 }, //0
            { t: 4.5, reaction_num: 17 }, //1

            { t: 4.5, reaction_num: 18 }, //2
            { t: 4.5, reaction_num: 19 }, //3
            { t: 4.5, reaction_num: 20 }, //4
            { t: 4.5, reaction_num: 21 }, //5
            { t: 4.5, reaction_num: 22 }, //6

            { t: 5.5, reaction_num: 23 }, //7
            { t: 6.5, reaction_num: 24 }, //8
            { t: 7.5, reaction_num: 25 }, //9
            { t: 8.5, reaction_num: 26 }];//10

            // Act 2
            const result_arr = Pl.CreateReactArrAndLSeenFrame(timelist, now - RENDERTIME, ServerController.max_react_num);
            const reaction_arr = result_arr[0];
            const index = result_arr[1];

            // Test 2
            expect(reaction_arr.length).equals(5);
            expect(reaction_arr[0]).equals(18);
            expect(reaction_arr[1]).equals(19);
            expect(reaction_arr[2]).equals(20);
            expect(reaction_arr[3]).equals(21);
            expect(reaction_arr[4]).equals(22);
            expect(index).equals(6);
        });

        it("G: obj+dist+moveV +++ obj W: obj+dist+moveV/obj R: ___ D: Moves pl", function() {
            // Prepare 3
            let now = 10;
            let RENDERTIME = 5;
            const timelist = [];

            // Act 3
            const result_arr = Pl.CreateReactArrAndLSeenFrame(timelist, now - RENDERTIME, ServerController.max_react_num);
            const reaction_arr = result_arr[0];
            const index = result_arr[1];

            // Test 3
            expect(reaction_arr.length).equals(0);
            expect(index).equals(-1);
        });

    });

    describe("AlignTimelist", function() {

        it("G: timelist.l=7 +++ 3 cycles(1 aditional timemoment each) PingIn=1 R: ___ D: Alignes timelist", function() {
            // Prepare 1
            let now = 110;
            let RENDERTIME = 10;
            let FRAMETIME = 2;
            let PingIn = 1;

            const timelist = [
            { t: 0, index: 0 },
            { t: 0, index: 0 },
            { t: 0, index: 1 },
            { t: 0, index: 0 },
            { t: 0, index: 0 },
            { t: 0, index: 0 }];

            // Act 1
            Pl.AlignTimelist(timelist, now - RENDERTIME, now, FRAMETIME, PingIn);
            
            // Test 1
            expect(timelist.length, "T1: timelist.length").equals(6);
            expect(timelist[0].t, "T1: timelist[0].t").equals(99); expect(timelist[0].index, "T1: timelist[0].index").equals(0);
            expect(timelist[1].t, "T1: timelist[1].t").equals(101); expect(timelist[1].index, "T1: timelist[1].index").equals(0);
            expect(timelist[2].t, "T1: timelist[2].t").equals(101); expect(timelist[2].index, "T1: timelist[2].index").equals(1);
            expect(timelist[3].t, "T1: timelist[3].t").equals(103); expect(timelist[3].index, "T1: timelist[3].index").equals(0);
            expect(timelist[4].t, "T1: timelist[4].t").equals(105); expect(timelist[4].index, "T1: timelist[4].index").equals(0);
            expect(timelist[5].t, "T1: timelist[5].t").equals(107); expect(timelist[5].index, "T1: timelist[5].index").equals(0);

            // Prepare 2
            timelist.splice(0, 0+1);
            timelist.push({
                t: 0,
                index: 0
            });

            // Act 2
            Pl.AlignTimelist(timelist, now - RENDERTIME, now, FRAMETIME, PingIn);

            // Test 2
            expect(timelist.length).equals(6, "T2: timelist.length");
            expect(timelist[0].t).equals(99, "T2: timelist[0].t"); expect(timelist[0].index, "T2: timelist[0].index").equals(0);
            expect(timelist[1].t).equals(99, "T2: timelist[1].t"); expect(timelist[1].index, "T2: timelist[1].index").equals(1);
            expect(timelist[2].t).equals(101, "T2: timelist[2].t"); expect(timelist[2].index, "T2: timelist[2].index").equals(0);
            expect(timelist[3].t).equals(103, "T2: timelist[3].t"); expect(timelist[3].index, "T2: timelist[3].index").equals(0);
            expect(timelist[4].t).equals(105, "T2: timelist[4].t"); expect(timelist[4].index, "T2: timelist[4].index").equals(0);
            expect(timelist[5].t).equals(107, "T2: timelist[5].t"); expect(timelist[5].index, "T2: timelist[5].index").equals(0);

            // Prepare 3
            timelist.splice(0, 1+1);
            timelist.push({
                t: 0,
                index: 0
            });

            // Act 3
            Pl.AlignTimelist(timelist, now - RENDERTIME, now, FRAMETIME, PingIn);

            // Test 3
            expect(timelist.length).equals(5, "T3: timelist.length");
            expect(timelist[0].t).equals(99, "T3: timelist[0].t"); expect(timelist[0].index, "T3: timelist[0].index").equals(0);
            expect(timelist[1].t).equals(101, "T3: timelist[1].t"); expect(timelist[1].index, "T3: timelist[1].index").equals(0);
            expect(timelist[2].t).equals(103, "T3: timelist[2].t"); expect(timelist[2].index, "T3: timelist[2].index").equals(0);
            expect(timelist[3].t).equals(105, "T3: timelist[3].t"); expect(timelist[3].index, "T3: timelist[3].index").equals(0);
            expect(timelist[4].t).equals(107, "T3: timelist[4].t"); expect(timelist[4].index, "T3: timelist[4].index").equals(0);
        });

        it("G: timelist[3,4,5].index=1 PingIn=0 R: ___ D: Alignes timelist", function() {
            // Prepare 1
            let now = 110;
            let RENDERTIME = 10;
            let FRAMETIME = 2;
            let PingIn = 0;

            const timelist = [
            { t: 0, index: 0 },
            { t: 0, index: 0 },
            { t: 0, index: 0 },
            { t: 0, index: 1 },
            { t: 0, index: 1 },
            { t: 0, index: 1 }];

            // Act 1
            Pl.AlignTimelist(timelist, now - RENDERTIME, now, FRAMETIME, PingIn);
            
            // Test 1
            expect(timelist.length, "T1: timelist.length").equals(6);
            expect(timelist[0].t, "T1: timelist[0].t").equals(99); expect(timelist[0].index, "T1: timelist[0].index").equals(0);
            expect(timelist[1].t, "T1: timelist[1].t").equals(101); expect(timelist[1].index, "T1: timelist[1].index").equals(0);
            expect(timelist[2].t, "T1: timelist[2].t").equals(103); expect(timelist[2].index, "T1: timelist[2].index").equals(0);
            expect(timelist[3].t, "T1: timelist[3].t").equals(105); expect(timelist[3].index, "T1: timelist[3].index").equals(0);
            expect(timelist[4].t, "T1: timelist[4].t").equals(107); expect(timelist[4].index, "T1: timelist[4].index").equals(0);
            expect(timelist[5].t, "T1: timelist[5].t").equals(109); expect(timelist[5].index, "T1: timelist[5].index").equals(0);
        });

        it("G: timelist.l=21 PingIn=1 R: ___ D: Alignes timelist", function() {
            // Prepare 1
            let now = 110;
            let RENDERTIME = 10;
            let FRAMETIME = 2;
            let PingIn = 1;

            const timelist = [
            { t: 0, index: 0 },//0
            { t: 0, index: 0 },//1
            { t: 0, index: 0 },//2
            { t: 0, index: 1 },//3
            { t: 0, index: 0 },//4
            { t: 0, index: 0 },//5
            { t: 0, index: 0 },//6
            { t: 0, index: 0 },//7
            { t: 0, index: 0 },//8
            { t: 0, index: 0 },//9
            { t: 0, index: 0 },//10
            { t: 0, index: 0 },//11
            { t: 0, index: 0 },//12
            { t: 0, index: 0 },//13
            { t: 0, index: 0 },//14
            { t: 0, index: 0 },//15
            { t: 0, index: 0 },//16
            { t: 0, index: 0 },//17
            { t: 0, index: 0 },//18
            { t: 0, index: 0 },//19
            { t: 0, index: 0 }];//20

            // Act 1
            Pl.AlignTimelist(timelist, now - RENDERTIME, now, FRAMETIME, PingIn);
            
            // Test 1
            expect(timelist.length, "T1: timelist.length").equals(21);
            expect(timelist[0].t, "T1: timelist[0].t").equals(99); expect(timelist[0].index, "T1: timelist[0].index").equals(0); //0
            expect(timelist[1].t, "T1: timelist[1].t").equals(101); expect(timelist[1].index, "T1: timelist[1].index").equals(0);//1
            expect(timelist[2].t, "T1: timelist[2].t").equals(103); expect(timelist[2].index, "T1: timelist[2].index").equals(0);//2
            expect(timelist[3].t, "T1: timelist[3].t").equals(103); expect(timelist[3].index, "T1: timelist[3].index").equals(1);//3
            expect(timelist[4].t, "T1: timelist[4].t").equals(105); expect(timelist[4].index, "T1: timelist[4].index").equals(0);//4
            expect(timelist[5].t, "T1: timelist[5].t").equals(107); expect(timelist[5].index, "T1: timelist[5].index").equals(0);//5
            expect(timelist[6].t, "T1: timelist[6].t").equals(109); expect(timelist[6].index, "T1: timelist[6].index").equals(0);//6
            for (let i = 7; i <= 20; i++) {
                expect(timelist[i].t, "T1: timelist["+i+"].t").equals(109); expect(timelist[i].index, "T1: timelist["+i+"].index").equals(1);
            }
        });

        it("G: timelist.l=1 R: ___ D: Alignes timelist", function() {
            // Prepare 1
            let now = 110;
            let RENDERTIME = 10;
            let FRAMETIME = 2;
            let PingIn = 1;

            const timelist = [ { t: 0, index: 0 } ]; //0

            // Act 1
            Pl.AlignTimelist(timelist, now - RENDERTIME, now, FRAMETIME, PingIn);
            
            // Test 1
            expect(timelist.length, "T1: timelist.length").equals(1);
            expect(timelist[0].t, "T1: timelist[0].t").equals(99); expect(timelist[0].index, "T1: timelist[0].index").equals(0); //0
        });

        it("G: timelist.l=0 R: ___ D: Alignes timelist", function() {
            // Prepare 1
            let now = 110;
            let RENDERTIME = 10;
            let FRAMETIME = 2;
            let PingIn = 1;

            const timelist = [];

            // Act 1
            Pl.AlignTimelist(timelist, now - RENDERTIME, now, FRAMETIME, PingIn);
            
            // Test 1
            expect(timelist.length, "T1: timelist.length").equals(0);
        });

    });

    describe("GetDataFromSynclist", function() {

        it("S: last_frame_seen=2 R: ___ D: Success", function() {
            // Prepare
            const objects = [{
                x: 1,
                y: 1,
                r: 5,
                dir: {x: 0, y: 0},
                c: {
                    x: 0,
                    y: 0
                },
                synclist: [{
                    empty: true,
                },
                {
                    empty: true,
                },
                {
                    dir: {x: 0.707, y: 0.707},
                    x: 11,
                    y: 11,
                }]
            }];

            // Act
            Pl.GetDataFromSynclist(objects[0], 2);
            
            // Test
            expect(objects[0].x).equals(11); expect(objects[0].y).equals(11);
            expect(objects[0].c.x).equals(16); expect(objects[0].c.y).equals(16);
            expect(objects[0].dir.x).equals(0.707); expect(objects[0].dir.y).equals(0.707);
        });

        it("S: o.synclist.l=0 +++ o.synclist[0]=undefined +++ o.synclist.l=2+last_frame_seen=2 R: ___ D: Success", function() {
            // Prepare 1
            const objects = [{
                x: 1,
                y: 1,
                r: 5,
                dir: {x: 0, y: 0},
                c: {
                    x: 0,
                    y: 0
                },
                synclist: []
            }];

            // Act 1
            Pl.GetDataFromSynclist(objects[0], 0);
            
            // Test 1
            expect(objects[0].x, "T1: x").equals(1); expect(objects[0].y, "T1: y").equals(1);
            expect(objects[0].c.x, "T1: c.x").equals(0); expect(objects[0].c.y, "T1: c.y").equals(0);
            expect(objects[0].dir.x, "T1: dir.x").equals(0); expect(objects[0].dir.y, "T1: dir.y").equals(0);

            // Prepare 2
            objects[0].synclist.push({},{},undefined);

            // Act 2
            Pl.GetDataFromSynclist(objects[0], 2);

            // Test 2
            expect(objects[0].x, "T2: x").equals(1); expect(objects[0].y, "T2: y").equals(1);
            expect(objects[0].c.x, "T2: c.x").equals(0); expect(objects[0].c.y, "T2: c.y").equals(0);
            expect(objects[0].dir.x, "T2: dir.x").equals(0); expect(objects[0].dir.y, "T2: dir.y").equals(0);

            // Prepare 3
            objects[0].synclist.pop();

            // Act 3
            Pl.GetDataFromSynclist(objects[0], 2);

            // Test 3
            expect(objects[0].x, "T2: x").equals(1); expect(objects[0].y, "T2: y").equals(1);
            expect(objects[0].c.x, "T2: c.x").equals(0); expect(objects[0].c.y, "T2: c.y").equals(0);
            expect(objects[0].dir.x, "T2: dir.x").equals(0); expect(objects[0].dir.y, "T2: dir.y").equals(0);
        });

        it("S: last_frame_seen=-1 R: ___ D: Success", function() {
            // Prepare
            const objects = [{
                x: 1,
                y: 1,
                r: 5,
                dir: {x: 0, y: 0},
                c: {
                    x: 0,
                    y: 0
                },
                synclist: [{
                    empty: true,
                }]
            }];

            // Act
            Pl.GetDataFromSynclist(objects[0], -1);
            
            // Test
            expect(objects[0].x).equals(1); expect(objects[0].y).equals(1);
            expect(objects[0].c.x).equals(0); expect(objects[0].c.y).equals(0);
            expect(objects[0].dir.x).equals(0); expect(objects[0].dir.y).equals(0);
        });

    });

    describe("AcceptServerReactResults", function() {

        it("G: pl.synclist=[39,1,4,6,7]+server_synclist.last=4 D: Accepts react. results", function() {
            // Prepare
            const player = {
                x: 33,
                y: 0,
                c: {x: 0, y: 0},
                r: 5,
                energy: 111,
                synclist: [
                    {
                        poschange: {x:9,y:0},
                        energychange: 3,
                        last_react_sent: 39
                    },
                    {
                        poschange: {x:6,y:0},
                        energychange: 2,
                        last_react_sent: 1
                    },
                    {
                        poschange: {x:9,y:0},
                        energychange: 3,
                        last_react_sent: 4
                    },
                    {
                        poschange: {x:6,y:0},
                        energychange: 2,
                        last_react_sent: 6
                    },
                    {
                        poschange: {x:3,y:0},
                        energychange: 1,
                        last_react_sent: 7
                    },
                ]
            };

            const server_synclist = [37,38,39,0,1,2,3,{
                last_react_processed: 4,
                x: 24,
                y: 0,
                energy: 108
            }];
            
            // Act
            Pl.AcceptServerReactResults(player, server_synclist);

            // Test
            expect(player.synclist.length).equals(2);
            expect(player.x).equals(33);
            expect(player.y).equals(0);
            expect(player.energy).equals(111);
        });

        it("G: pl.synclist=[38,39,0,1]+server_synclist.last=38 D: Accepts react. results", function() {
            // Prepare
            const player = {
                x: 52,
                y: 0,
                c: {x: 0, y: 0},
                r: 5,
                energy: 64,
                synclist: [
                    {
                        poschange: {x:3,y:0},
                        energychange: 1,
                        last_react_sent: 38
                    },
                    {
                        poschange: {x:3,y:0},
                        energychange: 1,
                        last_react_sent: 39
                    },
                    {
                        poschange: {x:3,y:0},
                        energychange: 1,
                        last_react_sent: 0
                    },
                    {
                        poschange: {x:3,y:0},
                        energychange: 1,
                        last_react_sent: 1
                    },
                ]
            };

            const server_synclist = [{
                last_react_processed: 38,
                x: 43,
                y: 0,
                energy: 61
            }];
            
            // Act
            Pl.AcceptServerReactResults(player, server_synclist);

            // Test
            expect(player.synclist.length).equals(3);
            expect(player.x).equals(52);
            expect(player.y).equals(0);
            expect(player.energy).equals(64);
        });

        it("G: pl.synclist=[38,39]+server_synclist.l=0 D: Immediately exits func.", function() {
            // Prepare
            const player = {
                x: 52,
                y: 0,
                c: {x: 0, y: 0},
                r: 5,
                energy: 64,
                synclist: [
                    {
                        poschange: {x:3,y:0},
                        energychange: 1,
                        last_react_sent: 38
                    },
                    {
                        poschange: {x:3,y:0},
                        energychange: 1,
                        last_react_sent: 39
                    }
                ]
            };
            const server_synclist = [];
            
            // Act
            Pl.AcceptServerReactResults(player, server_synclist);

            // Test
            expect(player.synclist.length).equals(2);
            expect(player.x).equals(52);
            expect(player.y).equals(0);
            expect(player.energy).equals(64);
        });

    });

    describe("AdjustSynclist", function() {

        it("G: mvl.l=3+tl.l=5+desync=64+treshold=100 R: ___ D: Nothing", function() {
            // Prepare
            const player = {
                synclist: [1,2],
                desync: 98
            };

            const timelist = [1,2];

            // Act
            Pl.AdjustSynclist(player, timelist, 100);
            
            // Test
            expect(player.desync).equals(0);
            expect(player.synclist.length).equals(2);
            expect(timelist.length).equals(2);
        });

        it("G: mvl.l=3+tl.l=5+desync=64+treshold=100 R: ___ D: Nothing", function() {
            // Prepare
            const player = {
                synclist: [1,2,3],
                desync: 64
            };

            const timelist = [1,2,3,4,5];

            // Act
            Pl.AdjustSynclist(player, timelist, 100);
            
            // Test
            expect(player.desync).equals(66);
            expect(player.synclist.length).equals(3);
            expect(timelist.length).equals(5);
        });

        it("G: mvl.l=3+tl.l=5+desync=64+treshold=100 R: ___ D: Nothing", function() {
            // Prepare
            const player = {
                synclist: [1,2],
                desync: 98
            };

            const timelist = [1,2,3,4,5];

            // Act
            Pl.AdjustSynclist(player, timelist, 100);
            
            // Test
            expect(player.desync).equals(101);
            expect(player.synclist.length).equals(5);
            expect(player.synclist[2]).equals(undefined);
            expect(player.synclist[3]).equals(undefined);
            expect(player.synclist[4]).equals(undefined);
            expect(timelist.length).equals(5);
        });

        it("G: mvl.l=3+tl.l=5+desync=64+treshold=100 R: ___ D: Nothing", function() {
            // Prepare
            const player = {
                synclist: [1,2,3],
                desync: 99
            };

            const timelist = [];

            // Act
            Pl.AdjustSynclist(player, timelist, 100);
            
            // Test
            expect(player.desync).equals(102);
            expect(player.synclist.length).equals(0);
            expect(timelist.length).equals(0);
        });

    });

    describe("Move", function() {

        it("G: obj+dist+moveV +++ obj W: obj+dist+moveV/obj R: ___ D: Moves pl", function() {
            // Prepare
            const player = {
                x: 3,
                y: 10,
                r: 4,
                c: {
                    x: 7,
                    y: 14
                },
                velocity: {
                    x: 0,
                    y: 1
                }
            };
            const moveV = {x: 1, y: 0};

            // Act
            Pl.Move(player, 4, moveV);
            
            // Test
            expect(player.x).equals(7); expect(player.y).equals(10);
            expect(player.c.x).equals(11); expect(player.c.y).equals(14);

            // Act
            Pl.Move(player);

            // Test
            expect(player.x).equals(7); expect(player.y).equals(11);
            expect(player.c.x).equals(11); expect(player.c.y).equals(15);
        });

    });

    describe("PlayerTick", function() {

        it("S: 3 cycles: energy=142+max=150+undamaged=198 G: obj W: obj R: ___ D: Executes PlayerTick", function() {
            // Prepare
            const player = {
                undamaged: 198
            };

            // Act1
            Pl.PlayerTick(player);
            // Test1
            expect(player.undamaged).equals(199);

            // Act2
            Pl.PlayerTick(player);
            // Test2
            expect(player.undamaged).equals(200);

            // Act3
            Pl.PlayerTick(player);
            // Test3
            expect(player.undamaged).equals(200);
        });

    });

    describe("EnergyRegen", function() {

        it("S: 3 cycles: energy=142+max=150+undamaged=198 G: obj W: obj R: ___ D: Executes PlayerTick", function() {
            // Prepare
            const player = {
                energy: 142,
                energy_regen: 5,
                max_energy: 150,
            };

            // Act1
            Pl.EnergyRegen(player);
            // Test1
            expect(player.energy).equals(147);

            // Act2
            Pl.EnergyRegen(player);
            // Test2
            expect(player.energy).equals(150);

            // Act3
            Pl.EnergyRegen(player);
            // Test4
            expect(player.energy).equals(150);
        });

    });

    describe("Blink", function() {

        it("S: 3 cycles G: obj W: obj R: ___ D: Executes Blink", function() {
            // Prepare
            const player = {
                x: 7,
                y: 8,
                r: 2,
                c: {x: 9, y: 10},
                blink: true,
                blink_energycost: 100,
                blinkspeedV: 60,
                energy: 150,
                norm_velocity: {x: 1, y: 0},
                velocity: {x: 4, y: 0}
            };
            // Act1
            Pl.Blink(player);
            // Test1
            expect(player.energy).equals(50);
            expect(player.x).equals(67);
            expect(player.y).equals(8);
            expect(player.c.x).equals(69);
            expect(player.c.y).equals(10);
            expect(player.norm_velocity.x).equals(1);
            expect(player.norm_velocity.y).equals(0);

            // Act2
            Pl.Blink(player);
            // Test2
            expect(player.energy).equals(50);
            expect(player.x).equals(67);
            expect(player.y).equals(8);
            expect(player.c.x).equals(69);
            expect(player.c.y).equals(10);
            expect(player.norm_velocity.x).equals(1);
            expect(player.norm_velocity.y).equals(0);

            // Prepare3
            player.energy = 120;
            player.blink = false;
            // Act3
            Pl.Blink(player);
            // Test3
            expect(player.energy).equals(120);
            expect(player.x).equals(67);
            expect(player.y).equals(8);
            expect(player.c.x).equals(69);
            expect(player.c.y).equals(10);
            expect(player.norm_velocity.x).equals(1);
            expect(player.norm_velocity.y).equals(0);
        });

    });

    describe("HpRegen", function() {

        it("G: obj W: obj R: ___ D: Regenerates (or not) health", function() {
            // Prepare1
            const player = {
                undamaged: 80,
                hp_regen_threshold: 100,
                hp: 293,
                hp_max: 300,
                hp_regen: 3,
            };
            // Act1
            Pl.HpRegen(player);
            // Test1
            expect(player.hp).equals(293);

            // Prepare2
            player.undamaged = 100;
            player.hp = 0;
            // Act2
            Pl.HpRegen(player);
            // Test2
            expect(player.hp).equals(0);

            // Prepare3
            player.hp = 293;
            // Act3
            Pl.HpRegen(player);
            // Test3
            expect(player.hp).equals(296);

            // Prepare4
            player.undamaged = 120;
            // Act4
            Pl.HpRegen(player);
            // Test4
            expect(player.hp).equals(299);

            // Act5
            Pl.HpRegen(player);
            // Test5
            expect(player.hp).equals(player.hp_max);
        });

    });

});