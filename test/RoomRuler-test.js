'use strict';

const expect = require("chai").expect;
const rewire = require("rewire");
const sinon = require("sinon");

const RoomRuler = require("../private/RoomRuler");
const ServerController = require("../private/ServerController");
const Controller = require("../private/Controller");
const walls = require('../private/Warehouse');

let room0;
let player0 = {};
let player1 = {};
let player2 = {};

let testdata;
let hitlist;

describe("RoomRuler", function() {
    before(function () {

        for (let i = 0; i < 56; i++) {
            ServerController.timelist.push([ ServerController.timelist[0][0], i*10 ]);
            ServerController.timelist.shift();
        }
        
        ServerController.space = [];
        room0 = ServerController.CreateRoom(ServerController.rooms["FFA"]);
        
        ServerController.GeneratePlayer(room0, player0, {}, 100);
        ServerController.AddPlToRoom(room0, player0);

        ServerController.GeneratePlayer(room0, player1, {}, 100);
        ServerController.AddPlToRoom(room0, player1);
        
        ServerController.GeneratePlayer(room0, player2, {}, 100);
        ServerController.AddPlToRoom(room0, player2);

        testdata = {
            left: false,
            right: true,
            up: false,
            down: false,
            blink: true,
            fire: true,
            dir: {x: 1, y: 0 },
            reaction_arr: [14,15,16]
        };

        hitlist = [
        {
            dist: 10,
            obj: player1,
            index: 1,
        },
        {
            dist: 12,
            obj: player2,
            index: 2,
        }];
        
    });

    describe("IsSyncDataOk", function() {
        const data = {
            left: true,
            right: false,
            up: true,
            down: false,
            blink: true,
            fire: true,
            dir: {x: 3, y: 5},
            reaction_arr: [14,15,ServerController.ARCHIVELENGTH-1]
        };

        it("G: args is OK W: Good args R: true", function() {
            expect(RoomRuler.IsSyncDataOk(data)).to.be.true;
        });

        it("G: args.right = 3 W: args.right = 'boolean' R: false", function() {
            data.right = 3;
            expect(RoomRuler.IsSyncDataOk(data)).to.be.false;
            data.right = false;
        });

        it("G: args.blink = {} W: args.blink = 'boolean' R: false", function() {
            data.blink = {};
            expect(RoomRuler.IsSyncDataOk(data)).to.be.false;
            data.blink = false;
        });
        
        it("G: args.dir = '3' W: args.dir = 'object' R: false", function() {
            data.dir = "3";
            expect(RoomRuler.IsSyncDataOk(data)).to.be.false;
            data.dir = {x: 3, y: 5};
        });
        
        it("G: args.reaction_arr.l = 8 W: args.reaction_arr.l <= ServerController.max_react_num R: false", function() {
            data.reaction_arr = [1,2,3,4,5,6,7,8]; // ServerController.max_react_num should equal 5
            expect(RoomRuler.IsSyncDataOk(data)).to.be.false;
            data.reaction_arr = [14,15,16];
        });

        it("G: args.reaction_arr[0] = ServerController.ARCHIVELENGTH W: args.reaction_arr[x] >= 0 && <= ServerController.ARCHIVELENGTH-1 R: false", function() {
            data.reaction_arr = [14, ServerController.ARCHIVELENGTH, 15];
            expect(RoomRuler.IsSyncDataOk(data)).to.be.false;
            data.reaction_arr = [14,15,16];
        });

        it("G: args.reaction_arr[0] = -1 W: args.reaction_arr[x] >= 0 && <= ServerController.ARCHIVELENGTH-1 R: false", function() {
            data.reaction_arr = [-1, 9, 10];
            expect(RoomRuler.IsSyncDataOk(data)).to.be.false;
            data.reaction_arr = [14,15,16];
        });

        it("G: args.reaction_arr[0] = true W: Number.isFinite(args.reaction_arr[x])==true R: false", function() {
            data.reaction_arr = [true, 9, 10];
            expect(RoomRuler.IsSyncDataOk(data)).to.be.false;
            data.reaction_arr = [14,15,16];
        });

    });

    describe("AcceptAndArchiveInputs", function() {
        it("G: pl+testdata(frames_seen=2+reacted_num=23) W: pl+testdata R: ___ D: react_indexes=[38,39,0]+filled up archive+chngd farest_index", function() {
            // Prepare
            testdata.reaction_arr = [14,15,16];

            // Act
            RoomRuler.AcceptAndArchiveInputs(player0, testdata);
            
            // Check
            expect(player0.react_indexes).deep.equals([38, 39, 0]);

            expect(room0.archive[38][0].norm_velocity.x, "38: norm_velocity.x").equals(1);
            expect(room0.archive[38][0].norm_velocity.y, "38: norm_velocity.y").equals(0);
            expect(room0.archive[38][0].blink, "38: blink").equals(true);
            expect(room0.archive[38][0].fire, "38: fire").equals(testdata.fire);
            expect(room0.archive[38][0].dir.x, "38: dir.x").equals(testdata.dir.x);
            expect(room0.archive[38][0].dir.y, "38: dir.y").equals(testdata.dir.y);

            expect(room0.archive[39][0].norm_velocity.x, "39: norm_velocity.x").equals(1);
            expect(room0.archive[39][0].norm_velocity.y, "39: norm_velocity.y").equals(0);
            expect(room0.archive[39][0].blink, "39: blink").equals(false);
            expect(room0.archive[39][0].fire, "39: fire").equals(testdata.fire);
            expect(room0.archive[39][0].dir.x, "39: dir.x").equals(testdata.dir.x);
            expect(room0.archive[39][0].dir.y, "39: dir.y").equals(testdata.dir.y);
            
            expect(room0.archive[0][0].norm_velocity.x, "0: norm_velocity.x").equals(1);
            expect(room0.archive[0][0].norm_velocity.y, "0: norm_velocity.y").equals(0);
            expect(room0.archive[0][0].blink, "0: blink").equals(false);
            expect(room0.archive[0][0].fire, "0: fire").equals(testdata.fire);
            expect(room0.archive[0][0].dir.x, "0: dir.x").equals(testdata.dir.x);
            expect(room0.archive[0][0].dir.y, "0: dir.y").equals(testdata.dir.y);

            expect(room0.farest_index).equals(0);

            // Revert changes
            player0.react_indexes = [];
            room0.farest_index = 39;
            testdata.blink = true;
        });

        it("G: pl+testdata(frames_seen=4+reacted_num=14) W: pl+testdata R: ___ D: react_indexes=[15]+filled up archive+chngd farest_index", function() {
            // Prepare
            const norm_speed = Controller.norm({x: -1, y: -1});
            testdata.reaction_arr = [31];
            testdata.left = true;
            testdata.right = false;
            testdata.up = true;
            testdata.down = false;
            testdata.blink = true;

            // Act
            RoomRuler.AcceptAndArchiveInputs(player0, testdata);
            
            // Check
            expect(player0.react_indexes).deep.equals([15]);

            expect(room0.archive[15][0].norm_velocity.x, "15: norm_velocity.x").equals(norm_speed.x);
            expect(room0.archive[15][0].norm_velocity.y, "15: norm_velocity.y").equals(norm_speed.y);
            expect(room0.archive[15][0].blink, "15: blink").equals(true);
            expect(room0.archive[15][0].fire, "15: fire").equals(testdata.fire);
            expect(room0.archive[15][0].dir.x, "15: dir.x").equals(testdata.dir.x);
            expect(room0.archive[15][0].dir.y, "15: dir.y").equals(testdata.dir.y);

            expect(room0.archive[16][0].dir.x, "16: dir.x").equals(0);
            expect(room0.archive[16][0].dir.y, "16: dir.y").equals(0);

            expect(room0.farest_index).equals(15);

            // Revert changes
            player0.react_indexes = [];
            room0.farest_index = 39;
            testdata.blink = true;
        });

        it("G: pl+testdata(frames_seen=10+reacted_num=1) W: pl+testdata R: ___ D: react_indexes=[18, 19, 20, 21, 22]+filled up archive+chngd farest_index", function() {
            // Prepare
            const norm_speed = Controller.norm({x: -1, y: -1});
            testdata.reaction_arr = [34,35,36,37,38];
            testdata.blink = true;

            // Act
            RoomRuler.AcceptAndArchiveInputs(player0, testdata);

            // Check
            expect(player0.react_indexes).deep.equals([18, 19, 20, 21, 22]);

            let blink = true;
            for (let i = 18; i <= 22; i++) {
                expect(room0.archive[i][0].norm_velocity.x, i+": norm_velocity.x").equals(norm_speed.x);
                expect(room0.archive[i][0].norm_velocity.y, i+": norm_velocity.y").equals(norm_speed.y);
                expect(room0.archive[i][0].blink, i+": blink").equals(blink);
                expect(room0.archive[i][0].fire, i+": fire").equals(testdata.fire);
                expect(room0.archive[i][0].dir.x, i+": dir.x").equals(testdata.dir.x);
                expect(room0.archive[i][0].dir.y, i+": dir.y").equals(testdata.dir.y);
                if(i == 18) blink = false;
            }

            expect(room0.farest_index).equals(18);

            // Revert changes
            player0.react_indexes = [];
            room0.farest_index = 39;
            testdata.blink = true;
        });

        it("G: pl+testdata(frames_seen=10+reacted_num=1) W: pl+testdata R: ___ D: react_indexes=[]", function() {
            // Prepare
            const norm_speed = Controller.norm({x: -1, y: -1});
            testdata.reaction_arr = [5,6,7,8,9,10,11,12];
            testdata.blink = true;

            // Act
            RoomRuler.AcceptAndArchiveInputs(player0, testdata);

            // Check
            expect(player0.react_indexes).deep.equals([]);
            expect(room0.farest_index).equals(39);

            // Revert changes
            player0.react_indexes = [];
            room0.farest_index = 39;
            testdata.blink = true;
        });

    });

    describe("ResolveHitlist", function() {

        it("S: shooter.hp=0 G: pl+hitlist+arch_moment W: pl+hitlist+arch_moment R: ___ D: ___", function() {
            player1.hp = 450;
            player2.hp = 450;

            player0.hp = 0; player0.fire = true;
            RoomRuler.ResolveHitlist(player0, hitlist, ServerController.space[0].archive[31]);
            expect(hitlist[0].obj.hp, "0: hitlist[0].obj.hp").equals(450);
            expect(hitlist[1].obj.hp, "0: hitlist[1].obj.hp").equals(450);
            player0.hp = 0.5;
        
        });

        it("S: shooter.fire=false G: pl+hitlist+arch_moment W: pl+hitlist+arch_moment R: ___ D: ___", function() {
            player0.fire = false;
            RoomRuler.ResolveHitlist(player0, hitlist, ServerController.space[0].archive[31]);
            expect(hitlist[0].obj.hp, "1: hitlist[0].obj.hp").equals(450);
            expect(hitlist[1].obj.hp, "1: hitlist[1].obj.hp").equals(450);
            player0.fire = true;
        });

        it("S: [0] obj is a wall G: pl+hitlist+arch_moment W: pl+hitlist+arch_moment R: ___ D: ___", function() {
            hitlist.unshift({dist: 5, obj: walls["FFA"][0]});
            RoomRuler.ResolveHitlist(player0, hitlist, ServerController.space[0].archive[31]);
            expect(hitlist[0].obj.hp, "3: hitlist[0].obj.hp").equals(undefined);
            expect(hitlist[1].obj.hp, "3: hitlist[1].obj.hp").equals(450);
            hitlist.shift();
        });

        it("S: [0].hp=-2 G: pl+hitlist+arch_moment W: pl+hitlist+arch_moment R: ___ D: damages second obj", function() {
            hitlist[0].obj.hp = -2;
            RoomRuler.ResolveHitlist(player0, hitlist, ServerController.space[0].archive[31]);
            expect(hitlist[0].obj.hp, "4: hitlist[0].obj.hp").equals(-2);
            expect(hitlist[1].obj.hp, "4: hitlist[1].obj.hp").equals(450 - player0.dmg);
            expect(ServerController.space[0].archive[31][2].influence.dmg[0]).equals(player0);
            hitlist[0].obj.hp = 200;
        });

        it("S: [0] and [1] are fine G: pl+hitlist+arch_moment W: pl+hitlist+arch_moment R: ___ D: damages first obj", function() {
            RoomRuler.ResolveHitlist(player0, hitlist, ServerController.space[0].archive[31]);
            expect(hitlist[0].obj.hp, "5: hitlist[0].obj.hp").equals(200 - player0.dmg);
            expect(hitlist[1].obj.hp, "5: hitlist[1].obj.hp").equals(450 - player0.dmg);
            expect(ServerController.space[0].archive[31][1].influence.dmg[0]).equals(player0);
            expect(ServerController.space[0].archive[31][2].influence.dmg[0]).equals(player0);
        });

    });

    describe("ProcessInputs", function() {

        it("INDESCRIBABLE", function() {
            for (let i = 0; i < ServerController.space[0].archive.length; i++) {
                const time_moment = ServerController.space[0].archive[i];
                const time_moment_history = ServerController.space[0].gamehistory[i];
                if(i == 20) {
                    // ARCHIVE
                    time_moment[0].norm_velocity = Controller.norm({x: 1, y: 0});
                    time_moment[0].blink = true;
                    time_moment[0].fire = true;
                    time_moment[0].dir = Controller.norm({x: -3, y: 3});
                    time_moment[0].energy = 128;
                    time_moment[0].x = 18;
                    time_moment[0].y = 0;
                    time_moment[0].c.x = 22;
                    time_moment[0].c.y = 4;
                    time_moment[0].blinkspeedV = 80;
    
                    time_moment[1].norm_velocity = Controller.norm({x: 1, y: 0});
                    time_moment[1].blink = true;
                    time_moment[1].fire = true;
                    time_moment[1].dir = Controller.norm({x: -3, y: -2});
                    time_moment[1].energy = 24;
                    time_moment[1].x = 18;
                    time_moment[1].y = 11;
                    time_moment[1].c.x = 22;
                    time_moment[1].c.y = 15;
                    time_moment[0].blinkspeedV = 80;
    
                    time_moment[2].norm_velocity = Controller.norm({x: -1, y: 0});
                    time_moment[2].blink = false;
                    time_moment[2].fire = true;
                    time_moment[2].dir = Controller.norm({x: 0, y: -1});
                    time_moment[2].energy = 200;
                    time_moment[2].x = 0;
                    time_moment[2].y = 9;
                    time_moment[2].c.x = 4;
                    time_moment[2].c.y = 13;
                    time_moment[0].blinkspeedV = 80;
    
                    // GAMEHISTORY
                    time_moment_history[0].x = 2;
                    time_moment_history[0].y = 0;
                    time_moment_history[0].r = 4;
                    time_moment_history[0].c = {x: 6, y: 4};
    
                    time_moment_history[1].x = 9;
                    time_moment_history[1].y = 11;
                    time_moment_history[1].r = 4;
                    time_moment_history[1].c = {x: 13, y: 15};
    
                    time_moment_history[2].x = 0;
                    time_moment_history[2].y = 17;
                    time_moment_history[2].r = 4;
                    time_moment_history[2].c = {x: 4, y: 21};
                }
            }
    
            room0.farest_index = 20;

            const temp_walls = walls[room0.type];
            walls[room0.type] = [];
            const temp_ZoneClip = Controller.ZoneClip;
            Controller.ZoneClip = function () {
                
            }
            
            RoomRuler.ProcessInputs(ServerController.space[0]);

            walls[room0.type] = temp_walls;
            Controller.ZoneClip = temp_ZoneClip;

            const archive = ServerController.space[0].archive;
            expect(archive[21][0].hp).equals(444);
            expect(archive[21][0].x).equals(101);
            expect(archive[21][0].y).equals(0);
            expect(archive[21][0].energy).equals(29);
            expect(archive[21][0].undamaged).equals(1);
            expect(archive[20][0].influence.dmg[0], "1: dmg[0]").equals(player1);
            expect(archive[20][0].influence.dmg[1], "1: dmg[1]").equals(player2);

            expect(archive[21][1].hp).equals(447);
            expect(archive[21][1].x).equals(21);
            expect(archive[21][1].y).equals(11);
            expect(archive[21][1].energy).equals(25);
            expect(archive[21][1].undamaged).equals(1);
            expect(archive[20][1].influence.dmg[0], "2: dmg[0]").equals(player0);

            expect(archive[21][2].hp).equals(450);
            expect(archive[21][2].x).equals(-3);
            expect(archive[21][2].y).equals(9);
            expect(archive[21][2].energy).equals(200);
            expect(archive[21][2].undamaged).equals(1);
            expect(archive[20][2].influence.dmg).to.be.lengthOf(0);
        });

    });
});