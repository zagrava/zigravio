'use strict';

const expect = require("chai").expect;
const rewire = require("rewire");
const sinon = require("sinon");

const ServerController = require("../private/ServerController");
const walls = require('../private/Warehouse');
const Pl = require('../private/Player');

let player0;
let player1;
let player2;
let player3;
let player4;
let player5;

let room0;
let room1;

describe("ServerController", function() {

    before(function () {
        ServerController.space = [];

        player0 = {
            name: "Eney",
            nickname: "Fayniy kozak",
            hp: 300
        };
        player1 = {
            name: "Zeus",
            nickname: "Lightning man",
            hp: 30000
        };
        player2 = {
            name: "Nazair",
            nickname: "Revolution",
            hp: 800
        };
        player3 = {
            name: "Muselk",
            nickname: "Streamer",
            hp: 400
        };
        player4 = {
            name: "Elon",
            nickname: "Entrepreneur",
            hp: 2135
        };

        player5 = {
            name: "Elizabeth",
            falsename: "Anna",
            hp: 30
        };
        ServerController.GeneratePlayer({type: "FFA"}, player5, {}, 100);
        
        ServerController.CreateRoom(ServerController.rooms["DUEL"]);
        ServerController.CreateRoom(ServerController.rooms["FFA"]);
        
        room0 = ServerController.space[0];
        ServerController.GeneratePlayer(room0, player0, {teams: 1}, 100);
        ServerController.AddPlToRoom(room0, player0);
        ServerController.GeneratePlayer(room0, player1, {teams: 2}, 100);
        ServerController.AddPlToRoom(room0, player1);
        
        room1 = ServerController.space[1];
        ServerController.GeneratePlayer(room0, player4, {}, 100);
        ServerController.AddPlToRoom(room1, player4);
        ServerController.GeneratePlayer(room0, player2, {}, 100);
        ServerController.AddPlToRoom(room1, player2);
        ServerController.GeneratePlayer(room0, player3, {}, 100);
        ServerController.AddPlToRoom(room1, player3);
    })

    describe("IsEnterDataOk", function() {
        const data = {
            type: 'DUEL',
            name: '234Petro_Ignat'
        }

        it("G: args is OK R: true", function() {
            expect(ServerController.IsEnterDataOk(data)).to.be.true;
        });

        it("G: args.type = 3 W: args.type = 'string' R: false", function() {
            data.type = 3;
            expect(ServerController.IsEnterDataOk(data)).to.be.false;
            data.type = 'FFA';
        });

        it("G: args.name = {} W: args.name = 'string' R: false", function() {
            data.name = {};
            expect(ServerController.IsEnterDataOk(data)).to.be.false;
            data.name = 'Another test';
        });
    });

    describe("IsAppropriate", function() {
        const room = {
            type: 'DUEL',
            enterable: true,
            max: 15,
            objects: ["1obj", "2obj", "3obj"]
        }

        it("G: args is OK R: true", function() {
            expect(ServerController.IsAppropriate(room, "DUEL")).to.be.true;
        });

        it("G: args.type = 'FFA' W: args.type = 'DUEL' R: false", function() {
            expect(ServerController.IsAppropriate(room, "FFA")).to.be.false;
        });

        it("G: args.room.enterable = false W: args.room.enterable = true R: false", function() {
            room.enterable = false;
            expect(ServerController.IsAppropriate(room, "FFA")).to.be.false;
            room.enterable = true;
        });

        it("G: args.room.max = 3 + args.room.objects.length = 3 W: args.room.objects.length < args.room.max R: false", function() {
            room.max = 3;
            expect(ServerController.IsAppropriate(room, "FFA")).to.be.false;
        });
    });

    describe("CreateRoom", function() {
        it("G: args is OK R: ___ D: add room to space", function() {
            ServerController.space = [];
            ServerController.CreateRoom(ServerController.rooms["FFA"]);
            expect(ServerController.space.length).to.be.equal(1);
            expect(ServerController.space[0]).to.be.an('object');
            expect(ServerController.space[0].objects).an('array').that.is.empty;
            expect(ServerController.space[0].archive).an('array').that.have.lengthOf(ServerController.ARCHIVELENGTH);
            expect(ServerController.space[0].archive[0]).to.deep.equal([]);
            expect(ServerController.space[0].gamehistory).an('array').that.have.lengthOf(ServerController.ARCHIVELENGTH);
            expect(ServerController.space[0].gamehistory[0]).to.deep.equal([]);
            expect(ServerController.space[0].width).to.equal(3000);
            expect(ServerController.space[0].height).to.equal(2000);
            expect(ServerController.space[0].time).to.equal(-1);
            expect(ServerController.space[0].type).to.equal("FFA");
            expect(ServerController.space[0].max).to.equal(15);
            expect(ServerController.space[0].enterable).to.equal(true);
            expect(ServerController.space[0].gamestate).to.be.an('object');
            expect(ServerController.space[0].gamestate.final).to.equal(-1);
        });
    });

    describe("DeletePlFromQueues", function() {

        it("G: player1 W: player object R: ___ D: [pl0,pl2,pl3][pl2,pl3] left", function() {
            ServerController.duelqueue = [player0, player1, player2, player3];
            ServerController.ffaqueue = [player2, player3];

            ServerController.DeletePlFromQueues(player1);
            expect(ServerController.duelqueue[0]).to.be.equal(player0);
            expect(ServerController.duelqueue[1]).to.be.equal(player2);
            expect(ServerController.duelqueue[2]).to.be.equal(player3);
            expect(ServerController.duelqueue.length).to.be.equal(3);

            expect(ServerController.ffaqueue[0]).to.be.equal(player2);
            expect(ServerController.ffaqueue[1]).to.be.equal(player3);
            expect(ServerController.ffaqueue.length).to.be.equal(2);
        });

        it("G: player1 W: player object R: ___ D: [pl0,pl2,pl3][pl2,pl3] left", function() {
            ServerController.DeletePlFromQueues(player1);
            expect(ServerController.duelqueue[0]).to.be.equal(player0);
            expect(ServerController.duelqueue[1]).to.be.equal(player2);
            expect(ServerController.duelqueue[2]).to.be.equal(player3);
            expect(ServerController.duelqueue.length).to.be.equal(3);

            expect(ServerController.ffaqueue[0]).to.be.equal(player2);
            expect(ServerController.ffaqueue[1]).to.be.equal(player3);
            expect(ServerController.ffaqueue.length).to.be.equal(2);
        });

        it("G: player2 W: player object R: ___ D: [pl0,pl3][pl3] left", function() {
            ServerController.DeletePlFromQueues(player2);
            expect(ServerController.duelqueue[0]).to.be.equal(player0);
            expect(ServerController.duelqueue[1]).to.be.equal(player3);
            expect(ServerController.duelqueue.length).to.be.equal(2);

            expect(ServerController.ffaqueue[0]).to.be.equal(player3);
            expect(ServerController.ffaqueue.length).to.be.equal(1);
        });

        it("G: player0 W: player object R: ___ D: [pl3][pl3] left", function() {
            ServerController.DeletePlFromQueues(player0);
            expect(ServerController.duelqueue[0]).to.be.equal(player3);
            expect(ServerController.duelqueue.length).to.be.equal(1);
            
            expect(ServerController.ffaqueue[0]).to.be.equal(player3);
            expect(ServerController.ffaqueue.length).to.be.equal(1);
        });

        it("G: player3 W: player object R: ___ D: [] left", function() {
            ServerController.DeletePlFromQueues(player3);
            expect(ServerController.duelqueue.length).to.be.equal(0);
            expect(ServerController.ffaqueue.length).to.be.equal(0);
        });

        it("G: player2 W: player object R: ___ D: [] left", function() {
            ServerController.DeletePlFromQueues(player2);
            expect(ServerController.duelqueue.length).to.be.equal(0);
            expect(ServerController.ffaqueue.length).to.be.equal(0);
        });
    });

    describe("DeletePlFromRoom", function() {
        it("G: player4 W: player object R: true D: Deleted pl4 from room", function() {
            ServerController.space = [room0, room1];

            expect(ServerController.DeletePlFromRoom(player4)).to.be.true;
            expect(ServerController.space[1].archive[0]).to.have.lengthOf(2);
            expect(ServerController.space[1].gamehistory[0]).to.have.lengthOf(2);
            expect(ServerController.space[1].objects).to.have.lengthOf(2);
        });

        it("G: player2 W: player object R: true D: Deleted pl2 from room", function() {
            expect(ServerController.DeletePlFromRoom(player2)).to.be.true;
            expect(ServerController.space[1].archive[0]).to.have.lengthOf(1);
            expect(ServerController.space[1].gamehistory[0]).to.have.lengthOf(1);
            expect(ServerController.space[1].objects).to.have.lengthOf(1);
        });

        it("G: player3 W: player object R: true D: Deleted pl3 from room", function() {
            expect(ServerController.DeletePlFromRoom(player3)).to.be.true;
            expect(ServerController.space[1].archive[0]).to.have.lengthOf(0);
            expect(ServerController.space[1].gamehistory[0]).to.have.lengthOf(0);
            expect(ServerController.space[1].objects).to.have.lengthOf(0);
        });

        it("G: player4 W: player object R: false", function() {
            expect(ServerController.DeletePlFromRoom(player4)).to.be.false;

            ServerController.AddPlToRoom(room1, player4);
            ServerController.AddPlToRoom(room1, player2);
            ServerController.AddPlToRoom(room1, player3);
        });
    });

    describe("AddPlToRoom", function() {
        it("G: player5 W: player object R: true D: Added pl5 to room", function() {
            ServerController.AddPlToRoom(room0, player5);

            expect(ServerController.space[0].archive[0]).to.have.lengthOf(3);
            expect(ServerController.space[0].gamehistory[0]).to.have.lengthOf(3);
            expect(ServerController.space[0].objects).to.have.lengthOf(3);

            ServerController.DeletePlFromRoom(player5);
        });
    });

    describe("MainCheckRules", function() {
        describe("CheckDuelRules", function() {
            it("S: pl0.hp=-2 + pl1.hp=0 G: 'DUEL' W: gametype R: ___ D: Declare final 1", function() {
                ServerController.space[0].archive[0][0].hp = -2;
                ServerController.space[0].archive[0][1].hp = 0;
                ServerController.MainCheckRules(ServerController.space[0]);
                expect(ServerController.space[0].gamestate.final).to.equal(2);
            });

            it("S: pl0.hp=0 + pl1.hp=68 G: 'DUEL' W: gametype R: ___ D: Declare final 1", function() {
                ServerController.space[0].archive[0][0].hp = 0;
                ServerController.space[0].archive[0][1].hp = 68;
                ServerController.MainCheckRules(ServerController.space[0]);
                expect(ServerController.space[0].gamestate.final).to.equal(1);
            });

            it("S: pl0.hp=75 + pl1.hp=-8 G: 'DUEL' W: gametype R: ___ D: Declare final 1", function() {
                ServerController.space[0].archive[0][0].hp = 75;
                ServerController.space[0].archive[0][1].hp = -8;
                ServerController.MainCheckRules(ServerController.space[0]);
                expect(ServerController.space[0].gamestate.final).to.equal(0);
            });

            it("S: only 1 pl G: 'DUEL' W: gametype R: ___ D: Declare final 1", function() {
                ServerController.DeletePlFromRoom(player1);
                ServerController.MainCheckRules(ServerController.space[0]);
                expect(ServerController.space[0].gamestate.final).to.equal(3);
                ServerController.AddPlToRoom(room0, player1);
            });
        });

        describe("CheckFFARules", function() {
            it("E: pl4.hp=0 + pl4.infl.dmg=[pl2,pl3] G: 'DUEL' W: gametype R: ___ D: Add kills to offenders", function() {
                // Prepare
                const ffa_room = ServerController.space[1];
                let pl4_index = ffa_room.objects.indexOf(player4);
                ffa_room.archive[0][pl4_index].influence.dmg.push(player2, player3);
                ffa_room.archive[1][pl4_index].hp = 0;

                // Act
                ServerController.MainCheckRules(ffa_room);

                // Test
                expect(player2.kills).to.equal(1);
                expect(player3.kills).to.equal(1);

                // Clear
                player2.kills = 0; player3.kills = 0;
                room0.gamestate.final = -1;
                room0.archive[0][pl4_index].influence.dmg = [];
                room0.archive[1][pl4_index].hp = 5;
            });

            it("E: pl4.hp=3 + pl4.infl.dmg=[pl2] G: 'DUEL' W: gametype R: ___ D: No kills added", function() {
                // Prepare
                const ffa_room = ServerController.space[1];
                let pl4_index = ffa_room.objects.indexOf(player4);
                ffa_room.archive[0][pl4_index].influence.dmg.push(player2);
                ffa_room.archive[1][pl4_index].hp = 3;

                // Act
                ServerController.MainCheckRules(ffa_room);

                // Test
                expect(player2.kills).to.equal(0);

                // Clear
                player2.kills = 0; player3.kills = 0;
                room0.gamestate.final = -1;
                room0.archive[0][pl4_index].influence.dmg = [];
                room0.archive[1][pl4_index].hp = 5;
            });

            it("E: No players G: 'DUEL' W: gametype R: ___ D: ffa_room.gamestate.final=0", function() {
                // Prepare
                const ffa_room = ServerController.space[1];
                let temp_ffa_room = ffa_room.objects;
                ffa_room.objects = [];

                // Act
                ServerController.MainCheckRules(ffa_room);

                // Test
                expect(ffa_room.gamestate.final).to.equal(0);

                // Clear
                room0.gamestate.final = -1;
                ffa_room.objects = temp_ffa_room;
            });

        });
    });

    describe("ResolveQueues", function() {
        it("S: duelqueue.l=7 + ffaqueue.l=5 D: 6 rooms", function() {
            const temp_TransformData = Pl.TransformData;
            Pl.TransformData = sinon.stub();

            for (let i = 0; i < 7; i++) {
                ServerController.duelqueue.push({
                    emit: sinon.stub()
                });
                if(i < 5) {
                    ServerController.ffaqueue.push({
                        emit: sinon.stub()
                    });
                }
            }

            ServerController.space[1].objects.push({},{},{},{},{},{},{},{},{},{}); // 10

            ServerController.ResolveQueues(10);

            expect(ServerController.space[0].objects, "0: SHB with length 2").has.lengthOf(2);
            expect(ServerController.space[1].objects, "0: SHB with length 15").has.lengthOf(15);

            expect(ServerController.space[2], "2: SHB object").is.an('object');
            expect(ServerController.space[2].type, "2: SHB DUEL").equal('DUEL');
            expect(ServerController.space[2].objects, "2: SHB be array with length 2").is.an('array').that.has.lengthOf(2);
            expect(ServerController.space[2].objects[0].emit.calledOnce, "2: SHB be called 0").is.true;
            expect(ServerController.space[2].objects[1].emit.calledOnce, "2: SHB be called 1").is.true;

            expect(ServerController.space[3], "3: SHB object").is.an('object');
            expect(ServerController.space[3].type, "3: SHB DUEL").equal('DUEL');
            expect(ServerController.space[3].objects, "3: SHB be array with length 2").is.an('array').that.has.lengthOf(2);
            expect(ServerController.space[3].objects[0].emit.calledOnce, "3: SHB be called 0").is.true;
            expect(ServerController.space[3].objects[1].emit.calledOnce, "3: SHB be called 1").is.true;

            expect(ServerController.space[4], "4: SHB object").is.an('object');
            expect(ServerController.space[4].type, "4: SHB DUEL").equal('DUEL');
            expect(ServerController.space[4].objects, "4: SHB be array with length 2").is.an('array').that.has.lengthOf(2);
            expect(ServerController.space[4].objects[0].emit.calledOnce, "4: SHB be called 0").is.true;
            expect(ServerController.space[4].objects[1].emit.calledOnce, "4: SHB be called 1").is.true;

            expect(ServerController.space[5], "5: SHB object").is.an('object');
            expect(ServerController.space[5].type, "5: SHB FFA").equal('FFA');
            expect(ServerController.space[5].objects, "5: SHB be array with length 3").is.an('array').has.lengthOf(3);
            expect(ServerController.space[5].objects[0].emit.calledOnce, "5: SHB be called 0").is.true;
            expect(ServerController.space[5].objects[1].emit.calledOnce, "5: SHB be called 1").is.true;
            expect(ServerController.space[5].objects[2].emit.calledOnce, "5: SHB be called 2").is.true;

            ServerController.space.splice(2, ServerController.space.length-2);
            ServerController.space[1].objects.splice(3, ServerController.space[1].objects.length-3);

            Pl.TransformData = temp_TransformData;
        });
    });

    describe("CloseRoom", function() {
        beforeEach(function () {
            // Create new stub with clear data
            player0.emit = sinon.stub(); player1.emit = sinon.stub();
        });

        afterEach(function () {
            ServerController.space.unshift(room0);
        })

        it("S: room.gamestate.final = 0 G: room W: room R: ___ D: Close room + emits msgs", function() {
            expect(ServerController.space, '1: before length').has.lengthOf(2);
            
            // Preparation
            ServerController.space[0].gamestate.final = 0;
            ServerController.CloseRoom(ServerController.space[0]);

            expect(player0.emit.calledWith('closeroom', {final: "vic"})).to.be.true;
            expect(player1.emit.calledWith('closeroom', {final: "def"})).to.be.true;
            expect(ServerController.space).has.lengthOf(1);
        });

        it("S: room.gamestate.final = 1 G: room W: room R: ___ D: Close room + emits msgs", function() {
            expect(ServerController.space, '2: before length').has.lengthOf(2);

            // Preparation
            ServerController.space[0].gamestate.final = 1;
            ServerController.CloseRoom(ServerController.space[0]);

            expect(player0.emit.calledWith('closeroom', {final: "def"})).to.be.true;
            expect(player1.emit.calledWith('closeroom', {final: "vic"})).to.be.true;
            expect(ServerController.space).has.lengthOf(1);
        });

        it("S: room.gamestate.final = 2 G: room W: room R: ___ D: Close room + emits msgs", function() {
            expect(ServerController.space, '3: before length').has.lengthOf(2);

            // Preparation
            ServerController.space[0].gamestate.final = 2;
            ServerController.CloseRoom(ServerController.space[0]);

            expect(player0.emit.calledWith('closeroom', {final: "draw"})).to.be.true;
            expect(player1.emit.calledWith('closeroom', {final: "draw"})).to.be.true;
            expect(ServerController.space).has.lengthOf(1);
        });

        it("S: room.gamestate.final = 3 + one pl left G: room W: room R: ___ D: Close room + emits 1 msg", function() {
            expect(ServerController.space, '4: before length').has.lengthOf(2);

            // Preparation || room0 === ServerController.space[0]
            ServerController.space[0].gamestate.final = 3;
            room0.objects.shift();
            ServerController.CloseRoom(ServerController.space[0]);
            room0.objects.unshift(player0);

            expect(player0.emit.calledWith('closeroom', {final: "vic"})).to.be.false;
            expect(player1.emit.calledWith('closeroom', {final: "vic"})).to.be.true;
            expect(ServerController.space).has.lengthOf(1);
        });

        it("S: room.gamestate.final = 3 + no left pls G: room W: room R: ___ D: Close room + emits no msgs", function() {
            expect(ServerController.space, '3: before length').has.lengthOf(2);

            // Preparation || room0 === ServerController.space[0]
            ServerController.space[0].gamestate.final = 3;
            room0.objects.shift(); room0.objects.shift();
            ServerController.CloseRoom(ServerController.space[0]);
            room0.objects.unshift(player1); room0.objects.unshift(player0);

            expect(player0.emit.calledWith('closeroom', {final: "vic"})).to.be.false;
            expect(player1.emit.calledWith('closeroom', {final: "vic"})).to.be.false;
            expect(ServerController.space).has.lengthOf(1);
        });

    });

    describe("FindSocketInSpace", function() {
        it("G: args.socket = pl2 W: args.socket = pl R: ___ D: finds pl at [1,1]", function() {
            // room0 = [pl0, pl1]; room1 = [pl4, pl2, pl3];
            ServerController.space = [room0, room1];
            const location = ServerController.FindSocketInSpace(player2);
            expect(location[0], '1: which room').to.equal(1);
            expect(location[1], '1: which pl').to.equal(1);
        });

        it("G: args.socket = pl0 W: args.socket = pl R: ___ D: finds pl at [0,0]", function() {
            const location = ServerController.FindSocketInSpace(player0);
            expect(location[0], '2: which room').to.equal(0);
            expect(location[1], '2: which pl').to.equal(0);
        });

        it("G: args.socket = pl3 W: args.socket = pl R: ___ D: finds pl at [1,2]", function() {
            const location = ServerController.FindSocketInSpace(player3);
            expect(location[0], '3: which room').to.equal(1);
            expect(location[1], '3: which pl').to.equal(2);
        });

        it("G: args.socket = pl5 W: args.socket = pl R: ___ D: returns [-1,-1]", function() {
            const location = ServerController.FindSocketInSpace(player5);
            expect(location[0], '4: which room').to.equal(-1);
            expect(location[1], '4: which pl').to.equal(-1);
        });
    });

    describe("GetRoomEnvData", function() {
        it("G: room W: room R: OK env_data", function() {
            const env_data = ServerController.GetRoomEnvData(room1);

            expect(env_data).to.have.lengthOf(walls[room1.type].length+1);
            expect(env_data[0].max_react_num).to.equal(5);
            expect(env_data[0].width).to.equal(3000);
            expect(env_data[0].height).to.equal(2000);
            expect(env_data[0].type).to.equal('FFA');
            expect(env_data[1].type).to.equal('wl');
            expect(env_data[1].vertices).to.be.an('array')
                                        .that.have.lengthOf(walls[room1.type][0].vertices.length);
        });
    });

    describe("GetRoomObjsInitData", function() {
        it("G: room W: room R: OK objs_data", function() {
            const objs_data = ServerController.GetRoomObjsInitData(ServerController.space[1]);
            expect(objs_data).to.have.lengthOf(ServerController.space[1].objects.length);
        });
    });

    after(function () {
        setTimeout(function() {
            console.log("Stop process");
            process.exit();
        }, 1000);
    });
});