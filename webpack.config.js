var path = require('path');

module.exports = {
    mode: 'none',
    entry: './private/Client.js',
    output: {
        path: path.join(__dirname, 'public/javascript'),
        filename: 'bundle.js',
    }
};